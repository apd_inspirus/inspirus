﻿using System;
using Inspirus.Logic.Extension;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog;
using NLog.Config;

namespace Inspirus.Logic.Tests.Misc
{

    [TestClass]
    public class QueueLogTest
    {
        private readonly Logger _logger;

        public QueueLogTest()
        {
            ConfigurationItemFactory.Default.Targets.RegisterDefinition("ExpiringMsmq", typeof(NlogMsmqTarget));
            _logger = LogManager.GetLogger("UnitTestLogger");
        }

        [TestMethod]
        public void TestTrace()
        {
            _logger.Trace("unit test trace message at {0}", DateTime.Now);
        }

        [TestMethod]
        public void TestDebug()
        {
            _logger.Debug("unit test debug message at {0}", DateTime.Now);
        }

        [TestMethod]
        public void TestInfo()
        {
            _logger.Info("unit test info message at {0}", DateTime.Now);
        }

        [TestMethod]
        public void TestWarning()
        {
            _logger.Warn("unit test warn message at {0}", DateTime.Now);
        }

        [TestMethod]
        public void TestError()
        {
            _logger.Error("unit test error message at {0}", DateTime.Now);
        }

        [TestMethod]
        public void TestFatal()
        {
            _logger.Fatal("unit test fatal message at {0}", DateTime.Now);
        }
    }
}
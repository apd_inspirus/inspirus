﻿using System;
using System.Collections;
using Inspirus.Logic.Extension;
using Inspirus.Logic.Implementation;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog.Config;
using Ninject;
using Ninject.Modules;

namespace Inspirus.Logic.Tests.SmsProviders
{
    [TestClass]
    public class SybaseSsTest
    {
        protected ISmsProvider _provider;
        protected ISms _sms;

        public SybaseSsTest()
        {
            ConfigurationItemFactory.Default.Targets.RegisterDefinition("ExpiringMsmq", typeof(NlogMsmqTarget));
            IKernel kernel = new StandardKernel(new SmsSenderServiceModule());
            _provider = kernel.Get<ISmsProvider>();
            _sms = kernel.Get<ISms>();
        }

        [TestMethod]
        public void SmsSend()
        {
            ProviderResult result = _provider.Send(_sms);
            Assert.AreEqual(result.Code, ProviderResult.Ok.Code);
        }

        internal sealed class SmsSenderServiceModule : NinjectModule
        {
            public override void Load()
            {
                Bind<ISms>().To<Sms>()
                            .WithConstructorArgument("dt", DateTime.Now)
                            .WithConstructorArgument("id", "abcdef0123456789")
                            .WithConstructorArgument("recipientList", "64220990212")
                            .WithConstructorArgument("senderPhone", "64220990212")
                            .WithConstructorArgument("message", "test sms: in spirit we trust OR in spirit spiritas")
                            .WithConstructorArgument("gateways",
                                                     new ArrayList
                                                         {
                                                             Constant.MOBILEWAY_ROAMING
                                                         })
                            .WithConstructorArgument("origPhoneList", "0220990212")
                            .WithConstructorArgument("uidB2U", "3F7C0D85-D1F0-D655-ED6AA81C93A1697A")
                            .WithConstructorArgument("smsFlash", true);

                Bind<ISmsProvider>().To<SybaseProvider>()
                                    .WithConstructorArgument("username", "inspirus_roaming")
                                    .WithConstructorArgument("password", "rQjr3Hbl")
                                    .WithConstructorArgument("url", "/inspirus_roaming/inspirus_roaming.sms")
                                    .WithConstructorArgument("host", "messaging.sybase365.com")
                                    .WithConstructorArgument("replyUrl", "http://203.63.5.170:9098/NextWS/HandsetAck.aspx")
                                    .WithConstructorArgument("gatewayId", Constant.MOBILEWAY_ROAMING)
                                    .WithConstructorArgument("order", 1)
                                    .WithConstructorArgument("grade", "SS");
            }
        }
    }
}
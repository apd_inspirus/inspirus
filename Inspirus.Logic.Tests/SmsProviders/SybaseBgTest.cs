﻿using System;
using System.Collections;
using System.Collections.Generic;
using Inspirus.Logic.Extension;
using Inspirus.Logic.Implementation;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog.Config;
using Ninject;
using Ninject.Modules;

namespace Inspirus.Logic.Tests.SmsProviders
{
    [TestClass]
    public class SybaseBgTest
    {
        protected ISmsProvider _provider;
        protected ISms _sms;

        public SybaseBgTest()
        {
            ConfigurationItemFactory.Default.Targets.RegisterDefinition("ExpiringMsmq", typeof(NlogMsmqTarget));
            IKernel kernel = new StandardKernel(new SmsSenderServiceModule());
            _provider = kernel.Get<ISmsProvider>();
            _sms = kernel.Get<ISms>();
        }

        [TestMethod]
        public void SmsSend()
        {
            ProviderResult result = _provider.Send(_sms);
            Assert.AreEqual(result.Code, ProviderResult.Ok.Code);
        }


        internal sealed class SmsSenderServiceModule : NinjectModule
        {
            public override void Load()
            {
                Bind<ISms>().To<Sms>()
                            .WithConstructorArgument("dt", DateTime.Now)
                            .WithConstructorArgument("id", "abcdef0123456789")
                            .WithConstructorArgument("recipientList", "64220990212")
                            .WithConstructorArgument("senderPhone", "64220990212")
                            .WithConstructorArgument("message", "test sms: in spirit we trust OR in spirit spiritas")
                            .WithConstructorArgument("gateways",
                                                     new ArrayList
                                                         {
                                                             Constant.MOBILEWAY_DIRECT
                                                         })
                            .WithConstructorArgument("origPhoneList", "0220990212")
                            .WithConstructorArgument("uidB2U", "3F7C0D85-D1F0-D655-ED6AA81C93A1697A")
                            .WithConstructorArgument("smsFlash", true);

                Bind<ISmsProvider>().To<SybaseProvider>()
                                    .WithConstructorArgument("username", "inspirus_direct")
                                    .WithConstructorArgument("password", "jH9l8MKQ")
                                    .WithConstructorArgument("url", "/inspirus_direct/inspirus_direct.sms")
                                    .WithConstructorArgument("host", "messaging.sybase365.com")
                                    .WithConstructorArgument("replyUrl", "http://203.63.5.170:9098/NextWS/HandsetAck.aspx")
                                    .WithConstructorArgument("gatewayId", Constant.MOBILEWAY_DIRECT)
                                    .WithConstructorArgument("order", 1)
                                    .WithConstructorArgument("grade", "BG");
            }
        }
    }
}
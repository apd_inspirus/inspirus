﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using Inspirus.Logic.Implementation;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Mock;
using Inspirus.Logic.Service;
using InspirusScheduler;
using InspirusSender;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Ninject.Modules;

namespace Inspirus.Logic.Tests.Gateway
{
    [TestClass]
    public class ServiceTest
    {
        private static readonly string _cs1 = @"Data Source=.\dev7sql;Initial Catalog=InSpiritus;Integrated Security=True";//NEW
        private static readonly string _cs2 = @"Data Source=.\dev7sql;Initial Catalog=Inspirus;Integrated Security=True";//OLD
        private static readonly string _command = @"INSERT INTO smsschedule SELECT TOP 1 * from smsschedule2 WHERE smsschedule_datetime < getdate() AND smsschedule_sentstatus IS NULL ORDER BY smsschedule_datetime UPDATE smsschedule2 SET smsschedule_sentstatus = 9999 WHERE uid_smsschedule IN (SELECT uid_smsschedule COLLATE Latin1_General_CI_AS FROM dbo.smsschedule)";
        private static readonly string _path1 = @"c:\services\ServiceLogs\Info.Inspirus.Sender.log";//NEW
        private static readonly string _path2 = @"c:\services\ServiceLogs\Info.Sender.log";//OLD

        private static IService _service;
        private static Service1 _inspirus1;
        private static Service1 _inspirus2;
        
        public ServiceTest()
        {
            IKernel kernel = new StandardKernel(new SmsSenderMockModule());
            _service = kernel.Get<IService>();

            _inspirus1 = new Service1();
            _inspirus1.Start(_cs1, "q_");

            _inspirus2 = new Service1();
            _inspirus2.Start(_cs2, null);
        }

        [TestMethod]
        public void TestMethod1()
        {
            bool allIsRight = true;
            ThreadPool.QueueUserWorkItem(RunOldSender);
            _service.Start();
            while (allIsRight)
            {
                CopySms(_cs1);
                CopySms(_cs2);
                if (!ExpectedResult())
                {
                    allIsRight = false;
                }
            }
            _service.Stop();
            _inspirus1.StopMe();
            _inspirus2.StopMe();
            Assert.Fail();
        }

        private void RunOldSender(object state)
        {
            System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName("InspirusSender");
            if (processes.Length <= 1)
            {
                Application.Run(new fmSender());
            }
        }

        private bool ExpectedResult()
        {
            Thread.Sleep(TimeSpan.FromSeconds(20));
            return true;
        }

        private void CopySms(string cs)
        {
            using (SqlConnection connection = new SqlConnection(cs))
            {
                SqlCommand command = new SqlCommand(_command)
                    {
                        Connection = connection
                    };
                connection.Open();
                command.ExecuteNonQuery();
            }
        }
    }

    public sealed class SmsSenderMockModule : NinjectModule
    {
        public override void Load()
        {
            ISettings settings = new MockSettings();
            settings.Load("Software\\Nextdata\\Inspirus");
            settings.MqName = "q_";
            DataSet gatewayRange = settings.GetGatewayRangeDataSet();

            Bind<IPhonesValidator>().To<PhonesValidator>();
            Bind<ISmsProvider>().To<MockSybaseProvider>()
                                .WithConstructorArgument("username", "inspirus_roaming")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "/inspirus_roaming/inspirus_roaming.sms")
                                .WithConstructorArgument("host", "messaging.sybase365.com")
                                .WithConstructorArgument("replyUrl", "http://203.63.5.170:9098/NextWS/HandsetAck.aspx")
                                .WithConstructorArgument("gatewayId", Constant.MOBILEWAY_ROAMING)
                                .WithConstructorArgument("order", 2)
                                .WithConstructorArgument("grade", "SS");
            Bind<ISmsProvider>().To<MockSybaseProvider>()
                                .WithConstructorArgument("username", "inspirus_direct")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "/inspirus_direct/inspirus_direct.sms")
                                .WithConstructorArgument("host", "messaging.sybase365.com")
                                .WithConstructorArgument("replyUrl", "http://203.63.5.170:9098/NextWS/HandsetAck.aspx")
                                .WithConstructorArgument("gatewayId", Constant.MOBILEWAY_DIRECT)
                                .WithConstructorArgument("order", 2)
                                .WithConstructorArgument("grade", "BG");
            Bind<ISmsProvider>().To<MockNextDigitalProvider>()
                                .WithConstructorArgument("username", "inspirus")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "http://srs.nextdigital.com/Enterprise/sendsmsv2")
                                .WithConstructorArgument("route", "default")
                                .WithConstructorArgument("gatewayId", Constant.NEXTDIGITAL_SS)
                                .WithConstructorArgument("order", 1)
                                .WithConstructorArgument("grade", "SS");
            Bind<ISmsProvider>().To<MockNextDigitalProvider>()
                                .WithConstructorArgument("username", "inspirusdirect")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "http://srs.nextdigital.com/Enterprise/sendsmsv2")
                                .WithConstructorArgument("route", "default")
                                .WithConstructorArgument("gatewayId", Constant.NEXTDIGITAL_DIRECT)
                                .WithConstructorArgument("order", 1)
                                .WithConstructorArgument("grade", "BG");
            Bind<ISmsProvider>().To<MockMBloxProvider>()
                                .WithConstructorArgument("username", "Inspirus")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "http://xml3.mblox.com:8180/send")
                                .WithConstructorArgument("gatewayId", Constant.MBLOX)
                                .WithConstructorArgument("order", 3)
                                .WithConstructorArgument("grade", "SS");
            Bind<IMessageProcessor>().To<ManualMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Manual")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);
            Bind<IMessageProcessor>().To<GroupMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Group")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);
            Bind<IMessageProcessor>().To<GroupMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Staff")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);
            Bind<IMessageProcessor>().To<MergeMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Merge")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);

            Bind<IService>().To<InspirusSenderService>()
                            .WithConstructorArgument("settings", settings);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Inspirus.Logic.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Inspirus.Logic.Tests.Serialization
{
    /// <summary>
    /// Summary description for ResponseSerializationTest
    /// </summary>
    [TestClass]
    public class ResponseSerializationTest
    {
        public ResponseSerializationTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private string CreateXml(Sms sms, ProviderResult result, int gatewayId, DateTime now, string phone, int newState)
        {
            dsSchedule ds = new dsSchedule();
            DataTable tb = ds.Tables["response"];
            DataRow r = tb.NewRow();
            tb.Rows.Add(r);
            r["uid_schedule"] = sms.Id;
            r["state"] = newState;
            r["code"] = result.Code;
            r["message"] = result.Description;
            r["time_sent"] = now;
            r["gateway"] = gatewayId;
            r["time_start"] = sms.StartTime;
            if (phone.Equals(""))
            {
                r["recipient_list"] = sms.Recipients;
                r["orig_phone_list"] = sms.OriginalPhoneList;
                string[] p = sms.Recipients.Split(',');
                r["recipient_count"] = p.Length;
            }
            else
            {
                r["recipient_list"] = phone;
                r["orig_phone_list"] = phone;
                r["recipient_count"] = 1;
            }
            r["b2u"] = sms.UidB2U;
            string xml = ds.GetXml();
            return xml;
        }

        [TestMethod]
        public void TestMethod1()
        {
            ProviderResult providerResult = ProviderResult.Ok;
            int gateway = 0, newState = -1;
            string phone = string.Empty;
            DateTime now = DateTime.Now;
            Sms sms = new Sms(DateTime.Today, "12345", "+64220990212", "12345", "Hi, this is test", new List<int> {  0  }, "+64220990212,+64220990213", "123456", false);

            string xml = CreateXml(sms, providerResult, gateway, now, phone, newState);
            
            ScheduleResponse response = new ScheduleResponse
            {
                ScheduleId = sms.Id,
                State = newState,
                Code = providerResult.Code,
                Message = providerResult.Description,
                TimeSent = now,
                Gateway = gateway.ToString(CultureInfo.InvariantCulture),
                TimeStart = sms.StartTime,
                B2U = sms.UidB2U
            };
            if (phone.Equals(string.Empty))
            {
                response.RecipientList = sms.Recipients;
                response.OrigPhoneList = sms.OriginalPhoneList;
                string[] p = sms.Recipients.Split(',');
                response.RecipientCount = p.Length;
            }
            else
            {
                response.RecipientList = response.OrigPhoneList = phone;
                response.RecipientCount = 1;
            }
            ScheduleResponseRoot root = new ScheduleResponseRoot { Response = response };
            using (MemoryStream stream = new MemoryStream()) 
            {
                var settings = new XmlWriterSettings
                    {
                        Encoding = Encoding.GetEncoding(1252),
                        OmitXmlDeclaration = true,
                        Indent = true
                    };
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof (ScheduleResponseRoot));
                    serializer.Serialize(writer, root, new XmlSerializerNamespaces(new[] { new XmlQualifiedName("", "http://tempuri.org/dsSchedule.xsd") }));
                }
                using (StreamReader reader = new StreamReader(stream))
                {
                    stream.Position = 0;
                    string xml2 = reader.ReadToEnd();

                    Assert.AreEqual(xml, xml2);
                }
            }
        }
    }
}
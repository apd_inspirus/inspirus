﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Xml.Serialization;
using Inspirus.Logic.Model;
using InspirusSender;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Phone = Inspirus.Logic.Model.Phone;

namespace Inspirus.Logic.Tests.Serialization
{
    [TestClass]
    public class SmsDeserializationTest
    {
        protected string _smsXml = "<Dataset1 xmlns=\"http://tempuri.org/Dataset1.xsd\">"+
  @"<v_recipients>
    <client_mobile>0220990212</client_mobile>
    <client_firstname>Test</client_firstname>
  </v_recipients>
  <v_schedule_details>
    <b2u_nocdma>false</b2u_nocdma>
    <uid_b2u>3F7C0D85-D1F0-D655-ED6AA81C93A1697A</uid_b2u>
    <smsschedule_message>test sms: in spirit we trust OR in spirit spiritas</smsschedule_message>
    <smsschedule_type>Manual</smsschedule_type>
    <smsschedule_supportsms>false</smsschedule_supportsms>
    <smsschedule_creditsused>0</smsschedule_creditsused>
    <smsschedule_flash>true</smsschedule_flash>
    <smsschedule_grade_type>bg</smsschedule_grade_type>
    <origin_text>test sms: i</origin_text>
    <uid_smsschedule>abcdef0123456789                   </uid_smsschedule>
    <smsschedule_datetime>2013-05-01T00:00:00+12:00</smsschedule_datetime>
  </v_schedule_details>
</Dataset1>";

        [TestMethod]
        public void TestMethod1()
        {
            StringReader reader = new StringReader(_smsXml);
            XmlSerializer serializer = new XmlSerializer(typeof(Schedule));
            Schedule schedule = (Schedule) serializer.Deserialize(reader);
            SmsSchedule smsSchedule = new SmsSchedule(schedule.Details[0], schedule.Recipients);

            InspirusSender.dsSchedule dsSchedule = new InspirusSender.dsSchedule();
            StringReader reader2 = new StringReader(_smsXml);
            dsSchedule.ReadXml(reader2, XmlReadMode.InferSchema);
            SMSSchedule smsSchedule2 = new SMSSchedule(dsSchedule.Tables["v_schedule_details"].Rows[0], dsSchedule, new DataSet());

            Assert.AreEqual(smsSchedule.B2Uid, smsSchedule2.B2UID);
            Assert.AreEqual(smsSchedule.GatewayId, smsSchedule2.GatewayIndex);
            Assert.AreEqual(smsSchedule.Grade, smsSchedule2.Grade);
            Assert.AreEqual(smsSchedule.GroupPhoneList.Count, smsSchedule2.GroupPhoneList.Count);
            for (int index = 0; index < smsSchedule.GroupPhoneList.Count; index++)
            {
                Phone phone = (Phone) smsSchedule.GroupPhoneList[index];
                InspirusSender.Phone phone2 = (InspirusSender.Phone) smsSchedule2.GroupPhoneList[index];
                Assert.AreEqual(phone.FirstName, phone2.firstName);
                Assert.AreEqual(phone.Gateway, phone2.gateway);
                Assert.AreEqual(phone.Number, phone2.phone);
                Assert.AreEqual(phone.OriginalPhone, phone2.originPhone);
                Assert.AreEqual(phone.OtherField, phone2.otherField);
            }
            Assert.AreEqual(smsSchedule.NoCdma, smsSchedule2.NoCDMA == SMSSchedule.YES);
            Assert.AreEqual(smsSchedule.SmsFlash, smsSchedule2.SMSFlash == SMSSchedule.YES);
            Assert.AreEqual(smsSchedule.SmsMessage, smsSchedule2.SMSMessage);
            Assert.AreEqual(smsSchedule.SmsOrigin, smsSchedule2.SMSOrigin);
            Assert.AreEqual(smsSchedule.SmsPhoneCount, smsSchedule2.SMSPhoneCount);
            Assert.AreEqual(smsSchedule.SmsPhoneList, smsSchedule2.SMSPhoneList);
            Assert.AreEqual(smsSchedule.SmsSupport, smsSchedule2.SMSSupport == SMSSchedule.YES);
            Assert.AreEqual(smsSchedule.SmsType, smsSchedule2.SMSType);
            Assert.AreEqual(smsSchedule.UidSchedule, smsSchedule2.uid_schedule);
        }
    }
}
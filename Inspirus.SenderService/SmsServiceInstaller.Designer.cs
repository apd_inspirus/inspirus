﻿using System.Configuration.Install;
using System.ServiceProcess;

namespace Inspirus.SenderService
{
    partial class ServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private System.ServiceProcess.ServiceProcessInstaller SmsServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller SmsServiceInstaller;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            SmsServiceProcessInstaller = new ServiceProcessInstaller
            {
                Account = ServiceAccount.LocalService,
                Username = null,
                Password = null
            };
            SmsServiceInstaller = new System.ServiceProcess.ServiceInstaller
            {
                ServiceName = "Inspirus Sms Sender (SS and BG)",
                Description = "Service sends SMS messages."
            };
            Installers.AddRange(new Installer[]
                                    {
                                        SmsServiceProcessInstaller,
                                        SmsServiceInstaller
                                    });
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceProcess;
using Inspirus.Logic;
using Inspirus.Logic.Implementation;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Service;
using NLog;
using Ninject;
using Ninject.Modules;

namespace Inspirus.SenderService
{
    public partial class SmsSenderService : ServiceBase
    {
        private readonly Logger _logger;
        private readonly IService _service;

        public SmsSenderService()
        {
            _logger = LogManager.GetLogger("Inspirus.Sender");
            InitializeComponent();
            IKernel kernel = new StandardKernel(new SmsSenderServiceModule());
            _service = kernel.Get<IService>();
        }

        protected override void OnStart(string[] args)
        {
            _service.Start();
            _logger.Debug("Start:{0}", DateTime.Now);
        }

        protected override void OnStop()
        {
            _service.Stop();
            _logger.Debug("Stop:{0}", DateTime.Now);
        }
    }

    public sealed class SmsSenderServiceModule : NinjectModule
    {
        public override void Load()
        {
            ISettings settings = new Settings();
            settings.Load("Software\\Nextdata\\Inspirus");
            DataSet gatewayRange = settings.GetGatewayRangeDataSet();

            Bind<IPhonesValidator>().To<PhonesValidator>();
            Bind<ISmsProvider>().To<SybaseProvider>()
                                .WithConstructorArgument("username", "inspirus_roaming")
                                .WithConstructorArgument("password", "rQjr3Hbl")
                                .WithConstructorArgument("url", "/inspirus_roaming/inspirus_roaming.sms")
                                .WithConstructorArgument("host", "messaging.sybase365.com")
                                .WithConstructorArgument("replyUrl", "http://203.63.5.170:9098/NextWS/HandsetAck.aspx")
                                .WithConstructorArgument("gatewayId", Constant.MOBILEWAY_ROAMING)
                                .WithConstructorArgument("order", 2)
                                .WithConstructorArgument("grade", "SS");
            Bind<ISmsProvider>().To<SybaseProvider>()
                                .WithConstructorArgument("username", "inspirus_direct")
                                .WithConstructorArgument("password", "jH9l8MKQ")
                                .WithConstructorArgument("url", "/inspirus_direct/inspirus_direct.sms")
                                .WithConstructorArgument("host", "messaging.sybase365.com")
                                .WithConstructorArgument("replyUrl", "http://203.63.5.170:9098/NextWS/HandsetAck.aspx")
                                .WithConstructorArgument("gatewayId", Constant.MOBILEWAY_DIRECT)
                                .WithConstructorArgument("order", 2)
                                .WithConstructorArgument("grade", "BG");
            Bind<ISmsProvider>().To<NextDigitalProvider>()
                                .WithConstructorArgument("username", "inspirus")
                                .WithConstructorArgument("password", "ab8GCjpz")
                                .WithConstructorArgument("url", "http://srs.nextdigital.com/Enterprise/sendsmsv2")
                                .WithConstructorArgument("route", "default")
                                .WithConstructorArgument("gatewayId", Constant.NEXTDIGITAL_SS)
                                .WithConstructorArgument("order", 1)
                                .WithConstructorArgument("grade", "SS");
            Bind<ISmsProvider>().To<NextDigitalProvider>()
                                .WithConstructorArgument("username", "inspirusdirect")
                                .WithConstructorArgument("password", "ab8GCjpz")
                                .WithConstructorArgument("url", "http://srs.nextdigital.com/Enterprise/sendsmsv2")
                                .WithConstructorArgument("route", "default")
                                .WithConstructorArgument("gatewayId", Constant.NEXTDIGITAL_DIRECT)
                                .WithConstructorArgument("order", 1)
                                .WithConstructorArgument("grade", "BG");
            Bind<ISmsProvider>().To<MBloxProvider>()
                                .WithConstructorArgument("username", "Inspirus")
                                .WithConstructorArgument("password", "9DuF4839")
                                .WithConstructorArgument("url", "http://xml3.mblox.com:8180/send")
                                .WithConstructorArgument("gatewayId", Constant.MBLOX)
                                .WithConstructorArgument("order", 3)
                                .WithConstructorArgument("grade", "SS");
            Bind<IMessageProcessor>().To<ManualMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Manual")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);
            Bind<IMessageProcessor>().To<GroupMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Group")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);
            Bind<IMessageProcessor>().To<GroupMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Staff")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);
            Bind<IMessageProcessor>().To<MergeMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Merge")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);

            Bind<IService>().To<InspirusSenderService>()
                            .WithConstructorArgument("settings", settings);
        }
    }
}
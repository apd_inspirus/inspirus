﻿using System.ComponentModel;
using System.ServiceProcess;


namespace Inspirus.SenderService
{
    [RunInstaller(true)]
    public partial class ServiceInstaller : System.Configuration.Install.Installer
    {
        public ServiceInstaller()
        {
            InitializeComponent();
            SmsServiceInstaller.DisplayName = "Inspirus Sms Sender (SS and BG)";
            SmsServiceInstaller.StartType = ServiceStartMode.Automatic;
        }
    }
}
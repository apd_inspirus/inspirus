﻿using System.Reflection;
using System.ServiceProcess;
using Inspirus.Logic.Extension;
using NLog.Config;

namespace Inspirus.SenderService
{
    static class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                RunService();
                return;
            }
            string arg = args[0].Trim().ToLower();
            switch (arg)
            {
                case "/i":
                case "/u":
                    System.Configuration.Install.ManagedInstallerClass.InstallHelper(new[] { arg, Assembly.GetExecutingAssembly().Location });
                    break;
                default:
                    RunService();
                    break;
            }
        }
        
        static void RunService()
        {
            ConfigurationItemFactory.Default.Targets.RegisterDefinition("ExpiringMsmq", typeof(NlogMsmqTarget));
            ServiceBase[] servicesToRun = new ServiceBase[] 
                { 
                    new SmsSenderService() 
                };
            ServiceBase.Run(servicesToRun);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Messaging;

namespace InspirusSender
{
    class Utils
    {
        static public bool PostToQueue(string qname, string label, string data)
        {
            MessageQueueTransaction mt = new MessageQueueTransaction();
            bool ret = false;
            try
            {
                string path = String.Format(".\\Private$\\{0}", qname);
                MessageQueue mq;
                if (!MessageQueue.Exists(path))
                {
                    mq = MessageQueue.Create(path, true);
                }
                else
                {
                    mq = new MessageQueue(path);
                }
                
                mt.Begin();
                mq.Send(data, label, mt);
                mt.Commit();
                ret = true;
            }
            catch (Exception ex)
            {
                mt.Abort();
                fmSender.Log(null, fmSender.LOG_ERROR, ex.Message);                
            }
            mt.Dispose();
            return ret;
        }


        public static bool PostToRemoteQueue(string label, string data, string message, string uid_b2u)
        {
            MessageQueueTransaction mt = new MessageQueueTransaction();
            bool ret = false;
            try
            {
                string q_host = Properties.Resources.RemoteHost;
                //string path = String.Format("FormatName:direct=tcp:{0}\\Private$\\Queue_incoming", q_host);
                string path = ".\\Private$\\Queue_incoming";
                MessageQueue mq = new MessageQueue(path);

                mt.Begin();
                mq.Send(String.Format("{0}\n\n{1}\n{2}", data, message, uid_b2u), label, mt);
                mt.Commit();
                ret = true;
            }
            catch (Exception ex)
            {
                mt.Abort();
                fmSender.Log(null, fmSender.LOG_ERROR, ex.Message);
            }
            mt.Dispose();
            return ret;
        }
    }
}

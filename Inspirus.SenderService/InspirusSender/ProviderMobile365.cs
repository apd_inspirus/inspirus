using System;
using System.Web;
using System.Text;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace InspirusSender
{
	/// <summary>
	/// Summary description for ProviderMobile365.
	/// </summary>
	public class ProviderMobile365 : SMSProvider
	{		
		private string host, cmd;
        private string uid_b2u;
		
		public ProviderMobile365(string host, string cmd, string userName, string password, 
			SMSMessage sms)
		{
			this.userName = userName;
			this.password = password;			
			this.host = host;
			this.cmd = cmd;
			this.recipientList = sms.Recipient;
			this.senderPhone = sms.Sender;
			this.Message = sms.Message;
			this.SMSFlash = sms.SMSFlash;
            uid_b2u = sms.Uid_b2u;
		}

		private string getURL()
		{
			return String.Format("{0}", url);
		}

		private string getData(string recipientList, string senderPhone, string Message)
		{
            Message = Message.Replace("\r", "<CR>");
            Message = Message.Replace("\n", "<LF>");
            // this to ensure no + in the phons
            // which might happen when routing table is in use
            if (recipientList.IndexOf("+") >= 0)
                recipientList = recipientList.Replace("+", "61");
            // 15 May 06
            if (!recipientList.StartsWith("+"))
            {
                recipientList = "+" + recipientList.Replace(",61", ",+61");
            }
            //string ret = String.Format("Subject={0}\n[MSISDN]\nList={1}\n[MESSAGE]\nText={2}\n[SETUP]\nOriginatingAddr={3}\nMobileNotification=Yes", 
            //        senderPhone, recipientList, Message, senderPhone);
            string ret = String.Format("Subject={0}\n[MSISDN]\nList={1}\n[MESSAGE]\nText={2}\n[SETUP]\nOriginatingAddr={3}\n" +
                "MobileNotification=Yes\n" +
                "AckType=Message\nAckReplyAddress=http://203.63.5.170:9098/NextWS/HandsetAck.aspx",
                //"AckType=Message\nAckReplyAddress=http://inspirusmail.com.au/NextWS/HandsetAck.aspx",
                    senderPhone, recipientList, Message, senderPhone);

			if(this.SMSFlash == 1)
				ret = String.Format("{0}\nClass=0", ret);
			return ret;
		}

		public ReplySimple Send()
		{
            //string sdata = getData(recipientList, senderPhone, Message);
            //string str = userName + ":" + password;
            //string message = String.Format("POST {0} HTTP/1.1\nHOST: {1}\nAuthorization: Basic " +
            //    Convert.ToBase64String(Encoding.UTF8.GetBytes(str)) +
            //    "\nCONTENT-LENGTH:" + sdata.Length + "\n\n{2}", cmd, host, sdata);

            //fmSender.Log(this, fmSender.LOG_TRACE, message);
            //resp = new ReplySimple(0, url);

            // UNDONE uncomment the following code
            
			StringBuilder sb = new StringBuilder();
			//ReplySimple rs = null;
			Byte[] RecvBytes = new Byte[2048];
			String strRetPage = null;
			IPAddress hostAddress = null;
			IPEndPoint hostEndPoint = null;			

			try 
			{
				string sdata = getData(recipientList, senderPhone, Message);
				string str = userName + ":" + password;
				string message = String.Format("POST {0} HTTP/1.1\nHOST: {1}\nAuthorization: Basic " +
					Convert.ToBase64String(Encoding.UTF8.GetBytes(str)) +
					"\nCONTENT-LENGTH:"+ sdata.Length +"\n\n{2}", cmd, host, sdata );

                fmSender.Log(null, fmSender.LOG_TRACE, message);
				//Console.WriteLine(message);

				Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				string server = "messaging.sybase365.com";
				//IPHostEntry hostInfo = Dns.Resolve(server);
                IPHostEntry hostInfo = Dns.GetHostEntry(server);
				IPAddress[] IPaddresses = hostInfo.AddressList;
				hostAddress = IPaddresses[0];
				hostEndPoint = new IPEndPoint(hostAddress, 80);

				s.Connect(hostEndPoint);
				if (s.Connected)
				{
					s.Send(Encoding.UTF8.GetBytes(message));

					// Receive the host home page content and loop until all the data is received.
					Int32 bytes = s.Receive(RecvBytes, RecvBytes.Length, 0);
					
					ASCIIEncoding ascii = new ASCIIEncoding();
					strRetPage = strRetPage + ascii.GetString(RecvBytes);
 
					while (bytes > 0)
					{
						bytes = s.Receive(RecvBytes, RecvBytes.Length, 0);
						strRetPage = strRetPage + ascii.GetString(RecvBytes);
					}

					resp = this.DecodeReply(strRetPage);
				} else 
					resp = new ReplySimple(-998, "Not able to connect");
			
				return resp;	
			} 
			catch (Exception ex)
			{
				fmSender.EventLog(ex.Message);
				resp = new ReplySimple(-999, ex.Message);
			}
            
			return resp;
		}

		/// <summary>
		/// 
		/// decode any reply
		/// 
		/// </summary>
		/// <param name="repl"></param>
		/// <returns></returns>
		private ReplySimple DecodeReply(string repl)
		{
			ReplySimple rs = null;
			int code = -1;
			string msg = repl;
			int p = repl.IndexOf("#Message Receive correctly");
			if( p > 0)
			{
				p = repl.IndexOf("ORDERID");
				int p2 = repl.IndexOf("<br>", p);
				if (p > 0)
				{
					code = 0;
					msg = repl.Substring(p, p2 - p);    // ORDER ID
					rs = new ReplySimple(code, msg);

                    Utils.PostToRemoteQueue("m365", msg, Message, uid_b2u);
				}
			}
			if(code != 0)
				rs = DecodeFailReply(repl);
			return rs;
		}

		/// <summary>
		/// decode any failed reply
		/// </summary>
		/// <param name="repl"></param>
		/// <returns></returns>
		private ReplySimple DecodeFailReply(string repl)
		{
			int code = -1;
			string msg = repl;
			int p = repl.IndexOf("<BODY>");
			ReplySimple rs = null;
			if( p > 0)
			{
				int p2 = repl.IndexOf("</BODY>", p);
				if (p2 > 0)
				{
					msg = repl.Substring(p + 6, p2 - p - 6);
					msg = msg.Replace("<br>", "").Trim();
					rs = new ReplySimple(msg);
				} else 
					rs = new ReplySimple(code, msg);
			}
			else
				rs = new ReplySimple(code, msg);
			return rs;
		}
	}
}

using System;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;
using System.Collections;

namespace InspirusSender
{
    class ProviderMBlox: SMSProvider
    {
        public ProviderMBlox(string url, string userName, string password, SMSMessage sms)
		{
			this.userName = userName;
			this.password = password;
			this.url = url;
			this.SMSFlash = sms.SMSFlash;
		}

        private string EncodeString(string msg)
        {
            //msg = XmlConvert.EncodeName(msg);
            msg = msg.Replace("&", "&#x0026;");
            msg = msg.Replace("\"", "&#x0022;");
            msg = msg.Replace("<", "&#x003c;");
            msg = msg.Replace(">", "&#x003e;");
            msg = msg.Replace("'", "&#x0027;");
            return msg;
        }

		private string getXMLDATA(string recipientList, string senderPhone, string Message) 
		{
            // destination phone is in the following format
            // 61407850388
            // no plus sign in the beginning
            string msgType = "";
            if (SMSFlash == 1)
                msgType = "FlashSMS";
            else
                msgType = "SMS";
            string[] recipients = recipientList.Split(',');
            StringBuilder sb = new StringBuilder();
            foreach (string r in recipients)
            {
                sb.Append(String.Format("<Notification SequenceNumber=\"0\" MessageType=\"{0}\">"+
                "<Message>{1}</Message> " +
                "<Profile>10070</Profile>" +
                "<SenderID Type=\"Alpha\">{2}</SenderID>"+
                "<Subscriber> " +
                "<SubscriberNumber>{3}</SubscriberNumber> " +
                "</Subscriber> " +
                "</Notification> ", msgType, EncodeString(Message), senderPhone, r));
            }
            string xmldata = String.Format("<NotificationRequest Version=\"3.0\">" +
                "<NotificationHeader><PartnerName>{0}</PartnerName>" +
                "<PartnerPassword>{1}</PartnerPassword>" +
                "</NotificationHeader>" +
                "<NotificationList BatchID=\"{3}\">" +
                "{2}" +               
                "</NotificationList> " +
                "</NotificationRequest>", userName, password, sb.ToString(), 1);
            string ret = String.Format("XMLDATA={0}", HttpUtility.UrlEncode(xmldata));			
			return ret;
		}

        public ArrayList Send(string recipientList, string senderPhone, string Message)
        {
            //string url = getURL(recipientList, senderPhone, Message);
            //fmSender.Log(this, fmSender.LOG_TRACE, url);
            //resp = new ReplySimple(0, url);

            // UNDONE uncomment the following code
            ArrayList respList = new ArrayList();

            StringBuilder sb = new StringBuilder();
            try
            {
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                string xmldata = getXMLDATA(recipientList, senderPhone, Message);
                byte[] data = encode.GetBytes(xmldata);

                fmSender.Log(this, fmSender.LOG_TRACE, "url=" + url);

                HttpWebRequest h = (HttpWebRequest)WebRequest.Create(url);
                h.Method = "POST";
                h.ContentType = "application/x-www-form-urlencoded";
                h.ContentLength = data.Length;
                Stream newStream = h.GetRequestStream();                
                newStream.Write(data, 0, data.Length);

                HttpWebResponse rsp = (HttpWebResponse)h.GetResponse();
                Stream receiveStream = rsp.GetResponseStream();
                
                StreamReader readStream = new StreamReader(receiveStream, encode);

                fmSender.Log(this, fmSender.LOG_TRACE, "Response stream received");
                int n = 1024;
                Char[] read = new Char[n];
                // Reads 256 characters at a time.    
                int count = readStream.Read(read, 0, n);
                while (count > 0)
                {
                    // Dumps the 256 characters on a string and displays the string to the console.
                    String str = new String(read, 0, count);
                    sb.Append(str);
                    Console.Write(str);
                    count = readStream.Read(read, 0, n);
                }
                Console.WriteLine("");
                rsp.Close();
                readStream.Close();

                respList = decode(sb.ToString().Trim());
            }
            catch (Exception ex)
            {
                fmSender.EventLog(ex.Message);
                ReplyWithPhone r = new ReplyWithPhone();
                r.ErrCode = -999;
                r.ErrMsg = ex.Message;
                respList.Add(r);
            }

            return respList;
        }


        private ArrayList decode(string raw)
        {
            ArrayList resp = new ArrayList();
            try
            {
                fmSender.Log(this, fmSender.LOG_TRACE, raw);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(raw);
                XmlNodeList retList = doc.GetElementsByTagName("NotificationResult");
                foreach (XmlNode node in retList)
                {
                    XmlNode nSNumber = node.SelectSingleNode("descendant::SubscriberNumber");
                    XmlNode nResultCode = node.SelectSingleNode("descendant::SubscriberResultCode");
                    XmlNode nResultText = node.SelectSingleNode("descendant::SubscriberResultText");

                    if (nSNumber == null || nResultCode == null || nResultText == null)
                        continue;

                    string phone = nSNumber.InnerText.Trim();
                    string retCode = nResultCode.InnerText.Trim();
                    string retText = nResultText.InnerText.Trim();

                    ReplyWithPhone r = new ReplyWithPhone();
                    r.ErrCode = Convert.ToInt32(retCode);
                    r.ErrMsg = retText;
                    r.phone = phone;
                    resp.Add(r);
                }
            }
            catch (Exception ex)
            {
                fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
            }
            return resp;
        }
    }
}

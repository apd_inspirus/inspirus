using System;
using System.Collections.Generic;
using System.Text;
using System.Messaging;

namespace InspirusSender
{
    /// <summary>
    /// process message in a queue
    /// </summary>
    class SingleQueueProcessor
    {
        MessageQueue mq = null;

        public SingleQueueProcessor(string path)
        {
            //fmSender.Log(this, fmSender.LOG_TRACE, String.Format("SingleQueueProcessor\t{0}", path));
            mq = new MessageQueue(path);
            mq.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });
        }

        public void Process()
        {
            for (int i = 0; i < 5000; i++)
            {
                System.Windows.Forms.Application.DoEvents();
                try
                {
                    System.Messaging.Message m = mq.Receive(new TimeSpan(0, 0, 0, 1));
                    string xml = (string)m.Body;
                    SMSMessage sms = new SMSMessage(xml);
                    sms.send();
                }
                catch (MessageQueueException mex)
                {
                    if (mex.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                    {
                        fmSender.Log(this, fmSender.LOG_ERROR, "SQP:" + mex.Message);
                        System.Diagnostics.EventLog.WriteEntry("InspirusSender", mex.Message, System.Diagnostics.EventLogEntryType.Error);
                    }
                    break;
                }
                catch (Exception ex)
                {
                    fmSender.Log(this, fmSender.LOG_ERROR, "SQP:" + ex.Message);
                    System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
                    break;
                }
            }
        }
    }
}

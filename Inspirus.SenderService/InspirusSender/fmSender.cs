
#define NOT_SIMULATION


using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Net;
using System.IO;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using System.Messaging;
using Microsoft.Win32;

namespace InspirusSender
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class fmSender : System.Windows.Forms.Form
    {
		private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.IContainer components;

        private dsSettings ds;
        private ListBox lbLog;
        private Label label2;
        private NumericUpDown nmInterval;
        static private SqlConnection conn1;
        private int timer, QUEUE_N, reveiverTimer, smppProcessTimer;
        private string userid, password, host, dbname, mq_host, mq_name;
        private NumericUpDown mnLimit;
        private int isAuto = 0;
        static public int LOG_TRACE = 3, LOG_WARNING = 2, LOG_ERROR = 1;
        private TextBox txRunningSince;
        private Button button2;
        private Button button1;
        static private StreamWriter logFile;
        static public int SENDER_BUSY = 1, SENDER_IDLE = 0;
        static private int SenderState = SENDER_IDLE, logLevel = LOG_TRACE;
        private PictureBox pictureBox1;
        private bool closeWhenSafe = false;
        private System.Windows.Forms.Timer ReceiverTimer;
        private System.Windows.Forms.Timer SMPPQueueTimer;
        private System.Windows.Forms.Timer ActiveTimer;
        static private Hashtable sentList;

		//private int nMessageSent = 0;

		public fmSender()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            loadSettings();

            // Change by Sury - adding SRS, SMSC and sybase365 gateways.
            lbInfo.Text = "Build 110623";

            conn1 = new SqlConnection();
		    conn1.ConnectionString = @"Data Source=.\dev7sql;Initial Catalog=Inspirus;Integrated Security=True";
            /*conn1.ConnectionString = String.Format(
                "packet size=4096;user id={0};data source={1};persist security info=True;initial catalog={2};password={3}",
                userid, host, dbname, password);*/

            try
            {
                conn1.Open();
            }            
            catch (Exception ex)
            {
                fmSender.EventLog(ex.Message);
            }
            openLogFile();

            sentList = new Hashtable();

			resetCounter(false);
			txRunningSince.Text = DateTime.Now.ToString("dd/MM/yy hh:mm tt");

            if (isAuto == 1)
            {
                timer1.Enabled = true;
                lbLog.Items.Add("Audo Mode");
            }

			ds = new dsSettings();
            
#if SIMULATION
			label7.Text = "SIMULATION";
            button1.Visible = true;
            button2.Visible = true;
#else
            //button1.Visible = false;
            button2.Visible = false;
#endif
            //add by alan 2006_04_24
            timer1.Start();
            //ReceiverTimer.Start();

            // by CLC 2007-05-02
            //SMPPQueueTimer.Start();

            //active net connection with smsc
           // ActiveTimer.Start();
            //end by alan
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fmSender));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txRunningSince = new System.Windows.Forms.TextBox();
            this.mnLimit = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.nmInterval = new System.Windows.Forms.NumericUpDown();
            this.lbInfo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lbLog = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ReceiverTimer = new System.Windows.Forms.Timer(this.components);
            this.SMPPQueueTimer = new System.Windows.Forms.Timer(this.components);
            this.ActiveTimer = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mnLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmInterval)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.txRunningSince);
            this.panel1.Controls.Add(this.mnLimit);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.nmInterval);
            this.panel1.Controls.Add(this.lbInfo);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 97);
            this.panel1.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(463, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 25);
            this.button2.TabIndex = 31;
            this.button2.Text = "Schedule";
            this.button2.Click += new System.EventHandler(this.bnTestSchedule_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(377, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 25);
            this.button1.TabIndex = 30;
            this.button1.Text = "MQ";
            this.button1.Click += new System.EventHandler(this.bnTestMQ_Click);
            // 
            // txRunningSince
            // 
            this.txRunningSince.Location = new System.Drawing.Point(345, 7);
            this.txRunningSince.Name = "txRunningSince";
            this.txRunningSince.Size = new System.Drawing.Size(160, 21);
            this.txRunningSince.TabIndex = 29;
            // 
            // mnLimit
            // 
            this.mnLimit.Location = new System.Drawing.Point(511, 10);
            this.mnLimit.Name = "mnLimit";
            this.mnLimit.Size = new System.Drawing.Size(48, 21);
            this.mnLimit.TabIndex = 28;
            this.mnLimit.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(292, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 16);
            this.label2.TabIndex = 27;
            this.label2.Text = "Seconds";
            // 
            // nmInterval
            // 
            this.nmInterval.Location = new System.Drawing.Point(224, 33);
            this.nmInterval.Name = "nmInterval";
            this.nmInterval.Size = new System.Drawing.Size(62, 21);
            this.nmInterval.TabIndex = 26;
            this.nmInterval.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nmInterval.ValueChanged += new System.EventHandler(this.nmInterval_ValueChanged);
            // 
            // lbInfo
            // 
            this.lbInfo.Location = new System.Drawing.Point(174, 11);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(112, 16);
            this.lbInfo.TabIndex = 21;
            this.lbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 65);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(577, 28);
            this.panel2.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(172, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(160, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "www.nextdata.com.au";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(160, 16);
            this.label8.TabIndex = 0;
            this.label8.Text = "Developed by Nextdata P/L";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(218, 32);
            this.label7.TabIndex = 14;
            this.label7.Text = "Inspirus Sender";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(307, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Clock";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lbLog
            // 
            this.lbLog.FormattingEnabled = true;
            this.lbLog.ItemHeight = 15;
            this.lbLog.Location = new System.Drawing.Point(184, 106);
            this.lbLog.Name = "lbLog";
            this.lbLog.Size = new System.Drawing.Size(388, 169);
            this.lbLog.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(9, 106);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(169, 142);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // ReceiverTimer
            // 
            this.ReceiverTimer.Tick += new System.EventHandler(this.RevieverTimer_Tick);
            // 
            // SMPPQueueTimer
            // 
            this.SMPPQueueTimer.Tick += new System.EventHandler(this.SMPPQueueTimer_Tick);
            // 
            // ActiveTimer
            // 
            this.ActiveTimer.Tick += new System.EventHandler(this.ActiveTimer_Tick);
            // 
            // fmSender
            // 
            this.ClientSize = new System.Drawing.Size(581, 284);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbLog);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.Name = "fmSender";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMS Sender";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mnLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmInterval)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string [] args) 
		{
            // ensure only one instance 
            System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcessesByName("InspirusSender");
            if(processes.Length <= 1)
                Application.Run(new fmSender());
		}

		private void bnSend_Click(object sender, System.EventArgs e)
		{
			
		}


		private void stiButton1_Click(object sender, System.EventArgs e)
		{
		}

		private void resetCounter(Boolean justThread)
		{
		}

        /// <summary>
        /// the main timer loop
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void timer1_Tick(object sender, System.EventArgs e)
		{
			timer1.Enabled = false;

            if (lbLog.Items.Count >= 50)
                lbLog.Items.Clear();

            // CC - 2 Nov 2005
            // shutdown Sender daily to avoid Sender using up too much RAM
            if (closeWhenSafe && DateTime.Now.Hour == 1)
                Application.Exit();

            lbLog.Items.Insert(0, String.Format("Check queues {0}", DateTime.Now));

            lock (logFile)
            {
                try
                {
                    logFile.Close();
                    openLogFile();
                }
                catch (Exception ex)
                {
                    fmSender.EventLog(ex.Message);
                }
            }

            try
            {

                txRunningSince.Text = DateTime.Now.ToString("dd/MM/yy hh:mm:ss");
                Cursor = Cursors.WaitCursor;
                if (conn1.State != ConnectionState.Open)
                {
                    try
                    {
                        conn1.Open();
                        timer1.Interval = timer * 1000;
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
                        timer1.Interval = 60 * 1000;
                    }
                }
                if (conn1.State == ConnectionState.Open)
                {
                    // UNDONE change the second parameter to false for production version
#if (SIMULATION)
                        ConsumeSchedule(mq_host, true);
#else
                        ConsumeSchedule(mq_host, false);
#endif
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
            finally
            {
                Cursor = Cursors.Default;

                if (closeWhenSafe)
                    Application.Exit();
                else
                    timer1.Enabled = true;
            }
		}



		private void tbExit_Click(object sender, System.EventArgs e)
		{
			if(MessageBox.Show("Sure to exit?", "Confirmation", MessageBoxButtons.YesNo,
				MessageBoxIcon.Question) == DialogResult.Yes) 
			{
				Application.Exit();
			}
		}

		private void bnTestDB_Click(object sender, System.EventArgs e)
		{
          
			
		}

		private void bnTestMQ_Click(object sender, System.EventArgs e)
		{
            int gateway = SMSMessage.MBLOX;
            ArrayList gw = new ArrayList();
            gw.Add(gateway);
            string msg = String.Format("  Hi dear!  Test gateway {0} flash", gateway);
            SMSMessage m = new SMSMessage(DateTime.Now, "uid_TEST1", "61401363045", null, msg, gw, 0,
                "127.0.0.1", "0401363045", "uid_B2U");

            m.send();            

		}              


        private string getQueueName(string q_host, string qname)
        {
            if (q_host.Equals("localhost") || q_host.Equals("127.0.0.1"))
                return String.Format(".\\Private$\\{0}", qname);
            else
                return String.Format("FormatName:direct=tcp:{0}\\Private$\\{1}", q_host, qname);
        }


        /// <summary>
        /// process schedules in queue
        /// 
        /// the main class called is
        ///         MessageSchedule
        /// 
        /// </summary>
        /// <param name="q_host"></param>
        /// <param name="isDebug"></param>
        private void ConsumeSchedule(string q_host, bool isDebug)
        {
            //Log(this, fmSender.LOG_TRACE, "\n\n\nenter ConsumeSchedule");
            string queuename = "";
            DataSet dsGatewayRange = new DataSet();
            string path = String.Empty;
            
            queuename = string.Format("{0}DB", mq_name);
            path = getQueueName(q_host, queuename);
            MessageQueue mq = new MessageQueue(path);
            try
            {
                mq.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });
                System.Messaging.Message m = mq.Receive(new TimeSpan(0, 0, 0, 2));
                string xml = (string)m.Body;
                StringReader sr = new StringReader(xml);
                dsGatewayRange.ReadXml(sr, XmlReadMode.InferSchema);
                dsGatewayRange.WriteXml(".\\gateway_range.xml");
            }
            catch (MessageQueueException mex)
            {
                if (mex.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                {
                    fmSender.Log(this, fmSender.LOG_ERROR, mex.Message);
                    System.Diagnostics.EventLog.WriteEntry("InspirusSender", mex.Message, System.Diagnostics.EventLogEntryType.Error);
                }
            }
            catch (Exception ex)
            {
                fmSender.EventLog(ex.Message);
            }

            if (dsGatewayRange.Tables.Count == 0)
            {
                if (File.Exists(".\\gateway_range.xml"))
                    dsGatewayRange.ReadXml(".\\gateway_range.xml");
            }

            //queuename = string.Format("{0}Business", mq_name);
            //path = getQueueName(q_host, queuename);

            //MessageSchedule msBG = new MessageSchedule(path, dsGatewayRange, q_host, isDebug, queuename, 
            //    Convert.ToInt32(mnLimit.Value));
            //if (isDebug)
            //    msBG.ProcessMessages();
            //else
            //{
            //    Thread thBG = new Thread(new ThreadStart(msBG.ProcessMessages));
            //    thBG.Start();
            //}


            queuename = string.Format("{0}Merge", mq_name);
            path = getQueueName(q_host, queuename);

            string queuename2 = string.Format("{0}Business", mq_name);
            string path2 = getQueueName(q_host, queuename2);

            MessageSchedule msMerge = new MessageSchedule(path, dsGatewayRange, q_host, isDebug, queuename,
                Convert.ToInt32(mnLimit.Value));
            MessageSchedule msBusiness = new MessageSchedule(path2, dsGatewayRange, q_host, isDebug, queuename2,
                Convert.ToInt32(mnLimit.Value));
            if (isDebug)
            {
                msMerge.ProcessMessages();
                msBusiness.ProcessMessages();
            }
            else
            {
                Thread thMerge = new Thread(new ThreadStart(msMerge.ProcessMessages));
                thMerge.Start();
                Thread thBusiness = new Thread(new ThreadStart(msBusiness.ProcessMessages));
                thBusiness.Start();
            }

            // SS queue
            for (int i = 0; i < QUEUE_N; i++)
            {
                queuename = string.Format(".\\private$\\sms_{0}{1}", mq_name, i);
                SingleQueueProcessor sp = new SingleQueueProcessor(queuename);
                sp.Process();

                queuename = string.Format("{0}{1}", mq_name, i);
                path = getQueueName(q_host, queuename);

                MessageSchedule ms = new MessageSchedule(path, dsGatewayRange, q_host, isDebug, queuename,
                    Convert.ToInt32(mnLimit.Value));

                if (isDebug)
                    ms.ProcessMessages();
                else
                {
                    Thread th = new Thread(new ThreadStart(ms.ProcessMessages));
                    th.Start();
                }
            }
            //Log(this, fmSender.LOG_TRACE, "exit ConsumeSchedule\n");
        }

		private void bnTestSchedule_Click(object sender, System.EventArgs e)
		{
            ConsumeSchedule(mq_host, true);	
		}

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (closeWhenSafe)
                e.Cancel = false;
            else
            {
                closeWhenSafe = true;
                e.Cancel = true;
                label7.Text = "Closing...";
                Application.DoEvents();
            }
            //add by alan 2006_04_28
            ReceiverTimer.Stop();
            SMPPQueueTimer.Stop();
            ActiveTimer.Stop();
            ProviderMBloxSMPP.CloseStream();
            //end by alan
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            string oldName = "sentlist.txt";
            string newName = String.Format("sentlist_{0}.txt", DateTime.Now.ToString("MMdd"));
            if (File.Exists(newName))
                File.Delete(newName);
            if (File.Exists(oldName))
            {
                File.Move(oldName, newName);
            }
            fmSender.Log(this, LOG_TRACE, "Exit Sender");
            Application.Exit();
        }

        private void nmInterval_ValueChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// this function will be called before any message is sent
        /// to ensure this schedule has not been deleted
        /// 
        /// make sure the func is within a lock{}
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="b2u"></param>
        /// <returns></returns>
        static public bool IsScheduleDeleted(Object p, string uid, string b2u){
            bool ret = true;

            lock (conn1)
            {
                try
                {
                    string sql = String.Format("SELECT smsschedule_creditsused FROM smsschedule WHERE uid_smsschedule = '{0}' AND uid_b2u = '{1}'",
                        uid, b2u);
                    fmSender.Log(conn1, fmSender.LOG_TRACE, sql);

                    SqlCommand cmd = conn1.CreateCommand();
                    cmd.CommandText = sql;
                    SqlDataReader rs = cmd.ExecuteReader();
                    if (rs.Read())
                    {
                        ret = false;
                    }
                    rs.Close();
                    cmd.Dispose();
                }
                catch (Exception ex)
                {
                    // if fail to connect to DB
                    // the sms will be sent
                    fmSender.Log(conn1, fmSender.LOG_ERROR, ex.Message);
                    System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
                    ret = false;
                }
            }
            return ret;
        }
        /// <summary>
        /// get a value from the registry
        /// if a value is not found, get the default
        /// 
        /// </summary>
        /// <param name="rk"></param>
        /// <param name="key"></param>
        /// <param name="def"></param>
        /// <returns></returns>
        private string getVal(RegistryKey rk, string key, string def)
        {
            string ret = def;
            if (rk.GetValue(key) != null)
                ret = (string)rk.GetValue(key);

            return ret;
        }

        /// <summary>
        /// load the settings
        /// save the settings at the same time
        /// change the setting using Regedit
        /// 
        /// </summary>
        private void loadSettings()
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey k = rk.CreateSubKey("Software\\Nextdata\\Inspirus");
            userid = getVal(k, "db_user", "inspirus");
            password = getVal(k, "db_pass", "w84r9g7a");
            host = getVal(k, "db_host", "192.168.91.11");
            dbname = getVal(k, "db_name", "Inspirus");
            mq_host = getVal(k, "mq_host", "127.0.0.1");
            mq_name = getVal(k, "mq_name", "Queue_");
            QUEUE_N = Convert.ToInt32(getVal(k, "queue_count", "3"));
            timer = Convert.ToInt32(getVal(k, "sender_timer_sec", "3"));
            reveiverTimer = Convert.ToInt32(getVal(k, "receiver_timer_sec", "5"));
            smppProcessTimer = Convert.ToInt32(getVal(k, "smpp_timer_sec", "30"));
            isAuto = Convert.ToInt32(getVal(k, "autorun", "0"));
            logLevel = Convert.ToInt32(getVal(k, "log_level", "1"));

            int limit = Convert.ToInt32(getVal(k, "limit", "1"));
            /*
            k.SetValue("db_user", userid);
            k.SetValue("db_pass", password);
            k.SetValue("db_host", host);
            k.SetValue("db_name", dbname);
            k.SetValue("mq_host", mq_host);
            k.SetValue("mq_name", mq_name);
            k.SetValue("queue_count", Convert.ToString(QUEUE_N));
            k.SetValue("sender_timer_sec", Convert.ToString(timer));
            k.SetValue("receiver_timer_sec", Convert.ToString(reveiverTimer));
            k.SetValue("smpp_timer_sec", Convert.ToString(smppProcessTimer));
            k.SetValue("limit", Convert.ToString(limit));
            k.SetValue("autorun", Convert.ToString(isAuto));
            k.SetValue("log_level", Convert.ToString(logLevel));
*/
            mnLimit.Value = limit;
            nmInterval.Value = timer;

            timer1.Interval = timer * 1000;
            ReceiverTimer.Interval = reveiverTimer * 1000;
            SMPPQueueTimer.Interval = smppProcessTimer * 1000;
            ActiveTimer.Interval = 30 * 1000;
            
            
            string url1 = "smpp5.mblox.com";
            string url2 = "smpp6.mblox.com";

            string systemId = "Inspirus";
            string systemType = "SMPP";
            string _password = "9DuF4839";
            int port = 3216;
            ProviderMBloxSMPP.InitialParameters(url1, url2, port, systemId, _password, systemType);
        }

        static public void EventLog(string msg)
        {
            System.Diagnostics.EventLog.WriteEntry("InspirusSender", msg, 
                System.Diagnostics.EventLogEntryType.Error);            
        }


        private void openLogFile()
        {
            try
            {
                
                string fname = String.Format("{0}\\sender_{1}.log", Directory.GetCurrentDirectory(), 
                    DateTime.Now.ToString("yyMMdd"));
                string fname2 = String.Format("{0}\\sender_{1}b.log", Directory.GetCurrentDirectory(),
                    DateTime.Now.ToString("yyMMdd"));
                if (File.Exists(fname))
                {
                    FileInfo f = new FileInfo(fname);
                    if (f.Length > (1024 * 1024 * 5))
                    {
                        if (File.Exists(fname2))
                            File.Delete(fname2);
                        f.MoveTo(fname2);
                    }
                }
                logFile = new StreamWriter(fname, true);
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
        }

        static public void Log(Object p, int logLevel, string msg)
        {
            /*if (logLevel > fmSender.logLevel)
                return;

            lock (logFile)
            {
                string level = "TRACE";
                if (logLevel == LOG_ERROR)
                    level = "ERROR";
                else if (logLevel == LOG_WARNING)
                    level = "WARN";

                try
                {                    
                    logFile.WriteLine(String.Format("{0}\t{1}\t{2}", DateTime.Now.ToString(), level, msg));
                    logFile.Flush();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
                }
            }*/
            
        }

        static public void setState(int state){
            fmSender.SenderState = state;
        }

        static public bool isAlreadySent(SMSSchedule sch)
        {
            bool ret = true;
            lock (sentList)
            {
                //string pkey = String.Format("{0}={1}", sch.uid_schedule, sch.scheduleDateTime);
                string pkey = String.Format("{0}={1}", sch.uid_schedule, sch.SMSMessage.GetHashCode());
                string fname = System.IO.Directory.GetCurrentDirectory() + "\\sentlist.txt";
                if (sentList.Count == 0)
                {                    
                    if (File.Exists(fname))
                    {
                        // load from send file
                        try
                        {
                            StreamReader sr = new StreamReader(fname);
                            string sline = "";
                            while ((sline = sr.ReadLine()) != null)
                            {
                                string[] s = sline.Split(';');
                                if (s.Length >= 2)
                                {
                                    if(!sentList.ContainsKey(s[0]))
                                        sentList.Add(s[0], s[1]);
                                }
                            }
                            sr.Close();
                        }
                        catch (Exception ex)
                        {
                            fmSender.Log(ex, fmSender.LOG_ERROR, ex.Message);
                            EventLog(ex.Message);
                        }
                    }
                }
                if (sentList.ContainsKey(pkey))
                    ret = true;
                else
                {
                    ret = false;
                    sentList.Add(pkey, Convert.ToString(DateTime.Now.Ticks));
                    // write back to send file
                    try
                    {
                        StreamWriter writer = new StreamWriter(fname);
                        foreach (object key in sentList.Keys)
                        {
                            long ticks = Convert.ToInt64(sentList[key]);
                            TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - ticks);
                            if (ts.Days < 1)
                                writer.WriteLine(String.Format("{0};{1}", key, ticks));
                        }
                        writer.Close();
                    }
                    catch (Exception ex)
                    {
                        fmSender.Log(ex, fmSender.LOG_ERROR, ex.Message);
                        EventLog(ex.Message);
                    }
                }
            }
            return ret;
        }

        private void RevieverTimer_Tick(object sender, EventArgs e)
        {
            //string url1 = "smpp5.mblox.com";
            //string url2 = "smpp6.mblox.com";
            //string systemId = "Inspirus";
            //string systemType = "SMPP";
            //string password = "9DuF4839";
            //int port = 3216;
            //get MO and Notification from server 1 and 2
            try
            {
                ReceiverTimer.Enabled = false;

                //ProviderMBloxSMPP p1 = new ProviderMBloxSMPP();
                //ArrayList receiveList1 = p1.Receive();
                //p1.Receive();
            }
            finally
            {
                ReceiverTimer.Enabled = true;
            }
            
        }

        private void SMPPQueueTimer_Tick(object sender, EventArgs e)
        {
            //try
            //{
            //    SMPPQueueTimer.Enabled = false;

            //    string path = getQueueName(mq_host, "sms_smpp");
            //    SMppQueueProcessor processor = new SMppQueueProcessor(path);
            //    processor.Process();
            //}
            //finally
            //{
            //    SMPPQueueTimer.Enabled = true;
            //}
        }

        private void ActiveTimer_Tick(object sender, EventArgs e)
        {
            //try
            //{
            //    ActiveTimer.Enabled = false;
            //    ProviderMBloxSMPP.KeepActive();
            //}
            //finally
            //{
            //    ActiveTimer.Enabled = true;
            //}
        }

	}
}

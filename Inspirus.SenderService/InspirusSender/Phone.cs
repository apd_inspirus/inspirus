using System;

namespace InspirusSender
{
	/// <summary>
	/// Summary description for Phone.
	/// </summary>
	public class Phone
	{
        public string phone, originPhone, firstName, otherField;
		public int gateway;

		public Phone(string phone, int gateway, string originPhone, string firstName, string otherField)
		{
			this.phone = phone;
			this.gateway = gateway;
			this.originPhone = originPhone;
            this.firstName = firstName;
            this.otherField = otherField;
		}
	}
}

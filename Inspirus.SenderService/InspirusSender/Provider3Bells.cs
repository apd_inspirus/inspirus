using System;
using System.Web;
using System.Net;
using System.IO;
using System.Text;


namespace InspirusSender
{
	/// <summary>
	/// Summary description for Provider3Bells.
	/// </summary>
	public class Provider3Bells : SMSProvider
	{
		public Provider3Bells(string userName, string password, SMSMessage sms)
		{
			this.userName = userName;
			this.password = password;
            
            // production URL
			//this.url = "http://www.3bells.com/main/servlet/SendSMS";
            url = "http://www.dmssms.com/HttpSMSv3";
            // UNDONE testing 3bells URL
            //this.url = "221.133.201.77";

			this.SMSFlash = sms.SMSFlash;
		}

		private string getURL(string recipientList, string senderPhone, string Message) 
		{
            recipientList = recipientList.Replace("+", "61");
            if (senderPhone.StartsWith("+"))
            {
                if (senderPhone.StartsWith("+61"))
                    senderPhone = String.Format("0{0}", senderPhone.Substring(3));
                else
                    senderPhone = senderPhone.Substring(1);
            }
			//string ret = String.Format("{0}?UN={1}&PW={2}&NB={3}&ON={4}&ME={5}&TY=T", 
            string ret = String.Format("{0}?USER_NAME={1}&PASSWORD={2}&RECIPIENT={3}&ORIGINATOR={4}&MESSAGE_TEXT={5}&TY=T", 
					url, userName, password, recipientList, senderPhone, 
					HttpUtility.UrlEncode(Message));			
			if (SMSFlash == 1)
				ret = String.Format("{0}&FL=F", ret);
			return ret;
		}

		public ReplySimple Send(string recipientList, string senderPhone, string Message)	
		{
            //string url = getURL(recipientList, senderPhone, Message);
            //fmSender.Log(this, fmSender.LOG_TRACE, url);
            //resp = new ReplySimple(0, url);

            // UNDONE uncomment the following code
            
			string url = getURL(recipientList, senderPhone, Message);

            fmSender.Log(this, fmSender.LOG_TRACE, "url=" + url);
			//resp = SimpleSend(url, recipientList, senderPhone, Message);


            // added 11 March 06
            StringBuilder sb = new StringBuilder();
            try
            {
                //string url = getURL(recipientList, senderPhone, Message);
                HttpWebRequest h = (HttpWebRequest)WebRequest.Create(url);
                h.Method = "GET";
                h.ContentType = "application/x-www-form-urlencoded";
                HttpWebResponse rsp = (HttpWebResponse)h.GetResponse();
                Stream receiveStream = rsp.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(receiveStream, encode);

                Console.WriteLine("\r\nResponse stream received.");
                int n = 1024;
                Char[] read = new Char[n];
                // Reads 256 characters at a time.    
                int count = readStream.Read(read, 0, n);
                Console.WriteLine("HTML...\r\n");
                while (count > 0)
                {
                    // Dumps the 256 characters on a string and displays the string to the console.
                    String str = new String(read, 0, count);
                    sb.Append(str);
                    Console.Write(str);
                    count = readStream.Read(read, 0, n);
                }
                Console.WriteLine("");
                rsp.Close();
                readStream.Close();

                string[] pks = sb.ToString().Split(' ');
                if (pks.Length >= 4)
                    resp = new ReplySimple(0, pks[1]);
                else 
                    resp = new ReplySimple(-998, sb.ToString());


               // resp = new ReplySimple(sb.ToString());
            }
            catch (Exception ex)
            {
                fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
                fmSender.EventLog(ex.Message);
                resp = new ReplySimple(-999, ex.Message);
            }
            // end // added 11 March 06

            return resp;
		}
	
	}
}

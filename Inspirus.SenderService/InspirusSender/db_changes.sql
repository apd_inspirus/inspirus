alter table smsschedule
add schedule_state smallint default 0,
send_datetime datetime;




drop view v_schedule_details

create view v_schedule_details
as 
SELECT gateway.gateway_id, b2u.b2u_nocdma, smsschedule.uid_b2u, smsschedule.smsschedule_message, 
smsschedule.smsschedule_type, smsschedule.smsschedule_sendto, smsschedule.smsschedule_supportsms, 
smsschedule.smsschedule_creditsused, smsschedule.smsschedule_flash, smsschedule.smsschedule_grade_type, origin.origin_text , smsschedule.uid_smsschedule
FROM smsschedule INNER JOIN origin ON smsschedule.uid_origin = origin.uid_origin 
INNER JOIN b2u ON smsschedule.uid_b2u = b2u.uid_b2u LEFT JOIN gateway 
ON b2u.uid_gateway = gateway.uid_gateway 



create view v_recipients
as
SELECT client_mobile, a.client_firstname FROM clients a, smsscheduledclients b
WHERE a.uid_client = b.uid_client 
AND b.scheduleclient_sent = 0

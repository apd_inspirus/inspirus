using System;

namespace InspirusSender
{
	/// <summary>
	/// Summary description for ReplySimple.
	/// </summary>
	public class ReplySimple : ReplyBase
	{
		public ReplySimple(int errCode, string errMsg)
		{
			this.errCode = errCode;
			this.errMsg = errMsg;
		}
		
		public ReplySimple(string response)
		{            
			Decode(response, " ");
		}

		public ReplySimple(string response, string delimiter)
		{
			Decode(response, delimiter);
		}

		private void Decode(string response, string delimiter)
		{
			this.RawResponse = response;
			this.errCode = 0;
			this.errMsg = String.Empty;
			this.Response = response;
			int p = Response.IndexOf(delimiter);
			if (p >= 0)
			{
				errCode = Convert.ToInt32(Response.Substring(0, p));
				this.errMsg = Response.Substring(p).Trim();
			}
			else
			{
                if (response.Trim().Equals("100"))
                {
                    this.errCode = 100;
                    this.errMsg = "Sent Successfully!";
                }
                else
                {
                    this.errCode = -9901;
                    this.errMsg = "Blank response!";
                }
			}
		}
	}
}

﻿﻿using System;
using System.Web;
using System.Text;
using System.Net;
using System.IO;

namespace InspirusSender
{
    /// <summary>
    /// Summary description for ProviderRouto.
    /// </summary>
    public class ProviderSMSCentral : SMSProvider
    {
        public ProviderSMSCentral(string url, string userName, string password, SMSMessage sms)
        {
            this.userName = userName;
            this.password = password;
            this.url = url;
        }

        private string getURL(string recipientList, string senderPhone, string Message)
        {
            recipientList = recipientList.Replace("+", "61");
            if (senderPhone.StartsWith("+"))
            {
                if (senderPhone.StartsWith("+61"))
                    senderPhone = String.Format("0{0}", senderPhone.Substring(3));
                else
                    senderPhone = senderPhone.Substring(1);
            }
            string ret = String.Format("{0}?USER_NAME={1}&PASSWORD={2}&RECIPIENT={3}&ORIGINATOR={4}&MESSAGE_TEXT={5}",
                url, userName, password, recipientList, senderPhone,
                HttpUtility.UrlEncode(Message));
            return ret;
        }

        public ReplySimple Send(string recipientList, string senderPhone, string Message)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                string url = getURL(recipientList, senderPhone, Message);

                fmSender.Log(this, fmSender.LOG_TRACE, "url=" + url);

                HttpWebRequest h = (HttpWebRequest)WebRequest.Create(url);
                h.Method = "GET";
                h.ContentType = "application/x-www-form-urlencoded";
                HttpWebResponse rsp = (HttpWebResponse)h.GetResponse();
                Stream receiveStream = rsp.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(receiveStream, encode);

                Console.WriteLine("\r\nResponse stream received.");
                int n = 1024;
                Char[] read = new Char[n];
                // Reads 256 characters at a time.    
                int count = readStream.Read(read, 0, n);
                Console.WriteLine("HTML...\r\n");
                while (count > 0)
                {
                    // Dumps the 256 characters on a string and displays the string to the console.
                    String str = new String(read, 0, count);
                    sb.Append(str);
                    Console.Write(str);
                    count = readStream.Read(read, 0, n);
                }
                Console.WriteLine("");
                rsp.Close();
                readStream.Close();

                resp = decode(sb.ToString().Trim());
            }
            catch (Exception ex)
            {
                fmSender.EventLog(ex.Message);
                resp = new ReplySimple(-999, ex.Message);
            }

            return resp;
        }

        private ReplySimple decode(string raw)
        {
            if (raw.Equals("0"))
            {
                resp = new ReplySimple(0, "sending successfuly");
            }
            else if (raw.Equals("1"))
            {
                resp = new ReplySimple(0, "sending successfuly");
            }
            else
            {
                resp = new ReplySimple(101, "sending failed");
            }
            return resp;
        }

    }
}

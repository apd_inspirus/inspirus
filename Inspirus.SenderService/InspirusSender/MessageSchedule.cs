using System;
using System.Messaging;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Threading;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Web;
using NLog;

namespace InspirusSender
{
	/// <summary>
	/// MessageSchedule will be run in each own thread
	/// it is responsible of get message from the queue
	/// </summary>
	public class MessageSchedule
	{
		private string QueuePath, shortQName;
		private DataSet dsGatewayRange ;
		private string q_host;
        private bool isSimulate = true;
        private int limit;
        private int BATCH_LIMIT = 50, MERGE_CHANNEL = 3;
	    private readonly Logger _logger = LogManager.GetLogger("ParseLogger");

		public MessageSchedule(string queuePath, DataSet ds, string q_host, bool isSimulate, 
            string shortQName, int limit)
		{
			this.QueuePath = queuePath;
			dsGatewayRange = ds;
			this.q_host = q_host;
            this.isSimulate = isSimulate;
            this.shortQName = shortQName;
            this.limit = limit;
            
			//if(this.conn.State != ConnectionState.Open)
			//	this.conn.Open();
		}

		public void ProcessMessages()
		{
            bool keepGoing = false;
            try
            {
                //fmSender.Log(this, fmSender.LOG_TRACE, String.Format("Retrieve msg from {0}", QueuePath));
                MessageQueue mq = new MessageQueue(QueuePath);
                mq.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });

                //System.Messaging.Message[] ms = mq.GetAllMessages();
                //foreach(System.Messaging.Message m in ms)
                for (int i = 0; i < limit; i++)
                {
                    System.Windows.Forms.Application.DoEvents();
                    DateTime dt = DateTime.Now;
                    dsSchedule ds = new dsSchedule();
                    
                    //System.Messaging.Message m = mq.Receive(new TimeSpan(0, 0, 0, 3));
                    System.Messaging.Message m = mq.Receive(new TimeSpan(0, 0, 0, 2));
                    string xml = (string)m.Body;
                    StringReader sr = new StringReader(xml);
                    ds.ReadXml(sr, XmlReadMode.InferSchema);

                    SMSSchedule sch = getSchedule(ds);
                    _logger.Info("SMS is parsed. Id: {0}, gateway-in: {1}, grade: {2}", sch.uid_schedule, sch.GatewayIndex, sch.Grade);
                    // check if this schedule is already sent
                    //if (fmSender.isAlreadySent(sch))
                    //{
                    //    continue;
                    //}

                    // store the message in outgoing queue
                    if (isSimulate)
                    {
                        PostToQueue(null, q_host, "Queue_outgoing", sch.uid_schedule, xml);
                    }

                    fmSender.Log(this, fmSender.LOG_TRACE, String.Format("schedule type={0}; gw={1}; grade={2}", 
                        sch.SMSType, sch.GatewayIndex, sch.Grade));

                    // get a list of gateway
                    ArrayList gateways = getGateways(sch.Grade, sch.GatewayIndex);
                    foreach (int gw in gateways)
                    {
                        fmSender.Log(this, fmSender.LOG_TRACE, String.Format("\t\tgatway={0}", gw));
                    }

                    // ensure the gateway of all phones uses the first gateway
                    if (gateways.Count > 0)
                    {
                        foreach (Phone p in sch.GroupPhoneList)
                        {
                            p.gateway = Convert.ToInt32(gateways[0]);
                        }
                        //if (Convert.ToInt32(gateways[0]) == SMSMessage.MBLOX)
                        //    BATCH_LIMIT = 10;
                        //else
                            BATCH_LIMIT = 50;
                    }
                    //// add by Alan 20 April,2006 11:46
                    ////to divert message to smpp
                    //// store mesage to mq for smpp processing 
                    ////__Begin_By_Alan
                    //if (Convert.ToInt32(gateways[0]) == SMSMessage.MBLOX)
                    //{
                    //    //push to MQ for SMPPMaster
                    //    PostToQueue(null, q_host, "Queue_SMPPMaster", sch.uid_schedule, xml);
                    //    continue;
                    //}
                    ////__End_By_Alan

                    // send SMS
                    if (sch.SMSType.Equals("Manual"))
                    {
                        string plist = ValidatePhones(sch.SMSPhoneList);
                        SMSMessage sms = new SMSMessage(dt, sch.uid_schedule, plist, sch.SMSOrigin,
                            sch.SMSMessage, gateways, sch.SMSFlash, q_host, sch.SMSPhoneList, sch.B2UID);
                        
                        sms.send();

                        //Thread th = new Thread(new ThreadStart(sms.send));						
                        //th.Start();
                    }
                    else if (sch.SMSType.Equals("Group") || sch.SMSType.Equals("Staff"))
                    {                        
                        int counter = 0;
                        int lastGateway = -99;
                        StringBuilder sb = new StringBuilder();
                        fmSender.setState(fmSender.SENDER_BUSY);
                        // compile all phones of the same gateway up to 20 numbers and send as a lot
                        MessageQueueTransaction mt = new MessageQueueTransaction();
                        mt.Begin();
                        try
                        {
                            foreach (Phone p in sch.GroupPhoneList)
                            {
                                if (lastGateway != p.gateway)
                                {
                                    lastGateway = p.gateway;
                                    if (sb.Length > 0)
                                    {
                                        sendGroup(mt, dt, sch, lastGateway, sb.ToString());
                                        sb.Length = 0;
                                        counter = 0;
                                    }
                                }
                                sb.Append(p.phone);
                                sb.Append(",");
                                counter++;
                                if (counter >= BATCH_LIMIT)
                                {
                                    sendGroup(mt, dt, sch, lastGateway, sb.ToString());
                                    sb.Length = 0;
                                    counter = 0;
                                }
                            }
                            if (sb.Length > 0)
                                sendGroup(mt, dt, sch, lastGateway, sb.ToString());

                            mt.Commit();
                            // so we will stop this thread and start sending group sms
                            i = 1000;
                            keepGoing = true;
                        }
                        catch (Exception ex)
                        {
                            keepGoing = false;
                            mt.Abort();
                            // put the schedule back to the queue
                            //PostToQueue(null, q_host, shortQName, sch.uid_schedule, xml);
                            fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
                            System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
                            //Thread.Sleep(5000);
                        }
                        finally
                        {
                            mt.Dispose();
                            fmSender.setState(fmSender.SENDER_IDLE);
                        }
                        if (keepGoing)
                        {
                            SingleQueueProcessor sp = new SingleQueueProcessor(getQueueName("sms_"));
                            sp.Process();
                            
                        }
                    }                   
                    else if (sch.SMSType.StartsWith("Merge"))
                    {
                        int channel = 0;
                        int lastGateway = -1;
                        int counter = 0;
                        fmSender.setState(fmSender.SENDER_BUSY);
                        MessageQueueTransaction mt = new MessageQueueTransaction();
                        mt.Begin();
                        try
                        {
                            foreach (Phone p in sch.GroupPhoneList)
                            {
                                string msg = this.mergeField("name", sch.SMSMessage, p, p.firstName);
                                
                                msg = this.mergeField("merge", msg, p, p.otherField);

                                //string msg = sch.SMSMessage;
                                //string umsg = sch.SMSMessage.ToLower();
                                //int p1 = umsg.IndexOf("%21name%21");
                                //int p2 = umsg.IndexOf("!name!");
                                //fmSender.Log(this, fmSender.LOG_TRACE, String.Format("p1={0};p2={1};msg={2}", p1, p2, umsg));
                                //if (p1 >= 0)
                                //{
                                //    string fld = "%21NAME%21";
                                //    fld = msg.Substring(p1, fld.Length);
                                //    msg = msg.Replace(fld, p.firstName);
                                //    fmSender.Log(this, fmSender.LOG_TRACE, String.Format("fld={0};msg={1}", fld, msg));
                                //}
                                //if (p2 >= 0)
                                //{
                                //    string fld = "!NAME!";
                                //    fld = msg.Substring(p2, fld.Length);
                                //    msg = msg.Replace(fld, p.firstName);
                                //    fmSender.Log(this, fmSender.LOG_TRACE, String.Format("fld={0};msg={1}", fld, msg));
                                //}


                                if (lastGateway != p.gateway)
                                {
                                    lastGateway = p.gateway;
                                    gateways = getGateways(sch.Grade, p.gateway);
                                }
                                string plist = p.phone;
                                if (isSimulate)
                                    plist = "+12345";
                                SMSMessage sms = new SMSMessage(dt, sch.uid_schedule, plist, sch.SMSOrigin,
                                    msg, gateways, sch.SMSFlash, q_host, p.originPhone, sch.B2UID);
                                // limit to 3 merge channels
                                string tempName = string.Format("_{0}_sms_{1}", (channel % MERGE_CHANNEL), shortQName);
                                PostToQueue(mt, q_host, tempName, sch.uid_schedule, sms.Xml);
                                channel++;
                                //PostToQueue(mt, q_host, "sms_" + shortQName, sch.uid_schedule, sms.Xml);

                                //Thread th = new Thread(new ThreadStart(sms.send));						
                                //th.Start();

                                //if (counter % 10 == 0)
                                //{
                                //    counter = 0;
                                //    Thread.Sleep(10000);

                                //}
                                counter++;
                            }
                            mt.Commit();

                            // so we will stop this thread and start sending group sms
                            i = 1000;
                            keepGoing = true;
                        }
                        catch (Exception ex)
                        {
                            keepGoing = false;
                            mt.Abort();
                            // put the schedule back to the queue
                            //PostToQueue(null, q_host, shortQName, sch.uid_schedule, xml);
                            fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
                            System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
                            //Thread.Sleep(5000);
                        }
                        finally
                        {
                            mt.Dispose();
                            fmSender.setState(fmSender.SENDER_IDLE);
                        }
                        if (keepGoing)
                        {
                            // 3 merge channels
                            for (int ch = 0; ch < MERGE_CHANNEL; ch++)
                            {
                                string tempName = string.Format("_{0}_sms_", ch);
                                SingleQueueProcessor sp = new SingleQueueProcessor(getQueueName(tempName));
                                Thread th = new Thread(new ThreadStart(sp.Process));
                                th.Start();
                            }
                        }
                    }
                    else
                    {
                        fmSender.Log(this, fmSender.LOG_ERROR, "Unknown schedule. " + sch.uid_schedule);
                        System.Diagnostics.EventLog.WriteEntry("InspirusSender", "Unknown schedule. " + sch.uid_schedule, System.Diagnostics.EventLogEntryType.Error);
                    }

                }
            }
            catch (MessageQueueException mex)
            {
                if (mex.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                {
                    fmSender.Log(this, fmSender.LOG_ERROR, mex.Message);
                    System.Diagnostics.EventLog.WriteEntry("InspirusSender", mex.Message, System.Diagnostics.EventLogEntryType.Error);
                }
            }
            catch (Exception ex)
            {
                fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
                System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
		}

        /// <summary>
        /// merge fields
        /// </summary>
        /// <param name="field"></param>
        /// <param name="sch"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        private string mergeField(string field, string message, Phone p, string newVal)
        {
            string lowerField = field.ToLower();
            string upperField = field.ToUpper();
            string msg = message;
            string umsg = message.ToLower();
            int p1 = umsg.IndexOf("%21"+ lowerField + "%21");
            int p2 = umsg.IndexOf("!" + lowerField + "!");
            fmSender.Log(this, fmSender.LOG_TRACE, String.Format("p1={0};p2={1};msg={2}", p1, p2, umsg));
            if (p1 >= 0)
            {
                string fld = "%21" + upperField + "%21";
                fld = msg.Substring(p1, fld.Length);
                msg = msg.Replace(fld, newVal);
                fmSender.Log(this, fmSender.LOG_TRACE, String.Format("fld={0};msg={1}", fld, msg));
            }
            if (p2 >= 0)
            {
                string fld = "!" + upperField + "!";
                fld = msg.Substring(p2, fld.Length);
                msg = msg.Replace(fld, newVal);
                fmSender.Log(this, fmSender.LOG_TRACE, String.Format("fld={0};msg={1}", fld, msg));
            }
            return msg;
        }

        private string getQueueName(string prefix)
        {
            if(q_host.Equals("localhost") || q_host.Equals("127.0.0.1"))
                return String.Format(".\\Private$\\{0}{1}", prefix, shortQName);
            else
                return String.Format("FormatName:direct=tcp:{0}\\Private$\\{1}{2}", q_host, prefix, shortQName);
        }


        private string getQueueName(string q_host, string qname)
        {
            if (q_host.Equals("localhost") || q_host.Equals("127.0.0.1"))
                return String.Format(".\\Private$\\{0}", qname);
            else
                return String.Format("FormatName:direct=tcp:{0}\\Private$\\{1}", q_host, qname);
        }

        private void PostToQueue(MessageQueueTransaction mt, string q_host, string path, string uid, string xml)
        {
            bool isSelfManaged = false;
            if (mt == null)
            {
                mt = new MessageQueueTransaction();
                isSelfManaged = true;
            }
            try
            {
                path = getQueueName(q_host, path);
                MessageQueue mq = new MessageQueue(path);            
                
                if(isSelfManaged)
                    mt.Begin();
                mq.Send(xml, uid, mt);

                if (isSelfManaged)
                    mt.Commit();
            }
            catch (Exception ex)
            {
                if (isSelfManaged)
                    mt.Abort();
                fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
                fmSender.EventLog(ex.Message);
            }
            if (isSelfManaged)
                mt.Dispose();
        }
	
		/// <summary>
		/// send phone list
		/// </summary>
		/// <param name="sch"></param>
		/// <param name="gw"></param>
		/// <param name="plist"></param>
		private void sendGroup(MessageQueueTransaction mt, DateTime dt, SMSSchedule sch, int gw, string plist)
		{
			ArrayList gateways = getGateways(sch.Grade, gw);
			// remove the last ,
			plist = plist.Substring(0, plist.Length - 1);
            string new_plist = ValidatePhones(plist);
			SMSMessage sms = new SMSMessage(dt, sch.uid_schedule, new_plist, 
				sch.SMSOrigin, sch.SMSMessage, gateways, sch.SMSFlash, q_host, plist, sch.B2UID);
            PostToQueue(mt, q_host, "sms_" + shortQName, sch.uid_schedule, sms.Xml);
            //sms.send();
			//Thread th = new Thread(new ThreadStart(sms.send));			
			//th.Start();
		}

		private ArrayList getGateways(string grade, int index)
		{
            ArrayList lists = new ArrayList();
            DataTable tb = null;
            //int defaultGateway;
            if (grade.Equals("BG"))
            {
                tb = dsGatewayRange.Tables["bg_route"];
                if (index == SMSMessage.MOBILEWAY_DIRECT || index == SMSMessage._3BELLS 
                    || index == SMSMessage.NEXTDIGITAL_DIRECT)
                {
                    if(index != 0)
                        lists.Insert(0, index);
                }
            }
            else
            {
                tb = dsGatewayRange.Tables["ss_route"];
                if (index == SMSMessage.MOBILEWAY_ROAMING || index == SMSMessage.MOBILEWAY_DIRECT ||
                index == SMSMessage.MOBILEWAY_ROAMING2 || index == SMSMessage._3BELLS ||
                index == SMSMessage.ROUTO || index == SMSMessage.MBLOX || index == SMSMessage.TYNTEC ||
                index == SMSMessage.NEXTDIGITAL_DIRECT)
                {
                    if (index >= 0)
                        lists.Insert(0, index);
                }
            }
			
            //if(index == -99)
            //    lists.Add(defaultGateway);
            //else
            //    lists.Add(index);

            if (tb != null)
            {
                foreach (DataRow r in tb.Rows)
                {
                    int n = Convert.ToInt32(r[0]);
                    lists.Add(n);
                }
            }
            if (lists.Count == 0)
            {
                if (grade.Equals("BG"))
                    lists.Add(index == SMSMessage._3BELLS);
                else
                    lists.Add(SMSMessage.MOBILEWAY_ROAMING);
            }
          
			return lists;
		}

		/// <summary>
		/// get all details of this schedule
		/// </summary>
		/// <param name="uid"></param>
		/// <returns></returns>
		/// 
		private SMSSchedule getSchedule(dsSchedule ds)
		{
			SMSSchedule sch = null;
			DataTable tb = ds.Tables["v_schedule_details"];
			if(tb.Rows.Count > 0)
			{
				sch = new SMSSchedule(tb.Rows[0], ds, dsGatewayRange);
			}
			return sch;
		}
		
        private string ValidatePhones(string plist)
        {
            string ret = plist;
            if (isSimulate)
            {
                StringBuilder sb = new StringBuilder();
                string[] lists = plist.Split(',');
                foreach (string p in lists)
                    sb.Append("+12345,");

                ret = sb.ToString();
                ret = ret.Substring(0, ret.Length - 1);
            }
            return ret;
        }

	}
}

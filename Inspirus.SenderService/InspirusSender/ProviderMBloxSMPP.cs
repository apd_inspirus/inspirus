using System;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;
using System.Collections;
using RoaminSMPP.Packet;
using RoaminSMPP.Utility;
using RoaminSMPP.Packet.Request;
using RoaminSMPP.Packet.Response;
using System.Net.Sockets;

namespace InspirusSender
{
    class ProviderMBloxSMPP: SMSProvider
    {
        static int _port = 2775;
        static string _systemId;
        static string _systemType;
        static string _url1, _url2;
        static string _password;
        static NetworkStream _sendStream;
        static NetworkStream _receiveStream1;
        static NetworkStream _receiveStream2;
        public ProviderMBloxSMPP()
		{
            //this.userName = userName;
            //this.password = password;
            //this.url1 = url1;
            //this.url2 = url2;
            //this.port = port;
            //this.systemId = systemId;
            //this.systemType = systemType;
            //this.SMSFlash = sms.SMSFlash;
		}
        //public ProviderMBloxSMPP()
        //{
        //    //this.userName = userName;
        //    //this.password = password;
        //    //this.url1 = url1;
        //    //this.url2 = url2;
        //    //this.port = port;
        //    //this.systemId = systemId;
        //    //this.systemType = systemType;
        //    //this.SMSFlash = sms.SMSFlash;
        //}
        public static void InitialParameters(string url1,string url2,int port,string systemId,string password, string systemType)
        {
            ProviderMBloxSMPP._url1 = url1;
            ProviderMBloxSMPP._url2 = url2;
            ProviderMBloxSMPP._port = port;
            ProviderMBloxSMPP._systemId = systemId;
            ProviderMBloxSMPP._password = password;
            ProviderMBloxSMPP._systemType = systemType;
        }
        public static NetworkStream GetSendStream()
        {
            try
            {
                TcpClient client = null;
                if (ProviderMBloxSMPP._sendStream != null)
                    return ProviderMBloxSMPP._sendStream;
                try
                {
                    client = new TcpClient(ProviderMBloxSMPP._url1, ProviderMBloxSMPP._port);
                    //get NetWorkStream
                    ProviderMBloxSMPP._sendStream = client.GetStream();
                }
                catch (Exception e)
                {
                    fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
                }
                if (ProviderMBloxSMPP._sendStream == null)
                {

                    try
                    {
                        client = new TcpClient(ProviderMBloxSMPP._url2, ProviderMBloxSMPP._port);
                        //get NetWorkStream
                        ProviderMBloxSMPP._sendStream = client.GetStream();
                    }
                    catch (Exception e)
                    {
                        fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
                    }
                }
                //bind as Transmitter
                SmppBind pdu = new SmppBind();
                pdu.AddressNpi = Pdu.NpiType.Unknown;
                pdu.AddressTon = Pdu.TonType.Alphanumeric;
                pdu.BindType = SmppBind.BindingType.BindAsTransmitter;
                pdu.InterfaceVersion = Pdu.SmppVersionType.Version3_4;
                //set password and systemId
                pdu.Password = ProviderMBloxSMPP._password;
                pdu.SystemId = ProviderMBloxSMPP._systemId;

                pdu.SystemType = ProviderMBloxSMPP._systemType;
                pdu.ToMsbHexEncoding();

                byte[] buffer = pdu.PacketBytes;
                ProviderMBloxSMPP._sendStream.Write(buffer, 0, buffer.Length);
                buffer = new byte[2048];
                //listen for the SMSC response
                ProviderMBloxSMPP._sendStream.Read(buffer, 0, 2048);
                SmppBindResp bindResp = new SmppBindResp(buffer);
            }
            catch (Exception ee)
            {
                fmSender.Log(null, fmSender.LOG_ERROR, ee.Message);
            }
            return ProviderMBloxSMPP._sendStream;
        }


        public static NetworkStream GetReceiveStream1()
        {
            try
            {
                TcpClient client = null;
                if (ProviderMBloxSMPP._receiveStream1 != null)
                    return ProviderMBloxSMPP._receiveStream1;
                try
                {
                    client = new TcpClient(ProviderMBloxSMPP._url1, ProviderMBloxSMPP._port);
                    //get NetWorkStream
                    ProviderMBloxSMPP._receiveStream1 = client.GetStream();

                    //bind as Reveiver
                    SmppBind pdu = new SmppBind();
                    pdu.AddressNpi = Pdu.NpiType.Unknown;
                    pdu.AddressTon = Pdu.TonType.Alphanumeric;
                    pdu.BindType = SmppBind.BindingType.BindAsReceiver;
                    pdu.InterfaceVersion = Pdu.SmppVersionType.Version3_4;
                    //set password and systemId
                    pdu.Password = _password;
                    pdu.SystemId = _systemId;

                    pdu.SystemType = _systemType;
                    pdu.ToMsbHexEncoding();

                    byte[] buffer = pdu.PacketBytes;
                    ProviderMBloxSMPP._receiveStream1.Write(buffer, 0, buffer.Length);
                    buffer = new byte[2048];
                    //listen for the SMSC response
                    ProviderMBloxSMPP._receiveStream1.Read(buffer, 0, 2048);
                    SmppBindResp bindResp = new SmppBindResp(buffer);
                }
                catch (Exception e)
                {
                    fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
                }
            }
            catch (Exception ee)
            {
                fmSender.Log(null, fmSender.LOG_ERROR, ee.Message);
            }
            return ProviderMBloxSMPP._receiveStream1;
        }

        public static NetworkStream GetReceiveStream2()
        {
            try
            {
                TcpClient client = null;
                if (ProviderMBloxSMPP._receiveStream2 != null)
                    return ProviderMBloxSMPP._receiveStream2;
                try
                {
                    client = new TcpClient(ProviderMBloxSMPP._url2, ProviderMBloxSMPP._port);
                    //get NetWorkStream
                    ProviderMBloxSMPP._receiveStream2 = client.GetStream();

                    //bind as Reveiver
                    SmppBind pdu = new SmppBind();
                    pdu.AddressNpi = Pdu.NpiType.Unknown;
                    pdu.AddressTon = Pdu.TonType.Alphanumeric;
                    pdu.BindType = SmppBind.BindingType.BindAsReceiver;
                    pdu.InterfaceVersion = Pdu.SmppVersionType.Version3_4;
                    //set password and systemId
                    pdu.Password = _password;
                    pdu.SystemId = _systemId;

                    pdu.SystemType = _systemType;
                    pdu.ToMsbHexEncoding();

                    byte[] buffer = pdu.PacketBytes;
                    ProviderMBloxSMPP._receiveStream2.Write(buffer, 0, buffer.Length);
                    buffer = new byte[2048];
                    //listen for the SMSC response
                    ProviderMBloxSMPP._receiveStream2.Read(buffer, 0, 2048);
                    SmppBindResp bindResp = new SmppBindResp(buffer);
                }
                catch (Exception e)
                {
                    fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
                }
            }
            catch (Exception ee)
            {
                fmSender.Log(null, fmSender.LOG_ERROR, ee.Message);
            }
            return ProviderMBloxSMPP._receiveStream2;
        }
        
        public static void KeepActive()
        {
            try
            {
                byte[] buffer = new byte[2048];
                if (ProviderMBloxSMPP._sendStream != null)
                {

                    SmppEnquireLink enquireLink = new SmppEnquireLink();
                    enquireLink.ToMsbHexEncoding();
                    buffer = enquireLink.PacketBytes;
                    lock (ProviderMBloxSMPP._sendStream)
                    {
                        ProviderMBloxSMPP._sendStream.Write(buffer, 0, buffer.Length);
                        buffer = new byte[2048];
                        //listen for the SMSC response
                        ProviderMBloxSMPP._sendStream.Read(buffer, 0, 2048);
                        SmppEnquireLinkResp enquireLinkResp = new SmppEnquireLinkResp(buffer);
                    }
                }
                
                
            }
            catch (Exception e)
            {
                ProviderMBloxSMPP._sendStream = null;
                fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
            }
            try
            {
                byte[] buffer = new byte[2048];
                if (ProviderMBloxSMPP._receiveStream1 != null)
                {

                    SmppEnquireLink enquireLink = new SmppEnquireLink();
                    enquireLink.ToMsbHexEncoding();
                    buffer = enquireLink.PacketBytes;
                    lock (ProviderMBloxSMPP._receiveStream1)
                    {
                        ProviderMBloxSMPP._receiveStream1.Write(buffer, 0, buffer.Length);
                        //we don't receive response here because we do not sure that next upd is response (maybe delivery_sm)


                        //buffer = new byte[2048];
                        ////listen for the SMSC response
                        //ProviderMBloxSMPP._sendStream.Read(buffer, 0, 2048);
                        //SmppEnquireLinkResp enquireLinkResp = new SmppEnquireLinkResp(buffer);
                    }
                }


            }
            catch (Exception e)
            {
                ProviderMBloxSMPP._receiveStream1 = null;
                fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
            }
            try
            {
                byte[] buffer = new byte[2048];
                if (ProviderMBloxSMPP._receiveStream2 != null)
                {

                    SmppEnquireLink enquireLink = new SmppEnquireLink();
                    enquireLink.ToMsbHexEncoding();
                    buffer = enquireLink.PacketBytes;
                    lock (ProviderMBloxSMPP._receiveStream2)
                    {
                        ProviderMBloxSMPP._receiveStream2.Write(buffer, 0, buffer.Length);
                        
                    }
                }


            }
            catch (Exception e)
            {
                ProviderMBloxSMPP._receiveStream2 = null;
                fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
            }
        }
        public static void CloseStream()
        {
            CloseSendStream();
            CloseReceiveStream();
        }
        public static void CloseSendStream()
        {
            try
            {
                if (ProviderMBloxSMPP._sendStream != null)
                {
                    lock (ProviderMBloxSMPP._sendStream)
                    {
                        byte[] buffer = new byte[2048];
                        //Unbind from smsc
                        SmppUnbind unbind = new SmppUnbind();
                        unbind.ToMsbHexEncoding();
                        buffer = unbind.PacketBytes;
                        ProviderMBloxSMPP._sendStream.Write(buffer, 0, buffer.Length);
                        buffer = new byte[2048];
                        //listen for the SMSC response
                        ProviderMBloxSMPP._sendStream.Read(buffer, 0, 2048);
                        SmppUnbindResp unbindResp = new SmppUnbindResp(buffer);
                        ProviderMBloxSMPP._sendStream.Close();
                        ProviderMBloxSMPP._sendStream.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
                fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
            }
        }
        public static void CloseReceiveStream()
        {
            try
            {
                if (ProviderMBloxSMPP._receiveStream1 != null)
                {
                    lock (ProviderMBloxSMPP._receiveStream1)
                    {
                        byte[] buffer = new byte[2048];
                        //Unbind from smsc
                        SmppUnbind unbind = new SmppUnbind();
                        unbind.ToMsbHexEncoding();
                        buffer = unbind.PacketBytes;
                        ProviderMBloxSMPP._receiveStream1.Write(buffer, 0, buffer.Length);
                        buffer = new byte[2048];
                        //listen for the SMSC response
                        ProviderMBloxSMPP._receiveStream1.Read(buffer, 0, 2048);
                        SmppUnbindResp unbindResp = new SmppUnbindResp(buffer);
                        ProviderMBloxSMPP._receiveStream1.Close();
                        ProviderMBloxSMPP._receiveStream1.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
                fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
            }
            try
            {
                if (ProviderMBloxSMPP._receiveStream2 != null)
                {
                    lock (ProviderMBloxSMPP._receiveStream2)
                    {
                        byte[] buffer = new byte[2048];
                        //Unbind from smsc
                        SmppUnbind unbind = new SmppUnbind();
                        unbind.ToMsbHexEncoding();
                        buffer = unbind.PacketBytes;
                        ProviderMBloxSMPP._receiveStream2.Write(buffer, 0, buffer.Length);
                        buffer = new byte[2048];
                        //listen for the SMSC response
                        ProviderMBloxSMPP._receiveStream2.Read(buffer, 0, 2048);
                        SmppUnbindResp unbindResp = new SmppUnbindResp(buffer);
                        ProviderMBloxSMPP._receiveStream2.Close();
                        ProviderMBloxSMPP._receiveStream2.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
                fmSender.Log(null, fmSender.LOG_ERROR, e.Message);
            }
        }
        public bool Send(string uid_sch, string b2u, string recipientList, string senderPhone, string Message, ArrayList resp)
        {
            bool returnValue = false;
            //ArrayList resp = new ArrayList();
            try
            {
                //create tcp/ip connection
                NetworkStream stream = ProviderMBloxSMPP.GetSendStream();
                lock (stream)
                {
                    if (stream == null)
                        return returnValue;

                    byte[] buffer = new byte[2048];
                    string[] recipients = recipientList.Split(new char[] { ',' });
                    foreach (string recipient in recipients)
                    {
                        //send SubmitSm per recipient
                        SmppSubmitSm pdup = new SmppSubmitSm();

                        pdup.SourceAddress = senderPhone;
                        pdup.SourceAddressNpi = Pdu.NpiType.Unknown;
                        pdup.SourceAddressTon = Pdu.TonType.Alphanumeric;
                        pdup.DestinationAddress = recipient;
                        pdup.DestinationAddressNpi = Pdu.NpiType.Unknown;
                        pdup.DestinationAddressTon = Pdu.TonType.Alphanumeric;
                        
                        pdup.ProtocolId = Pdu.SmppVersionType.Version3_4;
                        pdup.PriorityFlag = Pdu.PriorityType.Level1;
                        //pdup.ScheduleDeliveryTime = "051202120000006R";
                        //pdup.ValidityPeriod = "051202120000006R";
                        pdup.RegisteredDelivery = Pdu.RegisteredDeliveryType.OnFailure;
                        //pdup.RegisteredDelivery = Pdu.RegisteredDeliveryType.None;
                        pdup.ReplaceIfPresentFlag = false;
                        pdup.DataCoding = Pdu.DataCodingType.SMSCDefault;
                        pdup.ShortMessage = Message;
                        uint seq = Convert.ToUInt32(DateTime.Now.Hour * 100 + DateTime.Now.Minute);
                        //pdup.SequenceNumber = seq;
                        
                        pdup.ToMsbHexEncoding();

                        buffer = pdup.PacketBytes;

                        stream.Write(buffer, 0, buffer.Length);

                        buffer = new byte[2048];
                        //listen for the SMSC response
                        stream.Read(buffer, 0, 2048);
                        //get response from smsc
                        SmppSubmitSmResp msgResp = new SmppSubmitSmResp(buffer);
                        //create response for system
                        ReplyWithPhone reply = new ReplyWithPhone();
                        reply.phone = recipient;
                        reply.ErrCode = int.Parse(msgResp.CommandStatus.ToString());
                        resp.Add(reply);

                        string ret = String.Format("{0}\t{1}\t{2}\t{3}\t{4}", 
                            uid_sch, b2u, senderPhone, recipient, seq);
                        Utils.PostToQueue("sms_smpp_resp", "mblox", ret);
                        fmSender.Log(this, fmSender.LOG_TRACE, ret);
                    }
                }
                
                returnValue = true;

                Receive();
            }
            catch (Exception e)
            {
                fmSender.Log(this, fmSender.LOG_ERROR, e.Message);
                
            }
            return returnValue;
        }
 
        public ArrayList Receive()
        {
            ArrayList resp = new ArrayList();
            byte[] buffer = new byte[2048];
            try
            {
               
                //get NetWorkStream
                NetworkStream stream = ProviderMBloxSMPP.GetReceiveStream1();

                lock (stream)
                {//lock stream to prevent another thread to process it
                    
                    //listen for the SMSC response
                    while (true)
                    {
                        try
                        {
                            buffer = new byte[2048];
                            //check if has data
                            if (!stream.DataAvailable)
                            {
                                break;
                            }
                            //read command length
                            stream.Read(buffer, 0, 8);
                            
                            
                            
                            //read left data of one command
                            stream.Read(buffer, 8, GetInt(buffer,0,4) - 8);

                            if (GetInt(buffer,4,4) != 5)
                            {
                                // if command id is not equal 5(delivery_sm)
                                // it cound be enquire_link_respose
                                // we ignore it
                                continue;
                            }

                            SmppDeliverSm pdup = new SmppDeliverSm(buffer);

                            //add code to process deliversm from smsc
                            
                            //for test we write the MO to log file
                            string ret = String.Format("{0}\t{1}\t{2}\t{3}", 
                                pdup.CommandStatus,
                                pdup.SourceAddress,                                
                                pdup.ShortMessage, 
                                pdup.SequenceNumber);
                            Utils.PostToQueue("sms_smpp_notify", "mblox", ret);

                            fmSender.Log(this, fmSender.LOG_TRACE, ret);
                            //
                            // add to ArrayList 

                            SmppDeliverSmResp rep = new SmppDeliverSmResp();

                            rep.SequenceNumber = pdup.SequenceNumber;
                            rep.CommandStatus = 0;
                            rep.ToMsbHexEncoding();
                            buffer = rep.PacketBytes;
                            stream.Write(buffer, 0, buffer.Length);


                        }
                        catch (Exception eee)
                        {
                            fmSender.Log(this, fmSender.LOG_ERROR, eee.Message);
                            break;
                        }
                    }
                }

                #region GET_RECEIVE_STREAM2
                //stream = ProviderMBloxSMPP.GetReceiveStream2();

                //lock (stream)
                //{
                //    while (true)
                //    {
                //        try
                //        {
                //            buffer = new byte[2048];
                //            //check if has data
                //            if (!stream.DataAvailable)
                //            {
                //                break;
                //            }
                //            ///read command length
                //            stream.Read(buffer, 0, 8);

                //            //read left data of one command
                //            stream.Read(buffer, 8, GetInt(buffer, 0, 4) - 8);

                //            if (GetInt(buffer, 4, 4) != 5)
                //            {
                //                // if command id is not equal 5(delivery_sm)
                //                // it cound be enquire_link_respose
                //                // we ignore it
                //                continue;
                //            }

                //            SmppDeliverSm pdup = new SmppDeliverSm(buffer);

                //            string ret = String.Format("{0}\t{1}\t{2}\t{3}",
                //                pdup.CommandStatus,
                //                pdup.SourceAddress,
                //                pdup.ShortMessage,
                //                pdup.SequenceNumber);
                //            Utils.PostToQueue("sms_smpp_notify", "mblox", ret);

                //            fmSender.Log(this, fmSender.LOG_TRACE, ret);
                //            ////add code to process deliversm from smsc
                //            ////
                //            //fmSender.Log(this, fmSender.LOG_TRACE,
                //            //    "2.MO|Nofication CommandStatus=" + pdup.CommandStatus +
                //            //    " SourceAddress=" + pdup.SourceAddress +
                //            //    " Message=" + pdup.ShortMessage);

                //            ////for test we write the MO to log file


                //            //
                //            // add to ArrayList 

                //            SmppDeliverSmResp rep = new SmppDeliverSmResp();

                //            rep.SequenceNumber = pdup.SequenceNumber;
                //            rep.CommandStatus = 0;
                //            rep.ToMsbHexEncoding();
                //            buffer = rep.PacketBytes;
                //            stream.Write(buffer, 0, buffer.Length);


                //        }
                //        catch (Exception eee)
                //        {
                //            fmSender.Log(this, fmSender.LOG_ERROR, eee.Message);
                //            break;
                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
            }
            return resp;
        }
        int GetInt(byte[] buffer,int begin, int length)
        {
            StringBuilder bu = new StringBuilder();
            for (int i = begin; i < begin + length; i++)
            {
                bu.Append(buffer[i]);
            }
            //bu.Append(buffer[0]).Append(buffer[1]).Append(buffer[2]).Append(buffer[3]);
            int nu = int.Parse(string.Format("{0:d}", bu.ToString()));
            return nu;
        }
    }
}

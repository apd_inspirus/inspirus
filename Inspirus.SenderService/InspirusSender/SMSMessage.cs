
#define NOT_SIMULATION

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Messaging;
using System.Xml;
using System.Text;
using NLog;

namespace InspirusSender
{
    /// <summary>
    /// each SMS is encapsulated in a SMSMessage object
    /// </summary>
    public class SMSMessage
    {
        private string uid, recipientList, senderPhone, message, origPhoneList, uid_b2u;

        private readonly Logger _logger = LogManager.GetLogger("SendLogger");

        public string Uid_b2u
        {
            get { return uid_b2u; }
            set { uid_b2u = value; }
        }
        private ArrayList gateways;
        public int SMSFlash;
        private string q_host;
        private DateTime startTime;
        static public int MOBILEWAY_ROAMING = 0, CLICKATELL = 1, SMSWHIZ = 2, ROUTO = 5,
            ROUTO_BULK = 6, ROUTO_EMS = 7, MOBILEWAY_DIRECT = 8, _3BELLS = 9,
            MOBILEWAY_ROAMING2 = 10, MWEB = 12, MBLOX = 14, TYNTEC = 15, SMS_CENTRAL = 16,
            NEXTDIGITAL_SS = 17, NEXTDIGITAL_DIRECT = 18;

        public string toString()
        {
            return string.Format("uid={0},b2u={1},phones={2}", uid, uid_b2u, origPhoneList);
        }

        public SMSMessage(DateTime dt, string id, string recipientList, string senderPhone, string message,
            ArrayList gateways, int SMSFlash, string q_host, string origPhoneList, string uid_b2u)
        {
            this.uid = id;
            this.recipientList = recipientList;
            this.senderPhone = senderPhone;

            this.message = message;
            this.gateways = gateways;
            this.SMSFlash = SMSFlash;
            this.q_host = q_host;
            this.origPhoneList = origPhoneList;
            startTime = dt;
            this.uid_b2u = uid_b2u;
        }

        public SMSMessage(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNode node = doc.DocumentElement.FirstChild;
            long ticks = Convert.ToInt64(node.InnerText);
            startTime = new DateTime(ticks);

            node = node.NextSibling;
            uid = node.InnerText;

            node = node.NextSibling;
            this.uid_b2u = node.InnerText;

            node = node.NextSibling;
            recipientList = node.InnerText;

            node = node.NextSibling;
            senderPhone = node.InnerText;

            node = node.NextSibling;
            message = node.InnerText;

            node = node.NextSibling;
            SMSFlash = Convert.ToInt32(node.InnerText);

            node = node.NextSibling;
            q_host = node.InnerText;

            node = node.NextSibling;
            origPhoneList = node.InnerText;

            node = node.NextSibling;
            string[] gw = node.InnerText.Split(';');
            gateways = new ArrayList();
            foreach (string str in gw)
            {
                if (str.Length > 0)
                    gateways.Add(Convert.ToInt32(str));
            }
        }

        public void send()
        {
            bool isSent = false;
            bool isDeleted = false;

            for (int i = 0; i < 3; i++)
            {
                foreach (int gateway in gateways)
                {
                    for (int j = 0; j < 2; j++)
                    {
                        if (j == 1)
                            System.Threading.Thread.Sleep(20000);

                        lock (this)
                        {
                            // UNDONE must check if a schedule is deleted in production
                            if (fmSender.IsScheduleDeleted(this, this.uid, this.uid_b2u))
                            {
                                fmSender.Log(this, fmSender.LOG_TRACE, String.Format("deleted,gw={1},{0}", toString(), gateway));
                                isSent = true;      // skip the Cirtical Error log
                                isDeleted = true;
                                j = 9;
                                break;
                            }
                        }
                        if (sendSMS(gateway))
                        {
                            fmSender.Log(this, fmSender.LOG_TRACE, String.Format("sent,{0},gw={1}", toString(), gateway));
                            isSent = true;
                            j = 9;
                            break;
                        }
                        fmSender.Log(this, fmSender.LOG_TRACE, String.Format("wait 60,{0},gw={1}", toString(), gateway));
                    }
                    if (isSent || isDeleted)
                        break;
                }
                if (isSent || isDeleted)
                    break;
            }
            // log result
            if (!isSent)
            {
                fmSender.Log(this, fmSender.LOG_ERROR, String.Format("{0},Cirtical Error", toString()));
                ReplySimple rs = new ReplySimple(-9999, "Cirtical Error");
                updateState(rs.ErrCode, uid, 0, rs, -1, "");

                // TODO send an email to manager
            }
            if (isDeleted)
            {
                ReplySimple rs = new ReplySimple(-2, "Deleted Schedule");
                updateState(rs.ErrCode, uid, 0, rs, -1, "");
            }
        }
        /// <summary>
        /// add by alan to send message store in special messageQ which stores messages faild to be send before.
        /// </summary>
        /// <returns></returns>
        public bool sendSMS_SMPP()
        {
            //ReplySimple rs = null;
            //int gateway = MBLOX;
            bool ret = false;
            //ProviderMBloxSMPP p = new ProviderMBloxSMPP();
            //ArrayList respList = new ArrayList();
            //ret = p.Send(this.uid, this.uid_b2u, this.Recipient, this.Sender, this.Message, respList);
            //if (ret)
            //{
            //    foreach (ReplyWithPhone r in respList)
            //    {
            //        fmSender.Log(this, fmSender.LOG_TRACE, "updateState\t" + r.toString);

            //        ReplySimple rs2 = new ReplySimple(r.ErrCode, r.ErrMsg);
            //        if (r.ErrCode == 0)
            //            updateState(rs2.ErrCode, uid, 90, rs2, gateway, r.phone);
            //        else
            //            updateState(rs2.ErrCode, uid, 40, rs2, gateway, r.phone);
            //    }
            //    rs = new ReplySimple(0, "OK");
            //    ret = true;
            //    fmSender.Log(this, fmSender.LOG_TRACE, "exiting MBlox");
            //}
            //else
            //{
            //   ret = false;
            //}
            
            return ret;
        }
        private Boolean sendSMS(int gateway)
        {
            ReplySimple rs = null;
            Boolean ret = false;

            if (gateway == -99)
                gateway = MOBILEWAY_ROAMING;

            fmSender.Log(this, fmSender.LOG_TRACE, String.Format("sendSMS;sendto={0};gw={1};msg={2}", recipientList, gateway, message));

            // UNDONE remove the following line to set the right gateway
#if SIMULATION
            gateway = MOBILEWAY_ROAMING;
#endif
            if (gateway == -1)
                gateway = MOBILEWAY_ROAMING;

            //if (this.uid_b2u.Equals("FFC8E679-306E-1B4E-81417BB10095E346"))
            //{
            //    gateway = MBLOX;
            //    fmSender.Log(this, fmSender.LOG_TRACE, String.Format("switching to gateway {0}", gateway));
            //}

            if (gateway == ROUTO)
            {
                if (recipientList.IndexOf("61425") >= 0 || recipientList.IndexOf("61427") >= 0 ||
                    recipientList.IndexOf("61428") >= 0 || recipientList.IndexOf("61429") >= 0 ||
                    recipientList.IndexOf("61433") >= 0)
                    gateway = MOBILEWAY_ROAMING;
            }

            if (gateway == MOBILEWAY_ROAMING2)        // 10
            {
                ProviderMobile365 p = new ProviderMobile365(
                    "messaging.sybase365.com",
                    "/inspirus_r31017/inspirus_r31017.sms",
                    "inspirus_r31017", "e8QKwcM0", this);
//                rs = p.Send();
            }
            else if (gateway == MOBILEWAY_ROAMING)     // 0
            {
                ProviderMobile365 p = new ProviderMobile365(
                    "messaging.sybase365.com",
                    "/inspirus_roaming/inspirus_roaming.sms",
                    "inspirus_roaming", "rQjr3Hbl", this);
//                rs = p.Send();

            }
            else if (gateway == MOBILEWAY_DIRECT)       // 8    BG
            {
                ProviderMobile365 p = new ProviderMobile365(
                    "messaging.sybase365.com",
                    "/inspirus_direct/inspirus_direct.sms",
                    "inspirus_direct", "jH9l8MKQ", this);
//                rs = p.Send();

            }
            else if (gateway == CLICKATELL)         // TODO not use
            {
            }
            else if (gateway == _3BELLS)        // 9            BG
            {
                this.recipientList = this.origPhoneList.Replace(",0", ",+");
                this.recipientList = this.origPhoneList.Replace(",61", ",+");
                if (this.recipientList.StartsWith("0"))
                    this.recipientList = String.Format("+{0}", this.recipientList.Substring(1));
                if (this.recipientList.StartsWith("61"))
                    this.recipientList = String.Format("+{0}", this.recipientList.Substring(2));

                fmSender.Log(this, fmSender.LOG_TRACE, String.Format("3bells\t{0}\t{1}", recipientList, message));

                //Provider3Bells p = new Provider3Bells("inspirus", "xx4l56bb9", this);
                Provider3Bells p = new Provider3Bells("inspirus", "xx4156bb9", this);
//                rs = p.Send(this.Recipient, this.Sender, this.Message);
            }
            else if (gateway == ROUTO)           // 5
            {
                //string phone = origPhoneList.Replace(",0", ",61");
                string url = "http://smsc5.routotelecom.com/cgi-bin/SMSsend";
                ProviderRouto p = new ProviderRouto(url, "inspirus", "260901js", this);
//                rs = p.Send(this.Recipient, this.Sender, this.Message);

                //ProviderRouto p = new ProviderRouto(url, "ems", "inspiru5", this);                
                //rs = p.Send(this.Recipient, this.Sender, this.Message);
            }
            else if (gateway == ROUTO_EMS)       // TODO gateway 7 seems not used any more
            {
                if (SMSFlash == 1)
                {
                    string url = "http://smsc3.routotelecom.com/cgi-bin/SMSsend";
                    ProviderRouto p = new ProviderRouto(url, "ems", "inspiru5", this);
//                    rs = p.Send(this.Recipient, this.Sender, this.Message);
                }
                else
                {
                    string url = "http://smsc.routotelecom.com/cgi-bin/SMSsend";
                    ProviderRouto p = new ProviderRouto(url, "ems", "inspiru5", this);
//                    rs = p.Send(this.Recipient, this.Sender, this.Message);
                }
            }
            else if (gateway == NEXTDIGITAL_SS)
            {
                string url = "http://srs.nextdigital.com/Enterprise/sendsmsv2";
                ProviderNextDigital p = new ProviderNextDigital(url, "inspirus", "ab8GCjpz", this, "default");
//                rs = p.Send(this.Recipient, this.Sender, this.Message);
            }
            else if (gateway == NEXTDIGITAL_DIRECT)
            {
                string url = "http://srs.nextdigital.com/Enterprise/sendsmsv2";
                ProviderNextDigital p = new ProviderNextDigital(url, "inspirusdirect", "ab8GCjpz", this, "default");
//                rs = p.Send(this.Recipient, this.Sender, this.Message);
            }
            else if (gateway == SMS_CENTRAL)
            {
                string url = "http://srs.smsc.com.au/Enterprise/sendsmsv2";
                ProviderSMSCentral p = new ProviderSMSCentral(url, "inspirus", "ab8GCjpz", this);
//                rs = p.Send(this.Recipient, this.Sender, this.Message);
            }
            else if (gateway == MWEB)
            {
                string url = "http://196.4.93.150/hub3/HttpListener";
                ProviderMWeb p = new ProviderMWeb(url, "Inspirus", "371rus", this);
//                rs = p.Send(this.Recipient, this.Sender, this.Message);
            }
            else if (gateway == MBLOX)
            {
                /*
                string url = "http://xml3.mblox.com:8180/send";
                ProviderMBlox p = new ProviderMBlox(url, "Inspirus", "9DuF4839", this);
                ArrayList respList = p.Send(this.Recipient, this.Sender, this.Message);
                foreach (ReplyWithPhone r in respList)
                {
                    fmSender.Log(this, fmSender.LOG_TRACE, "updateState\t" + r.toString); 

                    ReplySimple rs2 = new ReplySimple(r.ErrCode, r.ErrMsg);
                    if(r.ErrCode == 0)
                        updateState(rs2.ErrCode, uid, 90, rs2, gateway, r.phone);
                    else
                        updateState(rs2.ErrCode, uid, 40, rs2, gateway, r.phone);
                }
                rs = new ReplySimple(0, "OK");
                ret = true;
                fmSender.Log(this, fmSender.LOG_TRACE, "exiting MBlox" ); 
                 */
                ////add by Alan to implemnt SMPP
                //string url1 = "smpp5.mblox.com";
                //string url2 = "smpp6.mblox.com";

                //string systemId = "Inspirus";
                //string systemType = "SMPP";
                //string password = "9DuF4839";
                //int port = 3216;
                //ProviderMBloxSMPP p = new ProviderMBloxSMPP();
                //ArrayList respList = new ArrayList();
                //ret = p.Send(this.uid, this.uid_b2u, this.Recipient, this.Sender, this.Message, respList);
                //if (ret)
                //{
                //    foreach (ReplyWithPhone r in respList)
                //    {
                //        fmSender.Log(this, fmSender.LOG_TRACE, "updateState\t" + r.toString);

                //        ReplySimple rs2 = new ReplySimple(r.ErrCode, r.ErrMsg);
                //        if (r.ErrCode == 0)
                //            updateState(rs2.ErrCode, uid, 90, rs2, gateway, r.phone);
                //        else
                //            updateState(rs2.ErrCode, uid, 40, rs2, gateway, r.phone);
                //    }
                //    rs = new ReplySimple(0, "OK");
                    
                //    fmSender.Log(this, fmSender.LOG_TRACE, "exiting MBlox");
                //}
                //else
                //{
                //    //There are exceptions
                //    //re-save message to MQ for resend in the furture
                //    PostToQueue(null, q_host, "sms_smpp", uid, Xml);
                //}

                fmSender.Log(this, fmSender.LOG_TRACE, Xml);

                // CC 2 May 2007
//                PostToQueue(null, q_host, "sms_smpp2", uid, Xml);

                // set ret is true to disable send() method to try 3 times
                ret = true;
                //end_add_by_alan

            }
            else if (gateway == TYNTEC)
            {
                fmSender.Log(this, fmSender.LOG_TRACE, Xml);

                // CC 4 Aug 07
//                PostToQueue(null, q_host, "sms_smpp_tyntec", uid, Xml);

                // set ret is true to disable send() method to try 3 times
                ret = true;
            }

            if (gateway == MBLOX || gateway == TYNTEC)
            {
            } 
            else
            {
                updateState(0, uid, 90, rs, gateway, "");
                /*if (rs != null)
                {
                    fmSender.Log(this, fmSender.LOG_TRACE, rs.Content);
                    // 100 is from 3Bells
                    if (rs.ErrCode == 0 || (gateway == _3BELLS && rs.ErrCode == 100))
                    {
                        // SMS sent successful
                        updateState(rs.ErrCode, uid, 90, rs, gateway, "");
                    }
                    else
                    {
                        updateState(rs.ErrCode, uid, 40, rs, gateway, "");
                    }
                    // log response from the server
                    if (rs.ErrCode != -999)  // -999 = try the next gateway
                        ret = true;
                }
                else
                {
                    rs = new ReplySimple(-1, "Blank Reply");
                    fmSender.Log(this, fmSender.LOG_TRACE, rs.Content);
                    updateState(rs.ErrCode, uid, 40, rs, gateway, "");
                }*/
            }
            _logger.Info("SMS is sent. Id: {0}, gateway-out: {1}", Id, gateway);
            fmSender.Log(this, fmSender.LOG_TRACE, String.Format("exit SMSMessage\tuid={0};gw={1}", 
                uid, gateway));
//            return ret;
            return true;
        }

        private bool updateState(int errcode, string uid, int new_state, ReplyBase rs, int gateway, string phone)
        {
            bool ret = false;
            string path = String.Format("FormatName:direct=tcp:{0}\\Private$\\Queue_incoming", q_host);
            if (errcode == 0 || (errcode == 100 && gateway == _3BELLS))
                path = String.Format("FormatName:direct=tcp:{0}\\Private$\\Queue_ok", q_host);
            else
                path = String.Format("FormatName:direct=tcp:{0}\\Private$\\Queue_fail", q_host);

            MessageQueue mq = new MessageQueue(path);
            MessageQueueTransaction mt = new MessageQueueTransaction();
            try
            {
                dsSchedule ds = new dsSchedule();
                DataTable tb = ds.Tables["response"];
                DataRow r = tb.NewRow();
                tb.Rows.Add(r);
                r["uid_schedule"] = uid;
                r["state"] = new_state;
                r["code"] = errcode;
                r["message"] = "";
                r["time_sent"] = DateTime.Now;
                r["gateway"] = gateway;
                r["time_start"] = startTime;
                if (phone.Equals(""))
                {
                    r["recipient_list"] = this.recipientList;
                    r["orig_phone_list"] = this.origPhoneList;
                    string[] p = recipientList.Split(',');
                    r["recipient_count"] = p.Length;
                }
                else
                {
                    r["recipient_list"] = phone;
                    r["orig_phone_list"] = phone;
                    r["recipient_count"] = 1;
                }

                r["b2u"] = this.uid_b2u;


                string xml = ds.GetXml();

                mt.Begin();
                mq.Send(xml, "update_state", mt);
                mt.Commit();
                ret = true;

                fmSender.Log(this, fmSender.LOG_TRACE, String.Format("gw={0};uid={1};err={2}",
                    gateway, uid, errcode));
            }
            catch (Exception ex)
            {
                fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
                //System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
                mt.Abort();
            }
            return ret;
        }


        public string Id
        {
            get
            {
                return uid;
            }
        }
        public string Recipient
        {
            get
            {
                return this.recipientList;
            }
        }

        public string Sender
        {
            get
            {
                return senderPhone;
            }
        }

        public string Message
        {
            get
            {
                return message;
            }
        }

        public string Xml
        {
            get
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><sms/>");
                addElement(doc, "start_time", String.Format("{0}", startTime.Ticks));
                addElement(doc, "uid", uid);
                addElement(doc, "uid_b2u", uid_b2u);
                addElement(doc, "recipients", recipientList);
                addElement(doc, "sender", senderPhone);
                addElement(doc, "message", message);
                addElement(doc, "flash", String.Format("{0}", SMSFlash));
                addElement(doc, "q_host", q_host);
                addElement(doc, "origRecipients", origPhoneList);
                StringBuilder sb = new StringBuilder();
                foreach (int gw in gateways)
                {
                    sb.Append(gw);
                    sb.Append(";");
                }
                addElement(doc, "gateways", sb.ToString());
                //doc.Save("./test.xml");
                sb.Length = 0;
                XmlWriter xw = XmlTextWriter.Create(sb);
                doc.WriteTo(xw);
                xw.Close();

                //Console.Write(sb.ToString());

                return sb.ToString();
            }

        }

        private void addElement(XmlDocument doc, string fname, string text)
        {
            XmlElement elm = doc.CreateElement(fname);
            XmlText xtext = doc.CreateTextNode(text);

            doc.DocumentElement.AppendChild(elm);
            doc.DocumentElement.LastChild.AppendChild(xtext);
        }
        private string getQueueName(string q_host, string qname)
        {
            if (q_host.Equals("localhost") || q_host.Equals("127.0.0.1"))
                return String.Format(".\\Private$\\{0}", qname);
            else
                return String.Format("FormatName:direct=tcp:{0}\\Private$\\{1}", q_host, qname);
        }

        private void PostToQueue(MessageQueueTransaction mt, string q_host, string path, string uid, string xml)
        {
            bool isSelfManaged = false;
            if (mt == null)
            {
                mt = new MessageQueueTransaction();
                isSelfManaged = true;
            }
            try
            {
                path = getQueueName(q_host, path);
                MessageQueue mq;
                if(!MessageQueue.Exists(path))
                {
                    mq = MessageQueue.Create(path,true);
                }
                else
                {
                    mq = new MessageQueue(path);
                }
                

                if (isSelfManaged)
                    mt.Begin();
                mq.Send(xml, uid, mt);

                if (isSelfManaged)
                    mt.Commit();
            }
            catch (Exception ex)
            {
                if (isSelfManaged)
                    mt.Abort();
                fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
                fmSender.EventLog(ex.Message);
            }
            if (isSelfManaged)
                mt.Dispose();
        }


    }
}

using System;

namespace InspirusSender
{
	/// <summary>
	/// Summary description for SMSReply.
	/// </summary>
	public class ReplyBase
	{
		protected string Response, errMsg;
		protected int errCode;
		protected string RawResponse;

		public ReplyBase()
		{
			errCode = 0;
			Response = String.Empty;
			errMsg = String.Empty;
		}

		public int ErrCode
		{
			get 
			{
				return errCode;
			}

            set
            {
                errCode = value;
            }
		}

		public string ErrMsg
		{
			get 
			{
				return errMsg;
			}
            set
            {
                errMsg = value;
            }
		}

		public string Content
		{
			get 
			{
				return String.Format("Code={0};Text={1}", errCode, errMsg);
			}
		}

		public string Raw
		{
			get 
			{
				return RawResponse;
			}
		}
	}
}

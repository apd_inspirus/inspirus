using System;
using System.Net;
using System.Text;
using System.IO;


namespace InspirusSender
{
	/// <summary>
	/// Summary description for SMSProvider.
	/// </summary>
	public class SMSProvider
	{
		protected string userName, password, url;
		protected string recipientList, senderPhone, Message;
		protected int SMSFlash;
		public ReplySimple resp = null;

		public SMSProvider()
		{
			this.userName = String.Empty;
			this.password = String.Empty;
			this.url = String.Empty;
			this.recipientList = String.Empty;
			this.senderPhone = String.Empty;
			this.Message = String.Empty;
		}

		/*
		 * 
		 * 
		 * 
		 * return code
		 * 0 - OK
		 * -101 - password invalid
		 * -104 - Recipient not valid
		 * 
		 */
		public ReplySimple SimpleSend(string url, string recipientList, string senderPhone, string Message)
		{
			StringBuilder sb = new StringBuilder();
			try 
			{
				//string url = getURL(recipientList, senderPhone, Message);
				HttpWebRequest h = (HttpWebRequest)WebRequest.Create(url);
				h.Method = "GET";
				h.ContentType="application/x-www-form-urlencoded";
				HttpWebResponse rsp = (HttpWebResponse)h.GetResponse();
				Stream receiveStream = rsp.GetResponseStream();
				Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
				StreamReader readStream = new StreamReader( receiveStream, encode );
				
				Console.WriteLine("\r\nResponse stream received.");
				int n = 1024;
				Char[] read = new Char[n];
				// Reads 256 characters at a time.    
				int count = readStream.Read( read, 0, n );
				Console.WriteLine("HTML...\r\n");
				while (count > 0) 
				{
					// Dumps the 256 characters on a string and displays the string to the console.
					String str = new String(read, 0, count);
					sb.Append(str);
					Console.Write(str);
					count = readStream.Read(read, 0, n);
				}
				Console.WriteLine("");
				rsp.Close();
				readStream.Close();
			
				resp = new ReplySimple(sb.ToString());
			} 
			catch (Exception ex )
			{
                fmSender.Log(this, fmSender.LOG_ERROR, ex.Message);
				fmSender.EventLog(ex.Message);
				resp = new ReplySimple(-999, ex.Message);
			}
			
			return resp;
		}

	}
}

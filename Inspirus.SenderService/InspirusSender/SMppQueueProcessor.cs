using System;
using System.Collections.Generic;
using System.Text;
using System.Messaging;
namespace InspirusSender
{
    public class SMppQueueProcessor
    {
        MessageQueue mq = null;
        public SMppQueueProcessor(string path)
        {
            mq = new MessageQueue(path);
            mq.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });
        }
        public void Process()
        {
            MessageQueueTransaction mt = new MessageQueueTransaction();
            for (int i = 0; i < 500; i++)
            {
                System.Windows.Forms.Application.DoEvents();
                
                try
                {
                    mt.Begin();
                    System.Messaging.Message m = mq.Receive(new TimeSpan(0, 0, 0, 1),mt);
                    
                    string xml = (string)m.Body;
                    SMSMessage sms = new SMSMessage(xml);
                    if (sms.sendSMS_SMPP())
                        mt.Commit();
                    else
                    {
                        mt.Abort();
                        break;
                    }
                }
                catch (MessageQueueException mex)
                {
                    if (mex.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                    {
                        fmSender.Log(this, fmSender.LOG_ERROR, "SQP:" + mex.Message);
                        System.Diagnostics.EventLog.WriteEntry("InspirusSender", mex.Message, System.Diagnostics.EventLogEntryType.Error);
                    }
                    mt.Abort();
                    break;
                }
                catch (Exception ex)
                {
                    mt.Abort();
                    fmSender.Log(this, fmSender.LOG_ERROR, "SQP:" + ex.Message);
                    System.Diagnostics.EventLog.WriteEntry("InspirusSender", ex.Message, System.Diagnostics.EventLogEntryType.Error);
                    break;
                }
            }
        }
    }
}

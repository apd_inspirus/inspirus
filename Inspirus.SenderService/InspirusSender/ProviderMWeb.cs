using System;
using System.Web;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;

namespace InspirusSender
{
    class ProviderMWeb : SMSProvider
    {
        public ProviderMWeb(string url, string userName, string password, SMSMessage sms)
		{
			this.userName = userName;
			this.password = password;
			this.url = url;
			this.SMSFlash = sms.SMSFlash;
		}

		private string getURL(string recipientList, string senderPhone, string Message) 
		{
            // destination phone is in the following format
            // 61407850388
            // no plus sign in the beginning
            string ret = String.Format("{0}?HUB_UID=inspirus&HUB_ACCOUNT={1}&HUB_PWD={2}&ADDR_DEST={3}&ADDR_ORIG={4}&SMSG={5}", 
				url, userName, password, recipientList, senderPhone, 
				HttpUtility.UrlEncode(Message));			
			return ret;
		}

        public ReplySimple Send(string recipientList, string senderPhone, string Message)
        {
            //string url = getURL(recipientList, senderPhone, Message);
            //fmSender.Log(this, fmSender.LOG_TRACE, url);
            //resp = new ReplySimple(0, url);

            // UNDONE uncomment the following code

            StringBuilder sb = new StringBuilder();
            try
            {
                string url = getURL(recipientList, senderPhone, Message);

                fmSender.Log(this, fmSender.LOG_TRACE, "url=" + url);

                HttpWebRequest h = (HttpWebRequest)WebRequest.Create(url);
                h.Method = "GET";
                h.ContentType = "application/x-www-form-urlencoded";
                HttpWebResponse rsp = (HttpWebResponse)h.GetResponse();
                Stream receiveStream = rsp.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(receiveStream, encode);

                Console.WriteLine("\r\nResponse stream received.");
                int n = 1024;
                Char[] read = new Char[n];
                // Reads 256 characters at a time.    
                int count = readStream.Read(read, 0, n);
                Console.WriteLine("HTML...\r\n");
                while (count > 0)
                {
                    // Dumps the 256 characters on a string and displays the string to the console.
                    String str = new String(read, 0, count);
                    sb.Append(str);
                    Console.Write(str);
                    count = readStream.Read(read, 0, n);
                }
                Console.WriteLine("");
                rsp.Close();
                readStream.Close();

                resp = decode(sb.ToString().Trim());
            }
            catch (Exception ex)
            {
                fmSender.EventLog(ex.Message);
                resp = new ReplySimple(-999, ex.Message);
            }

            return resp;
        }


        private ReplySimple decode(string raw)
        {
            XmlDocument d = new XmlDocument();
            d.LoadXml(raw);
            XmlNodeList disp = d.GetElementsByTagName("msg-disp");
            XmlNodeList reason = d.GetElementsByTagName("resp-reason");
            XmlNodeList msgRef = d.GetElementsByTagName("msg-ref");

            if (disp != null && disp.Count > 0 && reason != null & reason.Count > 0
                && msgRef != null && msgRef.Count > 0)
            {
                string str = disp[0].InnerText;
                if (str.Equals("OK"))
                    resp = new ReplySimple(0, msgRef[0].InnerText);
                else
                    resp = new ReplySimple(101, reason[0].InnerText);
            }
            return resp;
        }


    }
}

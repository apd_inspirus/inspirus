using System;
using System.Collections.Generic;
using System.Text;

namespace InspirusSender
{
    class ReplyWithPhone : ReplyBase
    {
        public string phone;

        public ReplyWithPhone()
        {
            phone = "";
            this.errCode = 0;
            this.errMsg = "";
        }

        public string toString
        {
            get
            {
                return String.Format("Code={0};Text={1};Phone={2}", errCode, errMsg, phone);
            }
        }
    }
}

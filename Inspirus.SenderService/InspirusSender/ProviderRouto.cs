using System;
using System.Web;
using System.Text;
using System.Net;
using System.IO;

namespace InspirusSender
{
	/// <summary>
	/// Summary description for ProviderRouto.
	/// </summary>
	public class ProviderRouto : SMSProvider
	{
		public ProviderRouto(string url, string userName, string password, SMSMessage sms)
		{
			this.userName = userName;
			this.password = password;
			this.url = url;
			this.SMSFlash = sms.SMSFlash;
		}

		private string getURL(string recipientList, string senderPhone, string Message) 
		{
			string ret = String.Format("{0}?user={1}&pass={2}&number={3}&ownnum={4}&message={5}", 
				url, userName, password, recipientList, senderPhone, 
				HttpUtility.UrlEncode(Message));			
			if(SMSFlash == 1)
				ret = String.Format("{0}&type=FlashSMS", ret);
			else
				ret = String.Format("{0}&type=SMS", ret);
			return ret;
		}

		public ReplySimple Send(string recipientList, string senderPhone, string Message)	
		{
            //string url = getURL(recipientList, senderPhone, Message);
            //fmSender.Log(this, fmSender.LOG_TRACE, url);
            //resp = new ReplySimple(0, url);

            // UNDONE uncomment the following code
            
			StringBuilder sb = new StringBuilder();
			try 
			{
				string url = getURL(recipientList, senderPhone, Message);
                
                fmSender.Log(this, fmSender.LOG_TRACE, "url=" + url);

				HttpWebRequest h = (HttpWebRequest)WebRequest.Create(url);
				h.Method = "GET";
				h.ContentType="application/x-www-form-urlencoded";
				HttpWebResponse rsp = (HttpWebResponse)h.GetResponse();
				Stream receiveStream = rsp.GetResponseStream();
				Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
				StreamReader readStream = new StreamReader( receiveStream, encode );
				
				Console.WriteLine("\r\nResponse stream received.");
				int n = 1024;
				Char[] read = new Char[n];
				// Reads 256 characters at a time.    
				int count = readStream.Read( read, 0, n );
				Console.WriteLine("HTML...\r\n");
				while (count > 0) 
				{
					// Dumps the 256 characters on a string and displays the string to the console.
					String str = new String(read, 0, count);
					sb.Append(str);
					Console.Write(str);
					count = readStream.Read(read, 0, n);
				}
				Console.WriteLine("");
				rsp.Close();
				readStream.Close();
			
				resp = decode(sb.ToString().Trim());
			} 
			catch (Exception ex )
			{
                fmSender.EventLog(ex.Message);
				resp = new ReplySimple(-999, ex.Message);
			}
			
			return resp;
		}

		private ReplySimple decode(string raw)
		{
			if(raw.Equals("success"))
			{
				resp = new ReplySimple(0, "sending successfuly");
			}
			else if (raw.Equals("failed"))
			{
				resp = new ReplySimple(101, "sending failed");
			}
			else if (raw.Equals("auth_failed"))
			{
				resp = new ReplySimple(102, "incorrect user name and/or password");
			}
			else if (raw.Equals("no_number"))
			{
				resp = new ReplySimple(103, "no phone number given");
			}
			else if (raw.Equals("no_message"))
			{
				resp = new ReplySimple(104, "no message given");
			}
			else if (raw.Equals("wrong_number"))
			{
				resp = new ReplySimple(105, "the number contains non-numeric characters");
			}
			else if (raw.Equals("wrong_message"))
			{
				resp = new ReplySimple(106, "the message contains | character");
			}
			else if (raw.Equals("too_long"))
			{
				resp = new ReplySimple(107, "message exceeds 160 characters");
			}
			else if (raw.Equals("error"))
			{
				resp = new ReplySimple(108, "an unknown error");
			}
			else if (raw.Equals("sys_error"))
			{
				resp = new ReplySimple(109, "gateway system error");
			}
			else if (raw.Equals("wrong_format"))
			{
				resp = new ReplySimple(110, "the wrong message format was selected");
			}
			else if (raw.Equals("wrong_type"))
			{
				resp = new ReplySimple(111, "an incorrect message type was selected");
			}
			else if (raw.Equals("bad_operator"))
			{
				resp = new ReplySimple(112, "wrong operator code");
			}
			else if (raw.Equals("not_allowed"))
			{
				resp = new ReplySimple(113, "you have set some parameters to unacceptable value");
			}
			return resp;
		}

	}
}

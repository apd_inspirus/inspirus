﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using Inspirus.Extractor.Abstract;

using SmartMail.Shared.Data;
using System.IO;
using CustomDev.Toolbox.Smtp;
using Inspirus.Extractor.Entities;
using Inspirus.Extractor.Misc;

namespace Inspirus.Extractor
{
    public sealed class InspirusReportSender : AbstractDoer
    {
        private readonly ErrorList _errorList = new ErrorList();
        private readonly int _emailId;
        private readonly string _postalToEmail;
        private readonly string _folder;

        public InspirusReportSender(string connectionString, StringBuilder reporter, string folder, string postalToEmail)
            : base("Exceptions", reporter, connectionString)
        {
            _folder = folder;
            _postalToEmail = postalToEmail;
        }

        protected override void ProcessIntl()
        {
            string prevDealernum = string.Empty;
            string curDealernum = string.Empty;
            IList<CustomFieldCsv> _records = new List<CustomFieldCsv>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                SqlDataReader reader = SQLHelper.ExecuteDataReader("sp_getcredit", connection, new object[] { });


                while (reader.Read())
                {
                        _records.Add(ReadRow(reader));

                }


                string csv = _records.ToCsv(_builder);
                string csvPath = Path.Combine(_folder, string.Format("{0:yyyyMMdd}_Inspirus_Report.csv", DateTime.Today));
                File.WriteAllText(csvPath, csv);
                SmtpHelper.Send("[Inspirus report data",
                                "Hi, this file is to be sent to you for your review from the Inspirus generated report.",
                                _postalToEmail, true, csvPath);
                _builder.AppendLine(string.Format("Inspirus report {0}", _postalToEmail));
                _records.Clear();






            }
        }


        private CustomFieldCsv ReadRow(SqlDataReader reader)
        {
            try
            {
                CustomFieldCsv record = new CustomFieldCsv()
                {

                    ID = ((string)reader["uid_b2u"]).Trim(),
                    Name = ((string)reader["b2u_name"]).Trim(),
                    SuperCredits=((decimal)reader["SuperCredits"]),
                    BGCredits=((decimal)reader["BGCredits"]),
                    SuperRate=((decimal)reader["super rate"]),
                    BGRate = ((decimal)reader["bg rate"]),
                    Type = ((string)reader["TYPE"]).Trim()

                };
                return record;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _logger.Error(e);
                _errorList.Add(e);
                _builder.AppendLine("We've got 1 unreadable record");
            }
            return null;
        }
    }
}


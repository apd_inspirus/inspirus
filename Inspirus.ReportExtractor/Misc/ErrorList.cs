﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inspirus.Extractor.Misc
{
    sealed class ErrorList
    {
        private static IList<Exception> _errors;
        private static readonly string _header = "Following errors occurred:";

        public ErrorList()
        {
            _errors = new List<Exception>();
        }

        public void Add(Exception e)
        {
            _errors.Add(e);
        }

        public int ErrorCountGet()
        {
            return _errors.Count;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder(_header);
            builder.AppendLine();
            foreach (Exception exception in _errors)
            {
                builder.AppendLine(exception.ToString());
            }
            return builder.ToString();
        }
    }
}
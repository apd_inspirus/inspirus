﻿using CustomDev.Toolbox.Csv;
using Inspirus.Extractor.Entities;
//using Jericho.CS.Entities;

namespace Inspirus.Extractor.Misc
{
    public sealed class CustomFieldCsv
    {
        public CustomFieldCsv(){}

        [Column(1)]
        [Name("ID")]
        public string ID { get; set; }

        [Column(2)]
        [Name("Name")]
        public string Name { get; set; }

        [Column(3)]
        [Name("SuperCredits")]
        public decimal SuperCredits { get; set; }

        [Column(4)]
        [Name("BGCredits")]
        public decimal BGCredits { get; set; }

        [Column(5)]
        [Name("SuperRate")]
        public decimal SuperRate { get; set; }

        [Column(6)]
        [Name("BGRate")]
        public decimal BGRate { get; set; }

        [Column(7)]
        [Name("Type")]
        public string Type { get; set; }


        public CustomFieldCsv(CustomFields customField)
        {
            ID = customField.id;
            Name = customField.name;
            SuperCredits = customField.SuperCredits;
            BGCredits = customField.BGCredits;
            SuperRate = customField.SuperRate;
            BGRate = customField.BGRate;
            Type = customField.Type;

        }
    }
}

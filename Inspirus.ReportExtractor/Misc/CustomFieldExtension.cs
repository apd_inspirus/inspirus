﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CustomDev.Toolbox.Csv;
using Inspirus.Extractor.Entities;
using Inspirus.Extractor.Misc;

namespace Inspirus.Extractor.Misc
{
    public static class CustomFieldExtension
    {

        public static string ToCsv(this IEnumerable<CustomFieldCsv> customFields, StringBuilder reporter)
        {
            int count = customFields.Count();
            reporter.AppendLine(string.Format("{0} to be converted to CSV", count));
            if (count > 0)
            {
                string output = CsvSerializer<CustomFieldCsv>.Serialize(customFields);
                return output;
            }
            return string.Empty;
        }

    }
}
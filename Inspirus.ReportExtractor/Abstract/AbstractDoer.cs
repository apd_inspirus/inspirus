﻿using System;
using System.Text;
using NLog;

namespace Inspirus.Extractor.Abstract
{
    public abstract class AbstractDoer
    {
        protected const string DIVIDER_FORMAT = "------{0}------";

        protected static readonly Logger _logger = LogManager.GetLogger("Nissan.App");
        protected readonly StringBuilder _builder;
        
        protected readonly int _list;
        protected readonly string _context;
        protected readonly string _connectionString;

        protected AbstractDoer(string context, StringBuilder builder, string connectionString)
        {
            //_list = list;
            _context = context;
            _builder = builder;
            _connectionString = connectionString;
        }

        public virtual StringBuilder Process()
        {
            try
            {
                _builder.AppendLine(string.Format(DIVIDER_FORMAT, _context));
                _logger.Info("Context start: {0}", _context);
                ProcessIntl();
                _logger.Info("Context finish: {0}", _context);
                _builder.AppendLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _logger.FatalException("Inspirus.Extractor encountered a really big problem :(", e);
            }
            return _builder;
        }

        protected abstract void ProcessIntl();
    }
}
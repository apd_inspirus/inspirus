﻿using System;
using System.Configuration;
using System.Globalization;
using System.Text;
using CustomDev.Toolbox.Smtp;
using NLog;
using Inspirus.Extractor;

namespace Nissan.App
{
    public sealed class Program
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            try
            {
                StringBuilder reporter = new StringBuilder();
                reporter.AppendLine(string.Format("Application Start: {0:G}", DateTime.Now));

                string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                string folder4Copies = ConfigurationManager.AppSettings["Folder4Copies"];
                string JerichoSupportEmail = ConfigurationManager.AppSettings["JerichoSupportEmail"];
                string CustomerRecepientEmail = ConfigurationManager.AppSettings["CustomerRecepientEmail"];
                
                int index = 0;
                while(args.Length > index)
                {
                    switch (args[index++].ToLower())
                    {
                        case "inspirusextractor":

                            InspirusReportSender extract = new InspirusReportSender(connectionString, reporter, folder4Copies, CustomerRecepientEmail);
                            extract.Process();
                            break;
                    }
                }
                reporter.AppendLine(string.Format("Application Finish: {0:G}", DateTime.Now));
                SmtpHelper.Send("Inspirus Scheduled Run", reporter.ToString(), JerichoSupportEmail, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _logger.FatalException("Ispirus.Extractor failed to start process", e);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inspirus.Extractor.Entities
{
    [Serializable]
    public sealed class CustomFields
    {

        public string id { get; set; }
        public string name { get; set; }
        public decimal SuperCredits { get; set; }
        public decimal BGCredits { get; set; }
        public decimal SuperRate { get; set; }
        public decimal BGRate { get; set; }
        public string Type { get; set; }

    }
}

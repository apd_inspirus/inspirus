using System;
using System.Collections.Generic;
using System.Text;

namespace InspirusScheduler
{
    class TSchedule
    {
        public string uid_smsschedule, uid_b2u, schedule_type, grade_type;

        public TSchedule(string uid_smsschedule, string uid_b2u, string schedule_type, string grade_type)
        {
            this.uid_smsschedule = uid_smsschedule;
            this.uid_b2u = uid_b2u;
            this.schedule_type = schedule_type;
            this.grade_type = grade_type;
        }
    }
}

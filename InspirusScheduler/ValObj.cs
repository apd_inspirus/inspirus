using System;

namespace InspirusScheduler
{
	/// <summary>
	/// Summary description for ValObj.
	/// </summary>
	public class ValObj
	{
		string v1, v2;

		public ValObj(string v1, string v2)
		{
			this.v1 = v1;
			this.v2 = v2;
		}

		public string Value1
		{
			get 
			{
				return v1;
			}
		}

		public string Value2
		{
			get 
			{
				return v2;
			}
		}
	}
}

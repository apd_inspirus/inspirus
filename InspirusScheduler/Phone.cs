namespace InspirusScheduler
{
	/// <summary>
	/// Summary description for Phone.
	/// </summary>
	public class Phone
	{
        public string phone, originPhone, firstName;
		public int gateway;

		public Phone(string phone, int gateway, string originPhone, string firstName)
		{
			this.phone = phone;
			this.gateway = gateway;
			this.originPhone = originPhone;
            this.firstName = firstName;
		}
	}
}

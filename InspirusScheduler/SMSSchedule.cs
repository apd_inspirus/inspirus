using System;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace InspirusScheduler
{
	/// <summary>
	/// populate all fields of this schedule
	/// get phone of all recipients if this is a Group or MergeStaff schedule
	/// Each phone is assigned a gateway
	/// </summary>
	public class SMSSchedule
	{
		public string uid_schedule, SMSOrigin, SMSType, SMSPhoneList, SMSMessage, Grade, B2UID;
		public int SMSPhoneCount, GatewayIndex;
		public int NoCDMA, SMSSupport, SMSFlash;
		public static int USE_DEFAULT_GATEWAY = -99, YES = 1, NO = 0, NA = -1;
		private SqlConnection conn; 
		public ArrayList GroupPhoneList = null;
		private DataSet dsGatewayRange;

		public SMSSchedule(DataRow rs, dsSchedule ds, DataSet dsGatewayRange)
		{
			this.dsGatewayRange = dsGatewayRange;
			GatewayIndex = NzI(rs, 0);
			NoCDMA = NzB(rs, 1);			
			B2UID = NzS(rs, 2);
			SMSMessage = NzS(rs, 3);
			SMSType = NzS(rs, 4);
			SMSPhoneList = NzS(rs, 5);
			SMSSupport = NzB(rs, 6);
			SMSPhoneCount = NzI(rs, 7);
			SMSFlash = NzB(rs, 8);
			Grade = NzS(rs, 9);
			SMSOrigin = NzS(rs, 10);
			//SMSOrigin = System.Web.HttpUtility.UrlDecode(SMSOrigin);
            SMSMessage = System.Web.HttpUtility.UrlDecode(SMSMessage);
            //SMSMessage = HttpUtility.UrlEncode(SMSMessage);
			uid_schedule = NzS(rs,11);

			if(SMSSupport == YES)
				SMSPhoneCount = 1;

//            if (SMSType.Equals("Group") || SMSType.Equals("MergeStaff") || SMSType.Equals("Staff") || SMSType.Equals("Merge"))
//			{
				DataTable tb = ds.Tables["v_recipients"];
				GroupPhoneList = getRecipientPhones(tb);
//			}
		}

		public SMSSchedule(SqlConnection conn, SqlDataReader rs, string uid)
		{
			this.conn = conn;
			this.uid_schedule = uid;
			GatewayIndex = NzI(rs, 0);
			NoCDMA = NzB(rs, 1);			
			B2UID = NzS(rs, 2);
			SMSMessage = NzS(rs, 3);
			SMSType = NzS(rs, 4);
			SMSPhoneList = NzS(rs, 5);
			SMSSupport = NzB(rs, 6);
			SMSPhoneCount = NzI(rs, 7);
			SMSFlash = NzB(rs, 8);
			Grade = NzS(rs, 9);
			SMSOrigin = NzS(rs, 10);
			SMSOrigin = System.Web.HttpUtility.UrlEncode(SMSOrigin);

			rs.Close();
		
			if(SMSSupport == YES)
				SMSPhoneCount = 1;

			if(SMSType.Equals("Group") || SMSType.Equals("MergeStaff") || SMSType.Equals("Staff"))
			{
				GroupPhoneList = _getRecipientPhones();
			}
		}

		private ArrayList getRecipientPhones(DataTable tb)
		{
			ArrayList lists = new ArrayList();
			foreach(DataRow r in tb.Rows)
			{
				string phone = NzS(r, 0);   // client_mobilephone
                string fname = NzS(r, 1);   // client_firstname
				string phone2 = "";
				if(phone.StartsWith("0"))
					phone2 = String.Format("61{0}", phone.Substring(1));
				else if (phone.StartsWith("61"))
					phone2 = phone;
				else
					phone2 = String.Format("61{0}", phone);

				if(NoCDMA == YES)
				{
					Phone p = new Phone(phone2, this.GatewayIndex, phone, fname);
					lists.Add(p);
				}
				else 
				{
					// for some phone, we need to specify a gateway
					int gw = getGatewayIndex(phone2);
					Phone p = new Phone(phone2, gw, phone, fname);
					lists.Add(p);
				}
			}
			return lists;
		}

		private int getGatewayIndex(string phone)
		{
			int gateway = this.GatewayIndex;
			if(dsGatewayRange == null)
				return gateway;

			DataTable tb = dsGatewayRange.Tables["gateway_range"];
			if(tb == null)
				return gateway;

			foreach(DataRow r in tb.Rows)
			{
				string range = Convert.ToString(r[0]);
				if(phone.StartsWith(range))
				{
					gateway = Convert.ToInt32(r[1]);
				}
			}
			
			return gateway;
		}

		private ArrayList _getRecipientPhones()
		{
            //ArrayList tmplists = new ArrayList();
            ArrayList lists = new ArrayList();
            //string sql = String.Format("SELECT client_mobile, client_firstname FROM clients INNER JOIN smsscheduledclients ON " +
            //    " clients.uid_client = smsscheduledclients.uid_client " +
            //    " WHERE smsscheduledclients.uid_smsschedule = '{0}' " +
            //    " AND clients.uid_b2u = '{1}' AND smsscheduledclients.scheduleclient_sent = 0",
            //    uid_schedule, B2UID);
            //SqlCommand cmd = conn.CreateCommand();
            //cmd.CommandText = sql;
            //SqlDataReader rs = cmd.ExecuteReader();
            //while(rs.Read())
            //{
            //    if(!rs.IsDBNull(0))
            //    {
            //        string phone = rs.GetString(0);
            //        string fname = rs.GetString(1);
            //        tmplists.Add(phone);
            //    }
            //}
            //rs.Close();
            //cmd.Dispose();

            //foreach(string phone in tmplists)
            //{
            //    string phone2 = "";
            //    if(phone.StartsWith("0"))
            //        phone2 = String.Format("61{0}", phone.Substring(1));
            //    else if (phone.StartsWith("61"))
            //        phone2 = phone;
            //    else
            //        phone2 = String.Format("61{0}", phone);

            //    if(NoCDMA == YES)
            //    {
            //        Phone p = new Phone(phone2, this.GatewayIndex, phone);
            //        lists.Add(p);
            //    }
            //    else 
            //    {
            //        // for some phone, we need to specify a gateway
            //        int gw = _getGatewayIndex(phone2);
            //        Phone p = new Phone(phone2, gw, phone);
            //        lists.Add(p);
            //    }
				
            //}
			
			return lists;
		}

		private int _getGatewayIndex(string phone)
		{
			int gateway = this.GatewayIndex;
			string sql = "SELECT addranges_range, gateway_id " +
				" FROM addressing_ranges INNER JOIN gateway ON addressing_ranges.uid_gateway = gateway.uid_gateway " +
				" WHERE addressing_ranges.uid_gateway IS NOT NULL or addressing_ranges.uid_gateway <> '' ";
			SqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = sql;
			SqlDataReader rs = cmd.ExecuteReader();
			while(rs.Read())
			{
				string range = Convert.ToString(rs.GetInt32(0));
				if(phone.StartsWith(range))
				{
					gateway = rs.GetInt32(1);
				}
			}
			rs.Close();
			cmd.Dispose();
			return gateway;
		}

		private int NzI(SqlDataReader rs, int i)
		{
			if (rs.IsDBNull(i))
				return USE_DEFAULT_GATEWAY;
			else
				return rs.GetInt32(i);
		}

		private int NzB(SqlDataReader rs, int i)
		{
			if (rs.IsDBNull(i))
				return NA;
			else 
			{
				if (rs.GetBoolean(i))
					return YES;
				else
					return NO;
			}
		}

		private string NzS(SqlDataReader rs, int i)
		{
			if (rs.IsDBNull(i))
				return String.Empty;
			else
				return rs.GetString(i).Trim();
		}

		private int NzI(DataRow rs, int i)
		{
			if (rs.IsNull(i))
				return USE_DEFAULT_GATEWAY;
			else
				return Convert.ToInt32(rs[i]);
		}

		private int NzB(DataRow rs, int i)
		{
			if (rs.IsNull(i))
				return NA;
			else 
			{
				if (Convert.ToBoolean(rs[i]))
					return YES;
				else
					return NO;
			}
		}

		private string NzS(DataRow rs, int i)
		{
			if (rs.IsNull(i))
				return String.Empty;
			else
				return Convert.ToString(rs[i]).Trim();
		}
	}
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Messaging;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Text;

namespace InspirusScheduler
{
	public class Service1 : System.ServiceProcess.ServiceBase
	{
		private System.ComponentModel.IContainer components = null;
		private System.Timers.Timer timer1;
		private System.Data.SqlClient.SqlConnection conn1;

		private int QUEUE_N = 3, timer = 3;
		private System.Messaging.MessageQueue [] mq;
        private System.Messaging.MessageQueue mqBusiness, mqMerge;
		private string db_queue = "";
		private string UpdateState = "N";
		private ArrayList uidList;
        private int QueueCounter = 0;
	    private readonly object _locker = new object();
        //private string sql_deleted = "SELECT uid_smsschedule, smsschedule_datetime FROM smsschedule where smsschedule_complete = -2";
        
		public Service1()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new Service1() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.conn1 = new System.Data.SqlClient.SqlConnection();
            this.timer1 = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).BeginInit();
            // 
            // conn1
            // 
            this.conn1.FireInfoMessageEventOnUserErrors = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Elapsed);
            // 
            // Service1
            // 
            this.ServiceName = "InspirusScheduler";
            ((System.ComponentModel.ISupportInitialize)(this.timer1)).EndInit();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private string getVal(RegistryKey rk, string key, string def)
		{
			string ret = def;
			if(rk.GetValue(key) != null)
				ret = (string)rk.GetValue(key);

			return ret;
		}
		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{			
			Start(null, null);
		}

        public void Start(string connectionString, string q)
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser;
                RegistryKey k = rk.CreateSubKey("Software\\Nextdata\\Inspirus");

                //sql_deleted = "SELECT uid_smsschedule, smsschedule_datetime FROM smsschedule where smsschedule_complete = -2";

                string userid = getVal(k, "db_user", "inspirus");
                string password = getVal(k, "db_pass", "w84r9g7a");
                string host = getVal(k, "db_host", "192.168.91.11");
                string dbname = getVal(k, "db_name", "Inspirus");
                string mq_host = getVal(k, "mq_host", "127.0.0.1");
                string mq_name = q ?? getVal(k, "mq_name", "Queue_");
                QUEUE_N = Convert.ToInt32(getVal(k, "queue_count", "3"));
                UpdateState = getVal(k, "update_state", "N");
                timer = Convert.ToInt32(getVal(k, "timer_sec", "3"));
                //sql_deleted = getVal(k, "sql_deleted", sql_deleted);
                timer1.Interval = timer * 1000;
                /*
                k.SetValue("db_user", userid);
                k.SetValue("db_pass", password);
                k.SetValue("db_host", host);
                k.SetValue("db_name", dbname);
                k.SetValue("mq_host", mq_host);
                k.SetValue("mq_name", mq_name);
                k.SetValue("queue_count", Convert.ToString(QUEUE_N));
                k.SetValue("update_state", UpdateState);
                k.SetValue("timer_sec", Convert.ToString(timer));
                 */
                //k.SetValue("sql_deleted", sql_deleted);

                db_queue = String.Format(
                        ".\\Private$\\{1}DB", mq_host, mq_name);

                string cstr = getVal(k, "cstr", 
                    "packet size=4096;user id={0};data source={1};persist security info=True;"+
                    "initial catalog={2};password={3}");
                host = "192.168.91.11";
                conn1.ConnectionString = connectionString ?? cstr;

                mq = new MessageQueue[QUEUE_N];

                for (int i = 0; i < QUEUE_N; i++)
                {
                    mq[i] = new MessageQueue(String.Format(
                        ".\\Private$\\{1}{2}", mq_host, mq_name, i));
                }

                string path = String.Format(".\\Private$\\{1}Business", mq_host, mq_name);
                mqBusiness = new MessageQueue(path);

                path = String.Format(".\\Private$\\{1}Merge", mq_host, mq_name);
                mqMerge = new MessageQueue(path);

                uidList = new ArrayList();

                this.getUidList();

                timer1.Enabled = true;

                string msg = String.Format("db={0}; mq={1}", host, mq_host);
                EventLog.WriteEntry(msg, EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("OnStart:" + ex.Message, EventLogEntryType.Error);
            }
        }
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			//EventLog.WriteEntry("game over");
            StopMe();
		}

        public void StopMe()
        {
            timer1.Enabled = false;
            this.saveUidList();
        }

		private void timer1_Tick(object sender, System.EventArgs e)
		{
		}


		private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			//EventLog.WriteEntry("timer tick");
            Random random = new Random(DateTime.Now.Millisecond);
			timer1.Enabled = false;
			try 
			{
                ArrayList lists = new ArrayList();
			    lock (_locker)
			    {
			        if (conn1.State != ConnectionState.Open)
			            conn1.Open();

			        // put all deleted schedules into MQ
			        //getDeletedSchedule();

			        string sql = " SELECT TOP 1" + //" SELECT TOP " + random.Next(1, 300) + 
			                     " uid_smsschedule, uid_b2u, smsschedule_type, smsschedule_grade_type, smsschedule_datetime,smsschedule_message FROM smsschedule " +
			                     " WHERE smsschedule_active = 1 AND (smsschedule_immediatesend = 1 OR smsschedule_datetime < GetDate()) " +
			                     " AND smsschedule_complete = 0  " +
			                     " AND smsschedule_sentstatus is null";
			        //" AND smsschedule_complete = 0 AND smsschedule_type <> 'Merge' AND smsschedule_type <> 'MergeStaff' " +
			        //" AND schedule_state = 0" ;				

			        SqlCommand cmd = conn1.CreateCommand();
			        cmd.CommandText = sql;
			        SqlDataReader rs = cmd.ExecuteReader();
			        int k = 0;
			        while (rs.Read())
			        {
			            string uid = rs.GetString(0);
			            DateTime dt = rs.GetDateTime(4);
			            string msg = rs.GetString(5);
			            string key = string.Format("{0};{1}", uid, msg.GetHashCode());
			            //if(!uidList.Contains(key))
			            {
			                string b2u = rs.GetString(1);
			                string type = rs.GetString(2);
			                string grade = "";
			                if (rs.IsDBNull(3))
			                    grade = "SS";
			                else
			                    grade = rs.GetString(3);

			                // limit to 200 schedules
			                //if (uidList.Count >= 200)
			                //    uidList.RemoveAt(0);
			                //uidList.Add(key);

			                TSchedule sch = new TSchedule(uid, b2u, type, grade);
			                lists.Add(sch);
			            }
			        }
			        rs.Close();
			        cmd.Dispose();
			        foreach (TSchedule s in lists)
			        {
			            if (QueueCounter >= QUEUE_N)
			                QueueCounter = 0;
			            int j = QueueCounter;
			            QueueCounter++;

			            string xml = getScheduleXml(s.uid_smsschedule, s.uid_b2u);
			            if (!xml.Equals(String.Empty))
			            {
			                //                        File.AppendAllText("d:\\sms.xml", xml + Environment.NewLine);
			                MessageQueueTransaction mt = new MessageQueueTransaction();
			                try
			                {
			                    mt.Begin();
			                    if (s.grade_type.Equals("BG"))
			                    {
			                        mqBusiness.Send(xml, s.uid_smsschedule, mt);
			                    }
			                    else
			                    {
			                        if (s.schedule_type.StartsWith("Merge"))
			                            mqMerge.Send(xml, s.uid_smsschedule, mt);
			                        else
			                            mq[j].Send(xml, s.uid_smsschedule, mt);
			                    }
			                    mt.Commit();

			                    //if(UpdateState.Equals("Y"))
			                    this.updateState(s.uid_smsschedule);

			                }
			                catch (Exception ex)
			                {
			                    mt.Abort();
			                    EventLog.WriteEntry("timer1_Elapsed:" + ex.Message, EventLogEntryType.Error);
			                }
			            }
			            k++;
			        }
			        if (lists.Count > 0)
			        {
			            MessageQueue m = new MessageQueue(db_queue);
			            MessageQueueTransaction mt = new MessageQueueTransaction();
			            // send gateways if diff
			            try
			            {
			                string xml = this.getGatewayXML();

			                sql = "SELECT addranges_range, gateway_id " +
			                      " FROM addressing_ranges INNER JOIN gateway ON addressing_ranges.uid_gateway = gateway.uid_gateway " +
			                      " WHERE addressing_ranges.uid_gateway IS NOT NULL or addressing_ranges.uid_gateway <> ''";

			                DataSet ds = new DataSet();
			                SqlDataAdapter da = new SqlDataAdapter(sql, conn1);
			                da.Fill(ds, "gateway_range");

			                sql = "select gateway_id, gateway_name, gateway_selected, gateway_bg from gateway where gateway_selected <> 0 order by gateway_selected";
			                da = new SqlDataAdapter(sql, conn1);
			                da.Fill(ds, "ss_route");

			                sql = "select gateway_id, gateway_name, gateway_selected, gateway_bg from gateway where gateway_bg <> 0 order by gateway_bg";
			                da = new SqlDataAdapter(sql, conn1);
			                da.Fill(ds, "bg_route");

			                File.AppendAllText("d:\\gateways.xml", ds.GetXml() + Environment.NewLine);
			                /*mt.Begin();

                            m.Send(ds.GetXml(), "gateway_range", mt);

                            mt.Commit();*/

			                ds.Dispose();
			            }
			            catch (Exception ex)
			            {
			                mt.Abort();
			                EventLog.WriteEntry("timer1_Elapsed:" + ex.Message, EventLogEntryType.Error);
			            }
			        }
			    }
			} 
			catch (Exception ex)
			{
				EventLog.WriteEntry("timer1_Elapsed:" + ex.Message + "<" + conn1.ConnectionString + ">", EventLogEntryType.Error);
				Console.WriteLine(ex.Message);
			}
            timer1.Interval = timer * 1000;
			timer1.Enabled = true;		
		}

		private string getGatewayXML()
		{
			DataSet ds = new DataSet();
			string sql = "SELECT addranges_range, gateway_id " +
				" FROM addressing_ranges INNER JOIN gateway ON addressing_ranges.uid_gateway = gateway.uid_gateway " +
				" WHERE addressing_ranges.uid_gateway IS NOT NULL or addressing_ranges.uid_gateway <> ''";
			SqlDataAdapter da = new SqlDataAdapter(sql, conn1);
			da.Fill(ds, "gateway_range");
			return ds.GetXml();
		}

        private string getXML(string sql, string tableName)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(sql, conn1);
            da.Fill(ds, tableName);
            return ds.GetXml();
        }


		private string getScheduleXml(string uid_sch, string uid_b2u)
		{
			string xml = String.Empty;
			try 
			{
				Dataset1 ds = new Dataset1();
				string sql = String.Format("SELECT gateway.gateway_id, b2u.b2u_nocdma, smsschedule.uid_b2u, smsschedule.smsschedule_message, " +
					" smsschedule.smsschedule_type, smsschedule.smsschedule_sendto, smsschedule.smsschedule_supportsms, " +
                    " smsschedule.smsschedule_creditsused, smsschedule.smsschedule_flash, smsschedule.smsschedule_grade_type, origin.origin_text , smsschedule.uid_smsschedule, smsschedule_datetime " +
					" FROM smsschedule INNER JOIN origin ON smsschedule.uid_origin = origin.uid_origin  " +
					" INNER JOIN b2u ON smsschedule.uid_b2u = b2u.uid_b2u LEFT JOIN gateway " +
					" ON b2u.uid_gateway = gateway.uid_gateway where smsschedule.uid_smsschedule = '{0}'", uid_sch);
				SqlDataAdapter da = new SqlDataAdapter(sql, conn1);
				da.Fill(ds, "v_schedule_details");

                // only picks up sms yet to be sent
                sql = String.Format("SELECT client_mobile, a.client_firstname, client_mergefield " +
					" FROM clients a, smsscheduledclients b " +
					" WHERE a.uid_client = b.uid_client AND b.scheduleclient_sent = 0 " +
					" AND b.uid_smsschedule = '{0}' " +
					" AND a.uid_b2u = '{1}' ", uid_sch, uid_b2u);

				SqlDataAdapter da2 = new SqlDataAdapter(sql, conn1);
				da2.Fill(ds, "v_recipients");
				xml = ds.GetXml();
				ds.Dispose();
				da.Dispose();
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry("getScheduleXml:" +ex.Message, EventLogEntryType.Error);
                timer1.Interval = 60000;
			}
			return xml;
		}

		private bool updateState(string uid)
		{
			bool ret = false;
            string sql = String.Format("update smsschedule set smsschedule_sentstatus = 10 where uid_smsschedule = '{0}'",
				uid);
			try 
			{
				SqlCommand cmd = conn1.CreateCommand();
				cmd.CommandText = sql;
				cmd.ExecuteNonQuery();
				cmd.Dispose();
			} 
			catch (Exception ex)
			{
				EventLog.WriteEntry("updateState:" + ex.Message, EventLogEntryType.Error);
			}
			return ret;
		}

        private void saveUidList()
        {
            //StringBuilder sb = new StringBuilder();
            //foreach (string uid in uidList)
            //{
            //    sb.Append(uid);
            //    sb.Append(";");
            //}
            //RegistryKey rk = Registry.CurrentUser;
            //RegistryKey k = rk.CreateSubKey("Software\\Nextdata\\Inspirus");
            //k.SetValue("uid_list", sb.ToString());
        }

        private void getUidList()
        {
            //if(uidList == null)
            //    uidList = new ArrayList();

            //uidList.Clear();

            //RegistryKey rk = Registry.CurrentUser;
            //RegistryKey k = rk.CreateSubKey("Software\\Nextdata\\Inspirus");
            //string slist = getVal(k, "uid_list", "");
            //if (slist.Length > 0)
            //{
            //    string[] str = slist.Split(';');
            //    foreach (string s in str)
            //    {
            //        if (s.Trim().Length > 0)
            //            uidList.Add(s);
            //    }
            //}
        }

	}
}

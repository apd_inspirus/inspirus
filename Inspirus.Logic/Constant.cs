﻿namespace Inspirus.Logic
{
    public sealed class Constant
    {
        public static readonly string ResultOk = "OK";
        public static readonly int ResultOkCode = 0;

        public static readonly string ResultDeletedSchedule = "Deleted Schedule";
        public static readonly int ResultDeletedScheduleCode = -2;

        public static readonly string ResultSendFailed = "Sending failed";
        public static readonly int ResultSendFailedCode = 101;

        public static readonly string BadResponse = "Bad response";
        public static readonly int BadResponseCode = -998;

        public static readonly int GeneralExceptionCode = -999;

        public static readonly int CriticalErrorCode = -9999;
        public static readonly string CriticalError = "Critical Error";

        public const int USE_DEFAULT_GATEWAY = 0, YES = 1, NO = 0, NA = -1;
        public const int MOBILEWAY_ROAMING = 0,
                         MOBILEWAY_DIRECT = 8,
                         MBLOX = 14,
                         NEXTDIGITAL_SS = 17,
                         NEXTDIGITAL_DIRECT = 18;

        public const int NON_EXISTING_GATEWAY = -99;
    }
}
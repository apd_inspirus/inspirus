﻿using System;
using System.ComponentModel;
using System.Messaging;
using System.Text;
using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;

namespace Inspirus.Logic.Extension
{
    [Target("ExpiringMsmq")]
    public class NlogMsmqTarget : TargetWithLayout
    {
        private readonly MessagePriority _messagePriority = MessagePriority.Normal;

        /// <summary>
        /// Initializes a new instance of the <see cref="NlogMsmqTarget"/> class.
        /// </summary>
        /// <remarks>
        /// The default value of the layout is: <code>${longdate}|${level:uppercase=true}|${logger}|${message}</code>
        /// </remarks>
        public NlogMsmqTarget()
        {
            MessageQueueProxy = new MessageQueueProxy();
            Label = "NLog";
            Encoding = Encoding.UTF8;
            CheckIfQueueExists = true;
        }

        /// <summary>
        /// Gets or sets the name of the queue to write to.
        /// </summary>
        /// <remarks>
        /// To write to a private queue on a local machine use <c>.\private$\QueueName</c>.
        /// For other available queue names, consult MSMQ documentation.
        /// </remarks>
        /// <docgen category='Queue Options' order='10' />
        [RequiredParameter]
        public Layout Queue { get; set; }

        /// <summary>
        /// Gets or sets the label to associate with each message.
        /// </summary>
        /// <remarks>
        /// By default no label is associated.
        /// </remarks>
        /// <docgen category='Queue Options' order='10' />
        [DefaultValue("NLog")]
        public Layout Label { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to create the queue if it doesn't exists.
        /// </summary>
        /// <docgen category='Queue Options' order='10' />
        [DefaultValue(false)]
        public bool CreateQueueIfNotExists { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use recoverable messages (with guaranteed delivery).
        /// </summary>
        /// <docgen category='Queue Options' order='10' />
        [DefaultValue(false)]
        public bool Recoverable { get; set; }

        /// <summary>
        /// Gets or sets the encoding to be used when writing text to the queue.
        /// </summary>
        /// <docgen category='Layout Options' order='10' />
        public Encoding Encoding { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use the XML format when serializing message.
        /// This will also disable creating queues.
        /// </summary>
        /// <docgen category='Layout Options' order='10' />
        [DefaultValue(false)]
        public bool UseXmlEncoding { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to check if a queue exists before writing to it.
        /// </summary>
        /// <docgen category='Layout Options' order='11' />
        [DefaultValue(true)]
        public bool CheckIfQueueExists { get; set; }

        /// <summary>
        /// Gets or sets value indicating TTL in seconds for a message in queue.
        /// </summary>
        [DefaultValue(0)]
        public int SecondsToLive { get; set; }

        internal MessageQueueProxy MessageQueueProxy { get; set; }

        /// <summary>
        /// Writes the specified logging event to a queue specified in the Queue 
        /// parameter.
        /// </summary>
        /// <param name="logEvent">The logging event.</param>
        protected override void Write(LogEventInfo logEvent)
        {
            if (Queue == null)
            {
                return;
            }

            var queue = Queue.Render(logEvent);

            if (CheckIfQueueExists)
            {
                if (!IsFormatNameSyntax(queue) && !MessageQueueProxy.Exists(queue))
                {
                    if (CreateQueueIfNotExists)
                    {
                        MessageQueueProxy.Create(queue);
                    }
                    else
                    {
                        return;
                    }
                }
            }

            var msg = PrepareMessage(logEvent);
            MessageQueueProxy.Send(queue, msg);
        }

        /// <summary>
        /// Prepares a message to be sent to the message queue.
        /// </summary>
        /// <param name="logEvent">The log event to be used when calculating label and text to be written.</param>
        /// <returns>The message to be sent.</returns>
        /// <remarks>
        /// You may override this method in inheriting classes
        /// to provide services like encryption or message 
        /// authentication.
        /// </remarks>
        protected virtual Message PrepareMessage(LogEventInfo logEvent)
        {
            var msg = new Message();
            if (Label != null)
            {
                msg.Label = Label.Render(logEvent);
            }

            msg.Recoverable = Recoverable;
            msg.Priority = _messagePriority;

            if (UseXmlEncoding)
            {
                msg.Body = Layout.Render(logEvent);
            }
            else
            {
                var dataBytes = Encoding.GetBytes(Layout.Render(logEvent));

                msg.BodyStream.Write(dataBytes, 0, dataBytes.Length);
            }
            if (SecondsToLive > 0)
            {
                msg.TimeToBeReceived = TimeSpan.FromSeconds(SecondsToLive);
            }

            return msg;
        }

        private static bool IsFormatNameSyntax(string queue)
        {
            return queue.ToLowerInvariant().IndexOf('=') != -1;
        }
    }

    internal class MessageQueueProxy
    {
        public virtual bool Exists(string queue)
        {
            return MessageQueue.Exists(queue);
        }

        public virtual void Create(string queue)
        {
            MessageQueue.Create(queue);
        }

        public virtual void Send(string queue, Message message)
        {
            if (message == null)
            {
                return;
            }

            using (var mq = new MessageQueue(queue))
            {
                mq.Send(message);
            }
        }
    }
}

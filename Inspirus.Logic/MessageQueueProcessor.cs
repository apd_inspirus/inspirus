using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.IO;
using System.Xml.Serialization;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Model;
using NLog;

namespace Inspirus.Logic
{
	/// <summary>
	/// MessageQueueProcessor will be run in each own thread
	/// it is responsible of get content from the queue
	/// </summary>
	public class MessageQueueProcessor
    {
        protected readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");

	    protected readonly string _queuePath;
	    protected readonly SmsSender _sender;
	    protected readonly MessageProcessorIndexer _messageProcessorIndexer;
	    protected readonly string _shortQName;
	    protected readonly string _qHost;
	    protected readonly bool _isSimulate = true;
	    protected readonly int _limit;

		public MessageQueueProcessor(string queuePath, string qHost, string shortQName, int limit, SmsSender sender, MessageProcessorIndexer messageProcessorIndexer)
		{
			_queuePath = queuePath;
			_qHost = qHost;
            _shortQName = shortQName;
            _limit = limit;
		    _sender = sender;
		    _messageProcessorIndexer = messageProcessorIndexer;
		}

		public void ProcessMessages()
		{
		    MessageQueue queue = new MessageQueue(_queuePath)
		        {
		            Formatter = new XmlMessageFormatter(new[] {typeof (String)})
		        };

		    for (int i = 0; i < _limit; i++)
		    {

		        try
		        {
		            DateTime dt = DateTime.Now;
		            Schedule schedule = new Schedule();

		            Message m = queue.Receive(new TimeSpan(0, 0, 0, 2));
		            if (m != null)
		            {
		                string xml = (string) m.Body;
		                XmlSerializer serializer = new XmlSerializer(typeof (Schedule));
		                schedule = (Schedule) serializer.Deserialize(new StringReader(xml));

		                SmsSchedule sch = GetSmsSchedule(schedule);

                        _logger.Info("SMS is parsed. Id: {0}, gateway-in: {1}, grade: {2}", sch.UidSchedule, sch.GatewayId, sch.Grade);
		                i = Do(sch, xml, dt, i);
		            }
		        }
		        catch (MessageQueueException mex)
		        {
		            if (mex.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
		            {
		                _logger.ErrorException(string.Format("Message Queue Receive failed. Path='{0}'", _queuePath), mex);
		            }
		        }
		        catch (Exception ex)
		        {
		            _logger.ErrorException("Message Queue Receive failed.", ex);
		        }
		    }
		}

	    protected virtual int Do(SmsSchedule schedule, string xml, DateTime dt, int i)
	    {
	        IMessageProcessor processor = _messageProcessorIndexer[schedule.SmsType];
            IEnumerable<int> gateways = processor.GetGateways(schedule.Grade, schedule.GatewayId);
            if (gateways.Any())
            {
                foreach (Phone p in schedule.GroupPhoneList)
                {
                    p.Gateway = Convert.ToInt32(gateways.First());
                }
            }
	        bool result = processor.Process(dt, schedule, _sender, _qHost, _shortQName);
	        if (result)
	        {
                return 1000; //comment from original code: 'so we will stop this thread and start sending group sms'
	        }
	        return i;
	    }


		/// <summary>
		/// get all details of this schedule
		/// </summary>
		/// <param name="schedule"></param>
		/// <returns></returns>
		/// 
		private SmsSchedule GetSmsSchedule(Schedule schedule)
		{
			SmsSchedule sch = null;
			IList<ScheduleDetails> details = schedule.Details;
			if(details.Count > 0)
			{
                sch = new SmsSchedule(details[0], schedule.Recipients);
			}
			return sch;
		}
	}
}
﻿namespace Inspirus.Logic.Interface
{
    public interface IQueueProcessor
    {
        void Process();
    }
}
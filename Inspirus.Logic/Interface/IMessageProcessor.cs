﻿using System;
using System.Collections.Generic;

namespace Inspirus.Logic.Interface
{
    public interface IMessageProcessor
    {
        string SmsChannel { get; }
        IPhonesValidator Validator { get; }

        bool Process(DateTime start, SmsSchedule schedule, SmsSender sender, string qHost, string qName);

        IEnumerable<int> GetGateways(string grade, int preferredId, int @default = Constant.MOBILEWAY_ROAMING);
    }
}
﻿using System.Collections.Generic;

namespace Inspirus.Logic.Interface
{
    public interface ISmsProvider
    {
        int GatewayId { get; }
        string Grade { get; }
        string Username { get; }
        string Password { get; }
        string Url { get; }
        int Order { get; set; }
        ProviderResult Send(ISms sms);
    }
}
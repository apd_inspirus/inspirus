﻿namespace Inspirus.Logic.Interface
{
    public interface IPhonesValidator
    {
        string GetValidateList(string phones);
    }
}
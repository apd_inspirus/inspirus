﻿namespace Inspirus.Logic.Interface
{
    public interface IService
    {
        void Start();
        void Stop();
    }
}
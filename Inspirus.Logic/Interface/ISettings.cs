﻿using System.Data;

namespace Inspirus.Logic.Interface
{
    public interface ISettings
    {
        int TimerIntervalSec { get; }
        int MBloxTimerIntervalSec { get; }
        int QNumber { get; }
        int Limit { get; }
        string UserId { get; }
        string Password { get; }
        string DbHost { get; }
        string DbName { get; }
        string MqHost { get; }
        string MqName { get; set; }

        void Load(string registryPath);
        void Save(string registryPath);

        DataSet GetGatewayRangeDataSet();

        string ConnectionString { get; }
    }
}
﻿using System;

namespace Inspirus.Logic.Interface
{
    public interface ISms
    {
        string Id { get; }
        string Recipients { get; }
        string Sender { get; }
        string Message { get; }
        bool SmsFlash { get; }
        DateTime StartTime { get; }
        string UidB2U { get; }
        string OriginalPhoneList { get; }
    }
}
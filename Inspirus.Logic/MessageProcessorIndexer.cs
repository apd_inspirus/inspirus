﻿using System;
using System.Collections.Generic;
using Inspirus.Logic.Interface;

namespace Inspirus.Logic
{
    public class MessageProcessorIndexer
    {
        protected readonly Dictionary<string, IMessageProcessor> _processors;
        
        public MessageProcessorIndexer(IEnumerable<IMessageProcessor> processors)
        {
            _processors = new Dictionary<string, IMessageProcessor>();
            foreach (IMessageProcessor processor in processors)
            {
                _processors.Add(processor.SmsChannel, processor);
            }
        }

        public IMessageProcessor this[string channel]
        {
            get
            {
                if (_processors.ContainsKey(channel))
                {
                    return _processors[channel];
                }
                throw new NotImplementedException(string.Format("Message Processor with channel = {0} is not implemented or injected", channel));
            }
        }
    }
}
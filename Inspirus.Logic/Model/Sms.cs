﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml;
using Inspirus.Logic.Interface;

namespace Inspirus.Logic.Model
{
    public class Sms : ISms
    {
        public string OriginalPhoneList { get; protected set; }
        public IEnumerable<int> Gateways { get; protected set; }
        public DateTime StartTime { get; protected set; }
        public string Id { get; protected set; }
        public string Recipients { get; protected set; }
        public string Sender { get; protected set; }
        public string Message { get; protected set; }
        public string UidB2U { get; protected set; }
        public bool SmsFlash { get; protected set; }
        
        public Sms(DateTime dt, string id, string recipientList, string senderPhone, string message,
            IEnumerable<int> gateways, string origPhoneList, string uidB2U, bool smsFlash)
        {
            Id = id;
            Recipients = recipientList;
            Sender = senderPhone;

            Message = message;
            Gateways = gateways;
//            this.q_host = q_host;
            OriginalPhoneList = origPhoneList;
            StartTime = dt;
            UidB2U = uidB2U;
            SmsFlash = smsFlash;
        }

        public Sms(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            if (doc.DocumentElement != null) 
            {
                XmlNode node = doc.DocumentElement.FirstChild;
                long ticks = Convert.ToInt64(node.InnerText);
                StartTime = new DateTime(ticks);

                node = node.NextSibling;
                if (node != null)
                {
                    Id = node.InnerText;
                }

                if (node != null)
                {
                    node = node.NextSibling;
                }
                if (node != null)
                {
                    UidB2U = node.InnerText;
                }

                if (node != null)
                {
                    node = node.NextSibling;
                }
                if (node != null)
                {
                    Recipients = node.InnerText;
                }

                if (node != null)
                {
                    node = node.NextSibling;
                }
                if (node != null)
                {
                    Sender = node.InnerText;
                }

                if (node != null)
                {
                    node = node.NextSibling;
                }
                if (node != null)
                {
                    Message = node.InnerText;
                }

                if (node != null)
                {
                    node = node.NextSibling;
                }

                if (node != null)
                {
                    SmsFlash = Convert.ToBoolean(node.InnerText);
                }

                /*if (node != null)
                {
                    node = node.NextSibling;
                }
                if (node != null)
                {
                    q_host = node.InnerText;
                }*/

                if (node != null)
                {
                    node = node.NextSibling;
                }
                if (node != null)
                {
                    OriginalPhoneList = node.InnerText;
                }

                if (node != null)
                {
                    node = node.NextSibling;
                }
                if (node != null) 
                {
                    string[] gw = node.InnerText.Split(';');
                    List<int> gateways = new List<int>();
                    foreach (string str in gw)
                    {
                        if (str.Length > 0)
                        {
                            gateways.Add(Convert.ToInt32(str));
                        }
                    }
                    Gateways = gateways;
                }
            }
        }

        public override string ToString()
        {
            return string.Format("uid={0},b2u={1},phones={2}", Id, UidB2U, OriginalPhoneList);
        }

        public string ToXml()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><sms/>");
            AddElement(doc, "start_time", StartTime.Ticks.ToString(CultureInfo.InvariantCulture));
            AddElement(doc, "uid", Id);
            AddElement(doc, "uid_b2u", UidB2U);
            AddElement(doc, "recipients", Recipients);
            AddElement(doc, "sender", Sender);
            AddElement(doc, "message", Message);
            AddElement(doc, "flash", SmsFlash.ToString());
//            AddElement(doc, "q_host", q_host);
            AddElement(doc, "origRecipients", OriginalPhoneList);
            StringBuilder sb = new StringBuilder();
            foreach (int gw in Gateways)
            {
                sb.Append(gw);
                sb.Append(";");
            }
            AddElement(doc, "gateways", sb.ToString());
            sb.Length = 0;
            XmlWriter xw = XmlWriter.Create(sb);
            doc.WriteTo(xw);
            xw.Close();

            return sb.ToString();
        }

        private static void AddElement(XmlDocument doc, string name, string text)
        {
            XmlElement elm = doc.CreateElement(name);
            XmlText xtext = doc.CreateTextNode(text);

            if (doc.DocumentElement != null) 
            {
                doc.DocumentElement.AppendChild(elm);
                doc.DocumentElement.LastChild.AppendChild(xtext);
                return;
            }
            throw new NullReferenceException("doc.DocumentElement is null.");
        }
    }
}
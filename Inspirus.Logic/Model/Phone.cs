﻿namespace Inspirus.Logic.Model
{
    public sealed class Phone
    {
        public string Number { get; private set; }
        public string OriginalPhone { get; private set; }
        public string FirstName { get; private set; }
        public string OtherField { get; private set; }
        public int Gateway { get; set; }

        public Phone(string phone, int gateway, string originalPhone, string firstName, string otherField)
        {
            Number = phone;
            Gateway = gateway;
            OriginalPhone = originalPhone;
            FirstName = firstName;
            OtherField = otherField;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Inspirus.Logic.Model
{
    [XmlRoot("Dataset1", Namespace = "http://tempuri.org/Dataset1.xsd")]
    public class Schedule
    {
        [XmlElement("v_recipients")]
        public List<ScheduleRecipient> Recipients { get; set; }
        
        public List<ScheduleSettings> Settings { get; set; }
        
        public List<ScheduleResponse> Responses { get; set; }

        [XmlElement("v_schedule_details")]
        public List<ScheduleDetails> Details { get; set; }

        public List<ScheduleRecord> Records { get; set; }
    }

    public class ScheduleRecipient
    {
        public ScheduleRecipient()
        {
            FirstName = Mobile = MergeField = string.Empty;
        }

        [XmlElement("client_firstname")]
        public string FirstName { get; set; }
        [XmlElement("client_mobile")]
        public string Mobile { get; set; }
        [XmlElement("client_mergefield")]
        public string MergeField { get; set; }
    }

    public class ScheduleSettings
    {
        public string DbUser { get; set; }
        public string DbPass { get; set; }
        public string DbServer { get; set; }
        public string DbName { get; set; }
        public string MqServer { get; set; }
        public string MqName { get; set; }
    }

    [XmlRoot("dsSchedule", Namespace = "http://tempuri.org/dsSchedule.xsd")]
    public class ScheduleResponseRoot
    {
        [XmlElement("response")]
        public ScheduleResponse Response { get; set; }
    }

    public class ScheduleResponse
    {
        [XmlElement(ElementName = "uid_schedule")]
        public string ScheduleId { get; set; }

        [XmlElement(ElementName = "code")]
        public int Code { get; set; }

        public string Message { get; set; }

        [XmlElement(ElementName = "time_sent")]
        public DateTime TimeSent { get; set; }

        [XmlElement(ElementName = "state")]
        public int State { get; set; }

        [XmlElement(ElementName = "gateway")]
        public string Gateway { get; set; }

        [XmlElement(ElementName = "recipient_list")]
        public string RecipientList { get; set; }

        [XmlElement(ElementName = "recipient_count")]
        public int RecipientCount { get; set; }

        [XmlElement(ElementName = "time_start")]
        public DateTime TimeStart { get; set; }

        [XmlElement(ElementName = "orig_phone_list")]
        public string OrigPhoneList { get; set; }

        [XmlElement(ElementName = "b2u")]
        public string B2U { get; set; }
    }

    public class ScheduleDetails
    {
        public ScheduleDetails()
        {
            GatewayId = Constant.NA;
            SmsScheduleSendTo = string.Empty;
            SmsScheduleGradeType = string.Empty;
        }

        [XmlElement("gateway_id")]
        public int GatewayId { get; set; }
        [XmlElement("b2u_nocdma")]
        public bool B2UNoCdma { get; set; }
        [XmlElement("uid_b2u")]
        public string UidB2U { get; set; }
        [XmlElement("smsschedule_message")]
        public string SmsScheduleMessage { get; set; }
        [XmlElement("smsschedule_type")]
        public string SmsScheduleType { get; set; }
        [XmlElement("smsschedule_sendto")]
        public string SmsScheduleSendTo { get; set; }
        [XmlElement("smsschedule_supportsms")]
        public bool SmsScheduleSupportSms { get; set; }
        [XmlElement("smsschedule_creditsused")]
        public int SmsScheduleCreditsUsed { get; set; }
        [XmlElement("smsschedule_flash")]
        public bool SmsScheduleFlash { get; set; }
        [XmlElement("smsschedule_grade_type")]
        public string SmsScheduleGradeType { get; set; }
        [XmlElement("origin_text")]
        public string OriginText { get; set; }
        [XmlElement("uid_smsschedule")]
        public string UidSmsSchedule { get; set; }
    }

    public class ScheduleRecord
    {
        public string UidSmsSchedule { get; set; }
        public string SmsScheduleMessage { get; set; }
        public DateTime SmsScheduleDateTime { get; set; }
        public bool SmsScheduleActive { get; set; }
        public bool SmsScheduleImmediateSend { get; set; }
        public bool SmsScheduleComplete { get; set; }
        public bool SmsScheduleSupportSms { get; set; }
        public int SmsScheduleCreditsUsed { get; set; }
        public decimal SmsScheduleCreditsB2U { get; set; }
        public bool SmsScheduleFlash { get; set; }
        public string SmsScheduleType { get; set; }
        public string SmsScheduleSendTo { get; set; }
        public string SmsScheduleConfirmUid { get; set; }
        public string SmsScheduleConfirmMobile { get; set; }
        public string SmsScheduleConfirmMsg { get; set; }
        public string SmsScheduleGradeType { get; set; }
        public string UidOrigin { get; set; }
        public string UidMember { get; set; }
        public string UidB2U { get; set; }
        public short ScheduleState { get; set; }
        public DateTime SendDateTime { get; set; }
    }
}
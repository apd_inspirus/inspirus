﻿using System;
using System.Messaging;
using NLog;

namespace Inspirus.Logic
{
    public sealed class QueueManager : IDisposable
    {
        private static readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");
        private static readonly object _locker = new object();
        public string Path { get; private set; }
        private readonly MessageQueue _q;

        public QueueManager(string path)
        {
            Path = path;
            _q = CreateIfNotExists(path);
            _q.Formatter = new XmlMessageFormatter(new[] { typeof(String) });
        }

        private static MessageQueue CreateIfNotExists(string queueName)
        {
            lock (_locker)
            {
                if (!MessageQueue.Exists(queueName))
                {
                    MessageQueue.Create(queueName, true);
                }
            }
            return new MessageQueue(queueName);
        }

        public bool Send(string message)
        {
            try
            {
                _q.Send(new Message(message));
                return true;
            }
            catch (Exception e)
            {
                _logger.ErrorException(string.Format("QueueManager Send failed. Message='{0}', Path='{1}'", message, Path), e);
            }
            return false;
        }

        public static void PostTransaction(MessageQueueTransaction transaction, string host, string path, string prefix, string uid, string xml, string label = null)
        {
            _logger.Debug("PostTransaction xml:'{0}'", xml);
            bool isSelfManaged = false;
            if (transaction == null)
            {
                transaction = new MessageQueueTransaction();
                isSelfManaged = true;
            }
            try
            {
                if (isSelfManaged)
                {
                    transaction.Begin();
                }
                path = Util.GetQueueName(host, path, prefix);
                MessageQueue queue = CreateIfNotExists(path);
                queue.Send(xml, label ?? uid, transaction);
                if (isSelfManaged)
                {
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (isSelfManaged)
                {
                    transaction.Abort();
                }
                _logger.ErrorException(string.Format("QueueManager PostTransaction failed. SmsId: {0}", uid), ex);
            }
            if (isSelfManaged)
            {
                transaction.Dispose();
            }
        }

        public void Dispose()
        {
            _q.Dispose();
        }

        public string ReceiveString(decimal value)
        {
            try
            {
                Message message = _q.Receive(TimeSpan.FromSeconds((double) value));
                if (message != null)
                {
                    return (string) message.Body;
                }
            }
            catch (Exception e)
            {
                _logger.ErrorException(string.Format("QueueManager ReceiveString failed. Path='{0}'", Path), e);
            }
            return null;
        }
    }
}
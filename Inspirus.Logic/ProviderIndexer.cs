﻿using System;
using System.Collections.Generic;
using System.Data;
using Inspirus.Logic.Interface;
using NLog;
using System.Linq;

namespace Inspirus.Logic
{
    public class ProviderIndexer
    {
        public static ProviderIndexer Instance { get; private set; }
        static ProviderIndexer()
        {
            Instance = new ProviderIndexer();
        }

        protected readonly Dictionary<int, ISmsProvider> _mainProviders = new Dictionary<int, ISmsProvider>();
        protected readonly Logger _logger = LogManager.GetLogger("ProviderIndexer");
        protected readonly object _locker = new object();

        public void Init(IEnumerable<ISmsProvider> providers)
        {
            foreach (ISmsProvider provider in providers)
            {
                if (_mainProviders.ContainsKey(provider.GatewayId))
                {
                    continue;
                }
                _mainProviders.Add(provider.GatewayId, provider);
            }
        }

        protected void ReorderWith(IDictionary<int, int> orderMap)
        {
            lock (_locker)
            {
                foreach (KeyValuePair<int, int> pair in orderMap)
                {
                    if (_mainProviders.ContainsKey(pair.Key))
                    {
                        _mainProviders[pair.Key].Order = pair.Value;
                    }
                    else
                    {
                        _logger.Warn("SMS Provider with GatewayID = {0} is not present in available providers list.", pair.Key);
                    }
                }
            }
        }

        public ISmsProvider this[int gatewayId]
        {
            get
            {
                if (_mainProviders.ContainsKey(gatewayId))
                {
                    return _mainProviders[gatewayId];
                }
                throw new NotImplementedException(string.Format("SMS Provider with GatewayID = {0} is not implemented or injected", gatewayId));
            }
        }

        public void Refresh(DataSet gatewayRangeDataSet)
        {
            IDictionary<int,int> mapping = new Dictionary<int, int>();
            foreach (DataTable table in gatewayRangeDataSet.Tables)
            {
                string tableName = table.TableName.ToLower();
                if (tableName.EndsWith("_route"))
                {
                    foreach (DataRow dataRow in table.Rows)
                    {
                        int order;
                        if (tableName == "ss_route")
                        {
                            order = Convert.ToInt32(dataRow["gateway_selected"]);
                        }
                        else
                        {
                            string grade = tableName.Remove(tableName.Length - 6);
                            order = Convert.ToInt32(dataRow[string.Format("gateway_{0}", grade)]);
                        }
                        int gatewayId = Convert.ToInt32(dataRow["gateway_id"]);
                        mapping.Add(gatewayId, order);
                    }
                }
            }
            ReorderWith(mapping);
        }

        public IEnumerable<ISmsProvider> GetGradeGateways(string grade)
        {
            return _mainProviders.Values.Where(p => p.Grade == grade.ToUpper()).OrderBy(p => p.Order).ThenBy(p => p.GatewayId);
        }
    }
}
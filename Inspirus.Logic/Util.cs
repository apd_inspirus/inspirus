﻿using System;
using System.Text;
using Microsoft.Win32;

namespace Inspirus.Logic
{
    public sealed class Util
    {
        public static string GetQueueName(string host, string shortQName, string prefix = "")
        {
            if (host.Equals("localhost") || host.Equals("127.0.0.1"))
            {
                return GetLocalQueueName(shortQName, prefix);
            }
            return string.Format("FormatName:direct=tcp:{0}\\Private$\\{1}{2}", host, prefix, shortQName);
        }

        public static string GetLocalQueueName(string shortQName, string prefix = "")
        {
            return string.Format(".\\Private$\\{0}{1}", prefix, shortQName);
        }

        public static string NewId()
        {
            return Guid.NewGuid().ToString("N");
        }
    }

    public static class RegistryExtension
    {
        public static string WithDefault(this RegistryKey rk, string key, string def)
        {
            string ret = def;
            if (rk.GetValue(key) != null)
            {
                ret = (string)rk.GetValue(key);
            }
            return ret;
        }
    }

    public static class BytesExtension
    {
        public static int GetInt(this byte[] buffer, int begin, int length)
        {
            StringBuilder bu = new StringBuilder();
            for (int i = begin; i < begin + length; i++)
            {
                bu.Append(buffer[i]);
            }
            return int.Parse(bu.ToString());
        }
    }
}
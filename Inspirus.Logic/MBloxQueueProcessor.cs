﻿using System;
using System.Messaging;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Model;
using NLog;

namespace Inspirus.Logic
{
    public class MBloxQueueProcessor
    {
        protected readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");

        protected readonly MessageQueue _mq;
        protected readonly ISmsProvider _provider;
        
        public MBloxQueueProcessor(string path, ISmsProvider provider)
        {
            _provider = provider;
            _mq = new MessageQueue(path)
                {
                    Formatter = new XmlMessageFormatter(new[] {typeof (String)})
                };
        }

        public void Process()
        {
            MessageQueueTransaction mt = new MessageQueueTransaction();
            for (int i = 0; i < 500; i++)
            {
                try
                {
                    mt.Begin();
                    var m = _mq.Receive(new TimeSpan(0, 0, 0, 1), mt);
                    if (m != null) 
                    {
                        string xml = (string)m.Body;
                        Sms sms = new Sms(xml);
                        bool success = SmsSender.SendMBlox(sms, _provider);
                        if (success)
                        {
                            mt.Commit();
                        }
                        else
                        {
                            mt.Abort();
                            break;
                        }
                    }
                }
                catch (MessageQueueException mex)
                {
                    if (mex.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                    {
                        _logger.ErrorException(string.Format("SQP:{0}", mex.Message), mex);
                    }
                    mt.Abort();
                    break;
                }
                catch (Exception ex)
                {
                    mt.Abort();
                    _logger.ErrorException(string.Format("SQP:{0}", ex.Message), ex);
                    break;
                }
            }
        }
    }
}
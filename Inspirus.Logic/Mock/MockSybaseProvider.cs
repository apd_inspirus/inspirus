﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Inspirus.Logic.Implementation;
using Inspirus.Logic.Interface;

namespace Inspirus.Logic.Mock
{
    public class MockSybaseProvider : SybaseProvider
    {
        public MockSybaseProvider(string username, string password, int gatewayId, int order, string url, string host, string replyUrl, string grade, string mobileCode = "61") 
            : base(username, password, gatewayId, order, url, host, replyUrl, grade, mobileCode) {}

        public override ProviderResult Send(ISms sms)
        {
            try
            {
                string postdata = GetData(sms.Recipients, sms.Sender, sms.Message, sms.SmsFlash);
                string str = string.Format("{0}:{1}", Username, Password);
                string message = string.Format(@"POST {0} HTTP/1.1
HOST: {1}
Authorization: Basic {3}
CONTENT-LENGTH:{4}

{2}",
                                               Url,
                                               Host,
                                               postdata,
                                               Convert.ToBase64String(Encoding.UTF8.GetBytes(str)),
                                               postdata.Length);
                _logger.Debug(string.Format("{0}:POST='{1}'", sms.Id, message));
                Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPHostEntry hostInfo = Dns.GetHostEntry(Host);
                IPEndPoint hostEndPoint = new IPEndPoint(hostInfo.AddressList[0], 80);

//                s.Connect(hostEndPoint);
//                if (s.Connected)
                if(true)
                {
                    MockDb.ConfirmSend(sms.Id);
                    string builder = "123 #Message Receive correctly ORDERID success <br>";
                    ProviderResult result = DeduceResult(builder);
                    if (result.Code == Constant.ResultOkCode)
                    {
                        _logger.Trace(string.Format("Successful send. Provider: Sybase, SmsId: {0}", sms.Id));
                    }
                    else
                    {
                        _logger.Error(string.Format("Sms send failed ({1}, data:'{2}'). Provider: Sybase, SmsId: {0}", sms.Id, result.Description, result.Data));
                    }
                    return result;
                }
                return new ProviderResult(Constant.BadResponseCode, "Not able to connect");
            }
            catch (Exception ex)
            {
                _logger.ErrorException(string.Format("SybaseProvider Send failed. SmsId: {0}", sms.Id), ex);
                return ProviderResult.Exception(ex);
            }
        }
    }
}

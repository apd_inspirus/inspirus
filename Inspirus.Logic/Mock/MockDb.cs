﻿using System.Data.SqlClient;

namespace Inspirus.Logic.Mock
{
    public class MockDb
    {
        private static readonly string _connection = @"Data Source=.\dev7sql;Initial Catalog=InSpiritus;Integrated Security=True";

        public static void ConfirmSend(string smsId)
        {
            lock (_connection)
            {
                using (SqlConnection connection = new SqlConnection(_connection))
                {
                    SqlCommand command = new SqlCommand(string.Format("DELETE FROM smsschedule WHERE uid_smsschedule = '{0}'", smsId), connection);
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
﻿using System.Configuration;
using Inspirus.Logic.Implementation;

namespace Inspirus.Logic.Mock
{
    public class MockSettings : Settings
    {
        public override string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["MockContext"].ConnectionString; }
        }
    }
}
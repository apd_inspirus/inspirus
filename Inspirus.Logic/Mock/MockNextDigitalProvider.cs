﻿using System;
using Inspirus.Logic.Implementation;
using Inspirus.Logic.Interface;

namespace Inspirus.Logic.Mock
{
    public class MockNextDigitalProvider : NextDigitalProvider
    {
        public MockNextDigitalProvider(string username, string password, int gatewayId, int order, string url, string route, string grade, string mobileCode = "61") 
            : base(username, password, gatewayId, order, url, route, grade, mobileCode) {}

        public override ProviderResult Send(ISms sms)
        {
            try
            {
                string url = GetUrl(sms.Recipients, sms.Sender, sms.Message);
                _logger.Debug(url);
                MockDb.ConfirmSend(sms.Id);
                string s = "0";
                ProviderResult result = DeduceResult(s.Trim());
                if (result.Code == Constant.ResultOkCode)
                {
                    _logger.Trace(string.Format("Successful send. Provider: NextDigital, SmsId: {0}", sms.Id));
                }
                else
                {
                    _logger.Error(string.Format("Sms send failed ({1}, data:'{2}'). Provider: NextDigital, SmsId: {0}", sms.Id, result.Description, result.Data));
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.ErrorException(string.Format("NextDigitalProvider Send failed. SmsId: {0}", sms.Id), ex);
                return ProviderResult.Exception(ex.Message);
            }
        }
    }
}
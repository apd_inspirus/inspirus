﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inspirus.Logic.Implementation;
using Inspirus.Logic.Interface;

namespace Inspirus.Logic.Mock
{
    public class MockMBloxProvider : MBloxProvider
    {
        public MockMBloxProvider(string username, string password, int gatewayId, int order, string url, string grade, string mobileCode = "61") 
            : base(username, password, gatewayId, order, url, grade, mobileCode) {}

        public override ProviderResult Send(ISms sms)
        {
            try
            {
                string xmlData = GetData(sms);
                _logger.Debug(xmlData);
                MockDb.ConfirmSend(sms.Id);
                string response =
                    string.Format(
                        "<root><NotificationResult><SubscriberNumber>{0}</SubscriberNumber><SubscriberResultCode>0</SubscriberResultCode><SubscriberResultText>OK</SubscriberResultText></NotificationResult></root>",
                        sms.Recipients);
                IEnumerable<ProviderResult> results = DeduceResult(response);
                ProviderResult first = results.FirstOrDefault(r => r.Code != Constant.ResultOkCode);
                return first ?? ProviderResult.Ok;
            }
            catch (Exception ex)
            {
                _logger.ErrorException(string.Format("MBloxProvider Send failed. SmsId: {0}", sms.Id), ex);
                return ProviderResult.Exception(ex.Message);
            }
        }
    }
}

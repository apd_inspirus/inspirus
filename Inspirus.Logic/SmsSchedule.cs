﻿using System;
using System.Collections;
using System.Collections.Generic;
using Inspirus.Logic.Model;

namespace Inspirus.Logic
{
    public sealed class SmsSchedule
    {
        public string UidSchedule;
        public string SmsOrigin;
        public string SmsType;
        public string SmsPhoneList;
        public string SmsMessage;
        public string Grade;
        public string B2Uid;
        public int SmsPhoneCount;
        public int GatewayId;
        public bool NoCdma;
        public bool SmsSupport;
        public bool SmsFlash;

        public ArrayList GroupPhoneList = null;
        public string MobileCode;

		public SmsSchedule(ScheduleDetails details, IEnumerable<ScheduleRecipient> groupPhoneList, string mobileCode = "61")
		{
            GatewayId = details.GatewayId;
            NoCdma = details.B2UNoCdma;
            B2Uid = details.UidB2U.Trim();
            SmsMessage = details.SmsScheduleMessage.Trim();
            SmsType = details.SmsScheduleType.Trim();
            SmsPhoneList = details.SmsScheduleSendTo.Trim();
            SmsSupport = details.SmsScheduleSupportSms;
            SmsPhoneCount = details.SmsScheduleCreditsUsed;
            SmsFlash = details.SmsScheduleFlash;
            Grade = details.SmsScheduleGradeType.Trim();
            SmsOrigin = details.OriginText.Trim();
            UidSchedule = details.UidSmsSchedule.Trim();
		    MobileCode = mobileCode;
            if (SmsOrigin.StartsWith(MobileCode))
            {
                SmsOrigin = string.Format("+{0}", SmsOrigin);
            }
			SmsMessage = System.Web.HttpUtility.UrlDecode(SmsMessage);
            if (SmsMessage != null && SmsMessage.Length > 160)
            {
                SmsMessage = SmsMessage.Substring(0, 160);
            }
			if (SmsSupport)
			{
			    SmsPhoneCount = 1;
			}
			GroupPhoneList = GetRecipientPhones(groupPhoneList);
		}

        //note pp phone validator?
        private ArrayList GetRecipientPhones(IEnumerable<ScheduleRecipient> recipients)
        {
            ArrayList lists = new ArrayList();
            foreach (ScheduleRecipient recipient in recipients)
            {
                string phone = recipient.Mobile;
                string firstName = recipient.FirstName;
                string otherFld = recipient.MergeField;
                string phone2;
                if (phone.StartsWith("0"))
                {
                    phone2 = String.Format("{0}{1}", MobileCode, phone.Substring(1));
                }
                else if (phone.StartsWith(MobileCode))
                {
                    phone2 = phone;
                }
                else
                {
                    phone2 = String.Format("{0}{1}", MobileCode, phone);
                }
                Phone p = new Phone(phone2, GatewayId, phone, firstName, otherFld);
                lists.Add(p);
            }
            return lists;
        }
    }
}
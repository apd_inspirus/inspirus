﻿using System;

namespace Inspirus.Logic
{
    public sealed class ProviderResult
    {
        private static readonly ProviderResult _resultOk = new ProviderResult(Constant.ResultOkCode, Constant.ResultOk);
        private static readonly ProviderResult _resultFailedSend = new ProviderResult(Constant.ResultSendFailedCode, Constant.ResultSendFailed);

        public int Code { get; set; }
        public string Description { get; set; }
        public object Data { get; set; }

        public static ProviderResult Ok { get { return _resultOk; } }
        public static ProviderResult SendFailed { get { return _resultFailedSend; } }
        
        public ProviderResult(int code, string description)
        {
            Code = code;
            Description = description;
        }

        public ProviderResult(int code, string description, object data):this(code, description)
        {
            Data = data;
        }

        public ProviderResult(int code) : this(code, string.Empty) {}

        public static ProviderResult Exception(string message)
        {
            return new ProviderResult(Constant.GeneralExceptionCode, message);
        }

        public static ProviderResult Exception(Exception exception)
        {
            return new ProviderResult(Constant.GeneralExceptionCode, exception.Message);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Model;
using NLog;

namespace Inspirus.Logic
{
    public class SmsSender
    {
        protected static readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");

        private readonly SqlConnection _connection;
        private static string _host;

        public SmsSender(SqlConnection connection, string host)
        {
            _connection = connection;
            _host = host;
        }

        protected bool IsScheduleDeleted(string uid, string b2U)
        {
            bool ret = true;

            lock (_connection)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(_connection.ConnectionString))
                    {
                        SqlCommand command = new SqlCommand(string.Format("SELECT smsschedule_creditsused FROM smsschedule WHERE uid_smsschedule = '{0}' AND uid_b2u = '{1}'", uid, b2U), connection);
                        connection.Open();
                        var rs = command.ExecuteReader();
                        if (rs.Read())
                        {
                            ret = false;
                        }
                        rs.Close();
                    }
                }
                catch (Exception ex)
                {
                    _logger.ErrorException(string.Format("SmsSender IsScheduleDeleted failed. ScheduleId: {0}", uid), ex);
                    ret = false;
                }
            }
            return ret;
        }

        public static bool SendMBlox(Sms sms, ISmsProvider provider)
        {
            ProviderResult result = provider.Send(sms);
            if (result.Code == Constant.ResultOkCode)
            {
                foreach (var item in (IEnumerable<ProviderResult>)result.Data)
                {
                    UpdateState(item.Code, sms.Id, item.Code == Constant.ResultOkCode ? 90 : 40, item, Constant.MBLOX, (string) item.Data, sms);
                }
                return true;
            }
            return false;
        }

        public void Send(Sms sms)
        {
            try
            {
                bool isSent = false;
                bool isDeleted = false;

                for (int i = 0; i < 3; i++)
                {
                    try
                    {
                        foreach (int gateway in sms.Gateways)
                        {
                            for (int j = 0; j < 2; j++)
                            {
                                if (j == 1)
                                {//todo pp why?!
                                    Thread.Sleep(20000);
                                }
                                lock (this)
                                {
                                    try
                                    {
                                        if (IsScheduleDeleted(sms.Id, sms.UidB2U))
                                        {
                                            isSent = true;      // skip the Cirtical Error log
                                            isDeleted = true;
                                            break;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        _logger.TraceException("IsScheduleDeleted", e);
                                        throw;
                                    }
                                }
                                ProviderResult result;
                                try
                                {
                                    result = SendSms(gateway, sms);
                                }
                                catch (Exception e)
                                {
                                    _logger.TraceException("SendSms", e);
                                    throw;
                                }
                                try
                                {
                                    if (result.Code == Constant.ResultOkCode)
                                    {
                                        isSent = true;
                                        break;
                                    }
                                }
                                catch (Exception e)
                                {
                                    _logger.TraceException("result", e);
                                    throw;
                                }
                            }
                            if (isSent)
                            {
                                break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.TraceException("sms.Gateways", e);
                        throw;
                    }
                    if (isSent)
                    {
                        break;
                    }
                }
                if (!isSent)
                {
                    ProviderResult rs = new ProviderResult(Constant.CriticalErrorCode, Constant.CriticalError);
                    UpdateState(rs.Code, sms.Id, 0, rs, -1, "", sms);

                    // TODO send an email to manager
                }
                if (isDeleted)
                {
                    ProviderResult rs = new ProviderResult(Constant.ResultDeletedScheduleCode, Constant.ResultDeletedSchedule);
                    UpdateState(rs.Code, sms.Id, 0, rs, -1, "", sms);
                }
            }
            catch (Exception e)
            {
                _logger.ErrorException(string.Format("SmsSend failed. SmsId: {0}", sms.Id), e);
            }
        }

        protected virtual ISmsProvider DeduceProvider(int gatewayId)
        {
            return ProviderIndexer.Instance[gatewayId];
        }

        private ProviderResult SendSms(int gateway, ISms sms)
        {
            ProviderResult rs;
            try
            {
                var provider = DeduceProvider(gateway);
                rs = provider.Send(sms);
                _logger.Info("SMS is sent. Id: {0}, gateway-out: {1}", sms.Id, gateway);
            }
            catch (Exception e)
            {
                _logger.ErrorException(string.Format("SmsSender SendSms failed. ScheduleId: {0}", sms.Id), e);
                rs = new ProviderResult(-1, "Blank Reply");
            }
            int status = rs.Code == Constant.ResultOkCode ? 90 : 40;
            UpdateState(rs.Code, sms.Id, status, rs, gateway, "", sms);
            return rs;
        }

        private static void UpdateState(int errcode, string uid, int newState, ProviderResult providerResult, int gateway, string phone, ISms sms)
        {
            string shortQName = errcode == Constant.ResultOkCode ? "ok" : "fail";
            try
            {
                ScheduleResponse response = new ScheduleResponse
                {
                    ScheduleId = sms.Id,
                    State = newState,
                    Code = providerResult.Code,
                    Message = providerResult.Description,
                    TimeSent = DateTime.Now,
                    Gateway = gateway.ToString(CultureInfo.InvariantCulture),
                    TimeStart = sms.StartTime,
                    B2U = sms.UidB2U
                };
                if (phone.Equals(string.Empty))
                {
                    response.RecipientList = sms.Recipients;
                    response.OrigPhoneList = sms.OriginalPhoneList;
                    string[] p = sms.Recipients.Split(',');
                    response.RecipientCount = p.Length;
                }
                else
                {
                    response.RecipientList = response.OrigPhoneList = phone;
                    response.RecipientCount = 1;
                }
                ScheduleResponseRoot root = new ScheduleResponseRoot { Response = response };
                using (MemoryStream stream = new MemoryStream())
                {
                    var settings = new XmlWriterSettings
                    {
                        Encoding = Encoding.GetEncoding(1252),
                        OmitXmlDeclaration = true,
                        Indent = true
                    };
                    using (var writer = XmlWriter.Create(stream, settings))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(ScheduleResponseRoot));
                        serializer.Serialize(writer, root, new XmlSerializerNamespaces(new[] { new XmlQualifiedName("", "http://tempuri.org/dsSchedule.xsd") }));
                    }
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        stream.Position = 0;
                        string xml = reader.ReadToEnd();

                        QueueManager.PostTransaction(null, _host, shortQName, "Queue_", sms.Id, xml);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorException(string.Format("SmsSender UpdateState failed. ScheduleId: {0}", uid), ex);
            }
        }
    }
}

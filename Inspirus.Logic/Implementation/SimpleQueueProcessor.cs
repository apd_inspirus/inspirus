﻿using System;
using System.Messaging;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Model;
using NLog;

namespace Inspirus.Logic.Implementation
{
    public class SimpleQueueProcessor : IQueueProcessor
    {
        protected readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");

        private readonly MessageQueue _queue;
        private readonly SmsSender _sender;

        public SimpleQueueProcessor(string path, SmsSender sender)
        {
            _queue = new MessageQueue(path)
                {
                    Formatter = new XmlMessageFormatter(new[] {typeof (string)})
                };
            _sender = sender;
        }

        public void Process()
        {
            //todo pp why 5000?
            for (int i = 0; i < 5000; i++)
            {
                try
                {
                    Message m = _queue.Receive(new TimeSpan(0, 0, 0, 1));
                    if (m != null) 
                    {
                        string xml = (string)m.Body;
                        Sms sms = new Sms(xml);
                        _sender.Send(sms);
                    }
                }
                catch (MessageQueueException mex)
                {
                    if (mex.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                    {
                        _logger.ErrorException(string.Format("Message Queue Receive failed. Path='{0}'", _queue.Path), mex);
                    }
                    break;
                }
                catch (Exception ex)
                {
                    _logger.ErrorException("Message Queue Receive failed.", ex);
                    break;
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Messaging;
using System.Text;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Model;

namespace Inspirus.Logic.Implementation
{
    public class GroupMessageProcessor : AMessageProcessor
    {
        protected const int BATCH_LIMIT = 50;

        public GroupMessageProcessor(string smsChannel, IPhonesValidator validator) 
            : base(smsChannel, validator) {}

        public override bool Process(DateTime start, SmsSchedule schedule, SmsSender sender, string qHost, string shortQName)
        {
            int counter = 0;
            int lastGateway = Constant.NON_EXISTING_GATEWAY;
            StringBuilder sb = new StringBuilder();
            MessageQueueTransaction transaction = new MessageQueueTransaction();
            transaction.Begin();
            try
            {
                foreach (Phone p in schedule.GroupPhoneList)
                {
                    if (lastGateway != p.Gateway)
                    {
                        lastGateway = p.Gateway;
                        if (sb.Length > 0)
                        {
                            SendGroup(transaction, start, schedule, lastGateway, sb.ToString(), qHost, shortQName);
                            sb.Clear();
                            counter = 0;
                        }
                    }
                    sb.Append(p.Number);
                    sb.Append(",");
                    counter++;
                    if (counter >= BATCH_LIMIT)
                    {
                        SendGroup(transaction, start, schedule, lastGateway, sb.ToString(), qHost, shortQName);
                        sb.Clear();
                        counter = 0;
                    }
                }
                if (sb.Length > 0)
                {
                    SendGroup(transaction, start, schedule, lastGateway, sb.ToString(), qHost, shortQName);
                }

                transaction.Commit();
                string qName = Util.GetQueueName(qHost, shortQName, "sms_");
                SimpleQueueProcessor sp = new SimpleQueueProcessor(qName, sender);
                sp.Process();

                return true;
            }
            catch (Exception ex)
            {
                transaction.Abort();
                _logger.ErrorException(string.Format("GroupMessage Process failed. ScheduleId: {0}", schedule.UidSchedule), ex);
            }
            finally
            {
                transaction.Dispose();
            }
            return false;
        }

        private void SendGroup(MessageQueueTransaction transaction, DateTime dateTime, SmsSchedule sch, int gateway, string phoneList, string qHost, string shortQName)
        {
            IEnumerable<int> gateways = GetGateways(sch.Grade, gateway);
            if (phoneList.Length > 0)
            {
                phoneList = phoneList.Substring(0, phoneList.Length - 1);
            }
            string phones = Validator.GetValidateList(phoneList);
            Sms sms = new Sms(dateTime,
                              sch.UidSchedule,
                              phones,
                              sch.SmsOrigin,
                              sch.SmsMessage,
                              gateways,
                              phoneList,
                              sch.B2Uid,
                              sch.SmsFlash);
            QueueManager.PostTransaction(transaction, qHost, shortQName, "sms_", sch.UidSchedule, sms.ToXml());
        }
    }
}
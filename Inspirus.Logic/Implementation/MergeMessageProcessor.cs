﻿using System;
using System.Collections.Generic;
using System.Messaging;
using System.Threading;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Model;

namespace Inspirus.Logic.Implementation
{
    public sealed class MergeMessageProcessor : AMessageProcessor
    {
        private const int MERGE_CHANNEL = 3;
        public MergeMessageProcessor(string smsChannel, IPhonesValidator validator) 
            : base(smsChannel, validator) {}
        
        public override bool Process(DateTime start, SmsSchedule schedule, SmsSender sender, string qHost, string shortQName)
        {
            int channel = 0;
            int lastGateway = int.MinValue;
            MessageQueueTransaction transaction = new MessageQueueTransaction();
            transaction.Begin();
            try
            {
                IEnumerable<int> gateways = null;
                foreach (Phone p in schedule.GroupPhoneList)
                {
                    string msg = MergeField("name", schedule.SmsMessage, p.FirstName);
                    msg = MergeField("merge", msg, p.OtherField);
                    if (lastGateway != p.Gateway)
                    {
                        lastGateway = p.Gateway;
                        gateways = GetGateways(schedule.Grade, p.Gateway);
                    }
                    string plist = p.Number;
                    Sms sms = new Sms(start,
                                      schedule.UidSchedule,
                                      plist,
                                      schedule.SmsOrigin,
                                      msg,
                                      gateways,
                                      p.OriginalPhone,
                                      schedule.B2Uid,
                                      schedule.SmsFlash);
                    string prefix = String.Format("_{0}_sms_", channel % MERGE_CHANNEL);
                    QueueManager.PostTransaction(transaction, qHost, shortQName, prefix, schedule.UidSchedule, sms.ToXml());
                    channel++;
                }
                transaction.Commit();
                for (int ch = 0; ch < MERGE_CHANNEL; ch++)
                {
                    string prefix = String.Format("_{0}_sms_", ch);
                    string qName = Util.GetQueueName(qHost, shortQName, prefix);
                    SimpleQueueProcessor sp = new SimpleQueueProcessor(qName, sender);
                    Thread th = new Thread(sp.Process);
                    th.Start();
                }
                return true;
            }
            catch (Exception ex)
            {
                transaction.Abort();
                _logger.ErrorException(string.Format("MergeMessage Process failed. ScheduleId: {0}", schedule.UidSchedule), ex);
            }
            finally
            {
                transaction.Dispose();
            }
            return false;
        }

        /// <summary>
        /// merge fields
        /// </summary>
        /// <param name="field"></param>
        /// <param name="content"></param>
        /// <param name="newValue"></param>
        /// <returns></returns>
        private string MergeField(string field, string content, string newValue)
        {
            string lowerField = field.ToLower();
            string upperField = field.ToUpper();
            string messageLower = content.ToLower();
            int p1 = messageLower.IndexOf(string.Format("%21{0}%21", lowerField), StringComparison.Ordinal);
            int p2 = messageLower.IndexOf(string.Format("!{0}!", lowerField), StringComparison.Ordinal);
            if (p1 >= 0)
            {
                //                fld = string.Format("%21{0}%21", upperField);
                string fld = content.Substring(p1, upperField.Length + 6);
                content = content.Replace(fld, newValue);
            }
            if (p2 >= 0)
            {
                //                string fld = string.Format("!{0}!", upperField);
                string fld = content.Substring(p2, upperField.Length + 2);
                content = content.Replace(fld, newValue);
            }
            return content;
        }
    }
}
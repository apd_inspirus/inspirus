﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using Inspirus.Logic.Interface;
using NLog;
using System.Linq;

namespace Inspirus.Logic.Implementation
{
    public class MBloxProvider : ISmsProvider
    {
        protected readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");

        public int GatewayId { get; private set; }
        public string Grade { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public string Url { get; private set; }
        public int Order { get; set; }
        public string MobileCode { get; private set; }

        public MBloxProvider(string username, string password, int gatewayId, int order, string url, string grade, string mobileCode = "61")
        {
            GatewayId = gatewayId;
            Grade = grade;
            Username = username;
            Password = password;
            Url = url;
            Order = order;
            MobileCode = mobileCode;
        }

        public virtual ProviderResult Send(ISms sms)
        {
            try
            {
                string xmlData = GetData(sms);
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    string response = client.UploadString(Url, xmlData);
                    IEnumerable<ProviderResult> results = DeduceResult(response);
                    ProviderResult first = results.FirstOrDefault(r => r.Code != Constant.ResultOkCode);
                    ProviderResult result = first ?? ProviderResult.Ok;
                    if (result.Code == Constant.ResultOkCode)
                    {
                        _logger.Debug(string.Format("Successful send. Provider: MBlox, SmsId: {0}", sms.Id));
                    }
                    else
                    {
                        _logger.Error(string.Format("Sms send failed ({1}, data:'{2}'). Provider: MBlox, SmsId: {0}", sms.Id, result.Description, result.Data));
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorException(string.Format("MBloxProvider Send failed. SmsId: {0}", sms.Id), ex);
                return ProviderResult.Exception(ex.Message);
            }
        }

        protected IEnumerable<ProviderResult> DeduceResult(string response)
        {
            IList<ProviderResult> results = new List<ProviderResult>();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(response);
                XmlNodeList retList = doc.GetElementsByTagName("NotificationResult");
                foreach (XmlNode node in retList)
                {
                    XmlNode nSNumber = node.SelectSingleNode("descendant::SubscriberNumber");
                    XmlNode nResultCode = node.SelectSingleNode("descendant::SubscriberResultCode");
                    XmlNode nResultText = node.SelectSingleNode("descendant::SubscriberResultText");

                    if (nSNumber == null || nResultCode == null || nResultText == null)
                    {
                        continue;
                    }

                    string phone = nSNumber.InnerText.Trim();
                    string retCode = nResultCode.InnerText.Trim();
                    string retText = nResultText.InnerText.Trim();

                    ProviderResult result = new ProviderResult(Convert.ToInt32(retCode), retText, phone);
                    results.Add(result);
                }
            }
            catch (Exception e)
            {
                _logger.ErrorException(string.Format("Mblox Response Parsing failed. Response: {0}", response), e);
                results.Add(ProviderResult.Exception(e));
            }
            return results;
        }

        protected string GetData(ISms sms)
        {
            StringBuilder builder = new StringBuilder("XMLDATA=");
            StringBuilder xmlBuilder = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(xmlBuilder))
            {
                writer.WriteStartElement("NotificationRequest");
                writer.WriteAttributeString("Version", "3.0");
                
                writer.WriteStartElement("NotificationHeader");
                writer.WriteElementString("PartnerName", Username);
                writer.WriteElementString("PartnerPassword", Password);
                writer.WriteEndElement();

                writer.WriteStartElement("NotificationList");
                writer.WriteAttributeString("BatchID", "1");
                foreach (string r in sms.Recipients.Split(','))
                {
                    writer.WriteStartElement("Notification");
                    writer.WriteAttributeString("SequenceNumber", "0");
                    writer.WriteAttributeString("MessageType", sms.SmsFlash ? "FlashSMS" : "SMS");
                    writer.WriteElementString("Message", sms.Message); //encode it properly?
                    writer.WriteElementString("Profile", "10070");

                    writer.WriteStartElement("SenderID");
                    writer.WriteAttributeString("Type", "Alpha");
                    writer.WriteString(sms.Sender);
                    writer.WriteEndElement();

                    writer.WriteStartElement("Subscriber");
                    writer.WriteElementString("SubscriberNumber", r);
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }
                writer.WriteEndDocument();
            }
            builder.Append(HttpUtility.UrlEncode(xmlBuilder.ToString()));
            return builder.ToString();
        }
    }
}
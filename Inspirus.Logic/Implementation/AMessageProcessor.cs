﻿using System;
using System.Collections.Generic;
using Inspirus.Logic.Interface;
using NLog;
using System.Linq;

namespace Inspirus.Logic.Implementation
{
    public abstract class AMessageProcessor : IMessageProcessor
    {
        protected readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");
        public string SmsChannel { get; protected set; }
        public IPhonesValidator Validator { get; protected set; }
        
        protected AMessageProcessor(string smsChannel, IPhonesValidator validator)
        {
            SmsChannel = smsChannel;
            Validator = validator;
        }

        public abstract bool Process(DateTime start, SmsSchedule schedule, SmsSender sender, string qHost, string qName);

        public IEnumerable<int> GetGateways(string grade, int preferredId, int @default = Constant.NEXTDIGITAL_SS)
        {
            IEnumerable<ISmsProvider> providers = ProviderIndexer.Instance.GetGradeGateways(grade);
            IList<int> gateways = new List<int>();
            if (providers != null)
            {
                if (grade.ToUpper() != "BG" && providers.All(p => p.GatewayId != preferredId)) //todo pp refactor this
                {
                    if (preferredId != -1)
                    {
                        gateways.Add(preferredId);
                    }
                    else
                    {
                        _logger.Warn("Gateway -1 was ignored, Grade '{0}'", grade);
                    }
                }
                if (providers.Any(p => p.GatewayId == preferredId && p.Grade == grade.ToUpper()))
                {
                    gateways.Add(preferredId);
                }
                foreach (var p in providers)
                {
                    if (!gateways.Contains(p.GatewayId))
                    {
                        gateways.Add(p.GatewayId);
                    }
                }
            }
            if (gateways.Count == 0)
            {
                gateways.Add(@default);
            }
            return gateways;
        }
    }
}
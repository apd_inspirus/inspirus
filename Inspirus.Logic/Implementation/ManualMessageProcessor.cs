﻿using System;
using System.Collections.Generic;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Model;

namespace Inspirus.Logic.Implementation
{
    public sealed class ManualMessageProcessor : AMessageProcessor
    {
        public ManualMessageProcessor(string smsChannel, IPhonesValidator validator) 
            : base(smsChannel, validator) {}
        
        public override bool Process(DateTime start, SmsSchedule schedule, SmsSender sender, string qHost, string qName)
        {
            try
            {
                IEnumerable<int> gateways = GetGateways(schedule.Grade, schedule.GatewayId);
                string plist = Validator.GetValidateList(schedule.SmsPhoneList);
                Sms sms = new Sms(start,
                                  schedule.UidSchedule,
                                  plist,
                                  schedule.SmsOrigin,
                                  schedule.SmsMessage,
                                  gateways,
                                  schedule.SmsPhoneList,
                                  schedule.B2Uid,
                                  schedule.SmsFlash);
                sender.Send(sms);
                return true;
            }
            catch (Exception e)
            {
                _logger.ErrorException(string.Format("ManualMessage Process failed. ScheduleId: {0}", schedule.UidSchedule), e);
            }
            return false;
        }
    }
}
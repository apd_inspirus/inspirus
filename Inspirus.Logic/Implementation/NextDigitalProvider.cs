﻿using System;
using System.Net;
using System.Web;
using Inspirus.Logic.Interface;
using NLog;

namespace Inspirus.Logic.Implementation
{
    public class NextDigitalProvider : ISmsProvider
    {
        protected readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");

        public int GatewayId { get; private set; }
        public string Grade { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public int Order { get; set; }
        public string Url { get; private set; }
        public string Route { get; private set; }
        public string MobileCode { get; private set; }

        public NextDigitalProvider(string username, string password, int gatewayId, int order, string url, string route, string grade, string mobileCode = "61")
        {
            Username = username;
            Password = password;
            Url = url;
            Route = route;
            GatewayId = gatewayId;
            Grade = grade;
            Order = order;
            MobileCode = mobileCode;
        }

        public virtual ProviderResult Send(ISms sms)
        {
            try
            {
                string url = GetUrl(sms.Recipients, sms.Sender, sms.Message);
                using (WebClient client = new WebClient())
                {
                    string s = client.DownloadString(url);
                    ProviderResult result = DeduceResult(s.Trim());
                    if (result.Code == Constant.ResultOkCode)
                    {
                        _logger.Debug(string.Format("Successful send. Provider: NextDigital, SmsId: {0}", sms.Id));
                    }
                    else
                    {
                        _logger.Error(string.Format("Sms send failed ({1}, data:'{2}'). Provider: NextDigital, SmsId: {0}", sms.Id, result.Description, result.Data));
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorException(string.Format("NextDigitalProvider Send failed. SmsId: {0}", sms.Id), ex);
                return ProviderResult.Exception(ex.Message);
            }
        }

        protected string GetUrl(string recipients, string senderPhone, string message)
        {
            recipients = recipients.Replace("+", MobileCode);
            if (senderPhone.StartsWith("+"))
            {
                senderPhone = senderPhone.Substring(1);
                if (senderPhone.StartsWith(MobileCode))
                {
                    senderPhone = string.Format("0{0}", senderPhone.Substring(2));
                }
            }
            return string.Format("{0}?USER_NAME={1}&PASSWORD={2}&RECIPIENT={3}&ORIGINATOR={4}&MESSAGE_TEXT={5}&ROUTE={6}",
                                Url, Username, Password, recipients, senderPhone, HttpUtility.UrlEncode(message), Route);
        }

        protected ProviderResult DeduceResult(string raw)
        {
            if (raw.Equals("0") || raw.Equals("1"))
            {
                return ProviderResult.Ok;
            }
            return ProviderResult.SendFailed;
        }
    }
}
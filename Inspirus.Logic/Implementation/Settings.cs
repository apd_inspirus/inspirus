﻿using System;
using System.Data;
using System.IO;
using System.Messaging;
using Inspirus.Logic.Interface;
using Microsoft.Win32;
using NLog;

namespace Inspirus.Logic.Implementation
{
    public class Settings : ISettings
    {
        private readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");

        public virtual int TimerIntervalSec { get; protected set; }
        public virtual int MBloxTimerIntervalSec { get; protected set; }
        public int QNumber { get; private set; }
        public int Limit { get; private set; }
        public string UserId { get; private set; }
        public string Password { get; private set; }
        public string DbHost { get; private set; }
        public string DbName { get; private set; }
        public string MqHost { get; private set; }
        public string MqName { get; set; }

        public virtual string ConnectionString
        {
            get
            {
                return string.Format(
                    "packet size=4096;user id={0};data source={1};persist security info=True;initial catalog={2};password={3}",
                    UserId,
                    DbHost,
                    DbName,
                    Password);
            }
        }

        public Settings() {}

        public Settings(int timerIntervalSec, int qNumber, int limit, string userId, string password, string dbHost, string dbName, string mqHost, string mqName, int mblox = 30)
        {
            TimerIntervalSec = timerIntervalSec;
            MBloxTimerIntervalSec = timerIntervalSec;
            QNumber = qNumber;
            Limit = limit;
            UserId = userId;
            Password = password;
            DbHost = dbHost;
            DbName = dbName;
            MqHost = mqHost;
            MqName = mqName;
        }

        public void Load(string registryPath)
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey k = rk.CreateSubKey(registryPath);
            UserId = k.WithDefault("db_user", "inspirus");
            Password = k.WithDefault("db_pass", "w84r9g7a");
            DbHost = k.WithDefault("db_host", "192.168.91.11");
            DbName = k.WithDefault("db_name", "Inspirus");
            MqHost = k.WithDefault("mq_host", "127.0.0.1");
            MqName = k.WithDefault("mq_name", "Queue_");
            QNumber = Convert.ToInt32(k.WithDefault("queue_count", "3"));
            TimerIntervalSec = Convert.ToInt32(k.WithDefault("sender_timer_sec", "3"));
            MBloxTimerIntervalSec = Convert.ToInt32(k.WithDefault("smpp_timer_sec", "30"));
            Limit = Convert.ToInt32(k.WithDefault("limit", "1"));
        }

        public void Save(string registryPath)
        {
            RegistryKey rk = Registry.CurrentUser;
            RegistryKey k = rk.CreateSubKey(registryPath);
            if (k != null)
            {
                k.SetValue("db_user", UserId);
                k.SetValue("db_pass", Password);
                k.SetValue("db_host", DbHost);
                k.SetValue("db_name", DbName);
                k.SetValue("mq_host", MqHost);
                k.SetValue("mq_name", MqName);
                k.SetValue("queue_count", Convert.ToString(QNumber));
                k.SetValue("sender_timer_sec", Convert.ToString(TimerIntervalSec));
                k.SetValue("smpp_timer_sec", Convert.ToString(MBloxTimerIntervalSec));
                k.SetValue("limit", Convert.ToString(Limit));
            }
        }

        public DataSet GetGatewayRangeDataSet()
        {
            DataSet dsGatewayRange = new DataSet();
            string path = String.Empty;

            string queuename = string.Format("{0}DB", MqName);
            path = Util.GetQueueName(MqHost, queuename);
            MessageQueue mq = new MessageQueue(path);
            try
            {
                mq.Formatter = new XmlMessageFormatter(new[] { typeof(String) });
                Message m = mq.Receive(new TimeSpan(0, 0, 0, 2));
                if (m != null)
                {
                    string xml = (string)m.Body;
                    StringReader sr = new StringReader(xml);
                    dsGatewayRange.ReadXml(sr, XmlReadMode.InferSchema);
                    _logger.Trace("Loaded from Queue: {0}", path);
                }
                dsGatewayRange.WriteXml(".\\gateway_range.xml");
            }
            catch (MessageQueueException mex)
            {
                if (mex.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                {
                    _logger.ErrorException(string.Format("Message Queue Receive failed. Path='{0}'", path), mex);
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorException("GetGatewayRangeDataSet failed.", ex);
            }
            if (dsGatewayRange.Tables.Count == 0)
            {
                if (File.Exists(".\\gateway_range.xml"))
                {
                    dsGatewayRange.ReadXml(".\\gateway_range.xml");
                    _logger.Trace("Gateway Range loaded from file.");
                }
                else
                {
                    _logger.Trace("Current directory '{0}'", Directory.GetCurrentDirectory());
                }
            }
            return dsGatewayRange;
        }
    }
}
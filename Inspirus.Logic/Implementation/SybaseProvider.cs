﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Inspirus.Logic.Interface;
using NLog;

namespace Inspirus.Logic.Implementation
{
    public class SybaseProvider : ISmsProvider
    {
        protected const int BUFFER = 2048;
        protected readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");

        public int GatewayId { get; private set; }
        public string Grade { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public string Url { get; private set; }
        public int Order { get; set; }
        public string ReplyUrl { get; private set; }
        public string Host { get; private set; }
        public string MobileCode { get; private set; }

        public SybaseProvider(string username, string password, int gatewayId, int order, string url, string host, string replyUrl, string grade, string mobileCode = "61")
        {
            Username = username;
            Password = password;
            Url = url;
            ReplyUrl = replyUrl;
            Host = host;
            MobileCode = mobileCode;
            GatewayId = gatewayId;
            Order = order;
            Grade = grade;
        }

        public virtual ProviderResult Send(ISms sms)
        {
            Byte[] recvBytes = new Byte[BUFFER];
            try
            {
                string postdata = GetData(sms.Recipients, sms.Sender, sms.Message, sms.SmsFlash);
                string str = string.Format("{0}:{1}", Username, Password);
                string message = string.Format(@"POST {0} HTTP/1.1
HOST: {1}
Authorization: Basic {3}
CONTENT-LENGTH:{4}

{2}",
                                               Url,
                                               Host,
                                               postdata,
                                               Convert.ToBase64String(Encoding.UTF8.GetBytes(str)),
                                               postdata.Length);
                _logger.Trace(string.Format("{0}:POST='{1}'", sms.Id, message));
                Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPHostEntry hostInfo = Dns.GetHostEntry(Host);
                IPEndPoint hostEndPoint = new IPEndPoint(hostInfo.AddressList[0], 80);

                s.Connect(hostEndPoint);
                if (s.Connected)
                {
                    s.Send(Encoding.UTF8.GetBytes(message));
                    Int32 bytes = s.Receive(recvBytes, recvBytes.Length, 0);
                    ASCIIEncoding ascii = new ASCIIEncoding();
                    StringBuilder builder = new StringBuilder(ascii.GetString(recvBytes));
                    while (bytes > 0)
                    {
                        bytes = s.Receive(recvBytes, recvBytes.Length, 0);
                        builder.Append(ascii.GetString(recvBytes));
                    }
                    ProviderResult result = DeduceResult(builder.ToString());
                    if (result.Code == Constant.ResultOkCode)
                    {
                        _logger.Debug(string.Format("Successful send. Provider: Sybase, SmsId: {0}", sms.Id));
                    }
                    else
                    {
                        _logger.Error(string.Format("Sms send failed ({1}, data:'{2}'). Provider: Sybase, SmsId: {0}", sms.Id, result.Description, result.Data));
                    }
                    return result;
                }
                return new ProviderResult(Constant.BadResponseCode, "Not able to connect");
            }
            catch (Exception ex)
            {
                _logger.ErrorException(string.Format("SybaseProvider Send failed. SmsId: {0}", sms.Id), ex);
                return ProviderResult.Exception(ex);
            }
        }

        protected string GetData(string recipients, string senderPhone, string content, bool smsFlash)
        {
            StringBuilder contentBuilder = new StringBuilder(content);
            contentBuilder.Replace("\r", "<CR>");
            contentBuilder.Replace("\n", "<LF>");
            StringBuilder builder = new StringBuilder(recipients);
            builder.Replace("+", MobileCode);
            builder.Insert(0, "+");
            builder.Replace(string.Format(",{0}", MobileCode), string.Format(",+{0}", MobileCode));
            string result = string.Format(@"Subject={0}
[MSISDN]
List={1}
[MESSAGE]
Text={2}
[SETUP]
OriginatingAddr={3}
MobileNotification=Yes
AckType=Message
AckReplyAddress={4}",
                                       senderPhone,
                                       builder,
                                       contentBuilder,
                                       senderPhone,
                                       ReplyUrl);
            if (smsFlash)
            {
                result = string.Format("{0}\nClass=0", result);
            }
            return result;
        }

        /// <summary>
        /// 
        /// decode any reply
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        protected ProviderResult DeduceResult(string response)
        {
            int p = response.IndexOf("#Message Receive correctly", StringComparison.Ordinal);
            if (p > 0)
            {
                p = response.IndexOf("ORDERID", StringComparison.Ordinal);
                int p2 = response.IndexOf("<br>", p, StringComparison.Ordinal);
                if (p > 0)
                {
                    string s = response.Substring(p, p2 - p);
                    return new ProviderResult(Constant.ResultOkCode, Constant.ResultOk, s);
                }
            }
            return DecodeFailReply(response);
        }

        /// <summary>
        /// decode any failed reply
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private ProviderResult DecodeFailReply(string response)
        {
            int p = response.IndexOf("<BODY>", StringComparison.Ordinal);
            if (p > 0)
            {
                int p2 = response.IndexOf("</BODY>", p, StringComparison.Ordinal);
                if (p2 > 0)
                {
                    StringBuilder s = new StringBuilder(response.Substring(p + 6, p2 - p - 6));
                    s.Replace("<br>", string.Empty);
                    return new ProviderResult(Constant.BadResponseCode, Constant.BadResponse, s.ToString().Trim());
                }
            }
            return new ProviderResult(Constant.BadResponseCode, Constant.BadResponse, response);
        }
    }
}
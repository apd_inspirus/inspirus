﻿using System.Text;
using Inspirus.Logic.Interface;

namespace Inspirus.Logic.Simulaton
{
    public sealed class PhonesValidatorSimulation : IPhonesValidator
    {
        public string GetValidateList(string phones)
        {
            StringBuilder sb = new StringBuilder();
            string[] lists = phones.Split(',');
            for (int index = 0; index < lists.Length; index++)
            {
                sb.Append("+12345,");
            }
            return sb.ToString(0, sb.Length - 1);
        }
    }
}
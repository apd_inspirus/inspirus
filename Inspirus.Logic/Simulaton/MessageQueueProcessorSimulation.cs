﻿using System;

namespace Inspirus.Logic.Simulaton
{
    public class MessageQueueProcessorSimulation : MessageQueueProcessor
    {
        public MessageQueueProcessorSimulation(string queuePath, string qHost, string shortQName, int limit, SmsSender sender, MessageProcessorIndexer messageProcessorIndexer) 
            : base(queuePath, qHost, shortQName, limit, sender, messageProcessorIndexer) {}

        protected override int Do(SmsSchedule sch, string xml, DateTime dt, int i)
        {
            QueueManager.PostTransaction(null, _qHost, "outgoing", "Queue_", sch.UidSchedule, xml);
            return base.Do(sch, xml, dt, i);
        }
    }
}
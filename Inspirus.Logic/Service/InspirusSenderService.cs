﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using Inspirus.Logic.Implementation;
using Inspirus.Logic.Interface;
using NLog;
using Ninject;

namespace Inspirus.Logic.Service
{
    public class InspirusSenderService : IService
    {
        protected readonly Logger _logger = LogManager.GetLogger("Inspirus.Sender.Logic");

        protected readonly ISettings _settings;
        protected readonly IPhonesValidator _phonesValidator;
        protected readonly MessageProcessorIndexer _messageProcessorIndexer;

        private static SqlConnection _connection; 
        private Timer _mainTimer;
        private Timer _mBloxTimer;

        [Inject]
        public InspirusSenderService(ISettings settings, IEnumerable<ISmsProvider> providers, IEnumerable<IMessageProcessor> processors, IPhonesValidator phonesValidator)
        {
            System.IO.Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

            _phonesValidator = phonesValidator;
            _settings = settings;
            _messageProcessorIndexer = new MessageProcessorIndexer(processors);
            ProviderIndexer.Instance.Init(providers);
        }

        private void Init()
        {
            _connection = new SqlConnection
                {
                    ConnectionString = _settings.ConnectionString
                };
        }

        public void Start()
        {
            try
            {
                Init();
                _mainTimer = new Timer(ConsumeSchedule, null, 0, _settings.TimerIntervalSec * 1000);
                _mBloxTimer = new Timer(MBloxSchedule, null, 0, _settings.TimerIntervalSec * 1000);
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Sender Service failed to start.", ex);
            }
        }

        public void Stop()
        {
            _mainTimer.Dispose();
            _mBloxTimer.Dispose();
        }

        private void MBloxSchedule(object state)
        {
            string queueName = Util.GetQueueName(_settings.MqHost, "sms_smpp");
            MBloxQueueProcessor processor = new MBloxQueueProcessor(queueName, ProviderIndexer.Instance[Constant.MBLOX]);
            processor.Process();
        }

        private void ConsumeSchedule(object state)
        {
            DataSet gatewayRangeDataSet = _settings.GetGatewayRangeDataSet();
            ProviderIndexer.Instance.Refresh(gatewayRangeDataSet);
            SmsSender sender = new SmsSender(_connection, _settings.MqHost);

            string queuename = string.Format("{0}Merge", _settings.MqName);
            StartProcessor(queuename, sender);

            string queuenameBG = string.Format("{0}Business", _settings.MqName);
            StartProcessor(queuenameBG, sender);

            for (int i = 0; i < _settings.QNumber; i++)
            {
                string shortQName = string.Format("{0}{1}", _settings.MqName, i);
                StartProcessor(shortQName, sender);

                queuename = Util.GetLocalQueueName(shortQName, "sms_");
                SimpleQueueProcessor sp = new SimpleQueueProcessor(queuename, sender);
                sp.Process();
            }
        }

        private void StartProcessor(string queuename, SmsSender sender)
        {
            string path = Util.GetQueueName(_settings.MqHost, queuename);
            MessageQueueProcessor queueProcessor = new MessageQueueProcessor(path,
                                                                             _settings.MqHost,
                                                                             queuename,
                                                                             _settings.Limit,
                                                                             sender,
                                                                             _messageProcessorIndexer);
            Thread thread = new Thread(queueProcessor.ProcessMessages);
            thread.Start();
        }
    }
}
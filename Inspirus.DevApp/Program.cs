﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Messaging;
using System.Xml.Serialization;
using Inspirus.Logic;
using Inspirus.Logic.Extension;
using Inspirus.Logic.Implementation;
using Inspirus.Logic.Interface;
using Inspirus.Logic.Mock;
using Inspirus.Logic.Model;
using Inspirus.Logic.Service;
using Inspirus.SenderService;
using NLog;
using NLog.Config;
using Ninject;
using Ninject.Modules;

namespace Inspirus.DevApp
{
    class Program
    {
        private const string QUEUE_NAME = ".\\test-queue";
        private static MessageQueue _queue;

        private static Logger _logger;
        private static IService _service;
        
        static void Main(string[] args)
        {
            ConfigurationItemFactory.Default.Targets.RegisterDefinition("ExpiringMsmq", typeof(NlogMsmqTarget));
            _logger = LogManager.GetLogger("Inspirus.Sender");
            IKernel kernel = new StandardKernel(new SmsSenderMockModule());
            _service = kernel.Get<IService>();
            _service.Start();
            _logger.Debug("Start:{0}", DateTime.Now);

            while (Console.ReadLine() != "stop")
            {
                
            }

            _service.Stop();
            _logger.Debug("Stop:{0}", DateTime.Now);

            //Sms sms = new Sms(DateTime.Today, "12345", "+64220990212", "12345", "Hi, this is test", new ArrayList { { 0 } }, string.Empty, "123456", false);
            /*
            Schedule schedule = new Schedule();
            XmlSerializer serializer = new XmlSerializer(typeof(Schedule));
            StringWriter writer = new StringWriter();
            serializer.Serialize(writer, schedule);
            string s = writer.ToString();
            Console.WriteLine(s);
            */
            /*
            if (!MessageQueue.Exists(QUEUE_NAME))
            {
                MessageQueue.Create(QUEUE_NAME);
            }
            else
            {
                Console.WriteLine(QUEUE_NAME + " already exists.");
            }
            _queue = new MessageQueue(QUEUE_NAME);
            Console.ReadKey();
            _queue.Send(new Message("Hi from Console", new BinaryMessageFormatter()));*/
        }
    }
    public sealed class SmsSenderMockModule : NinjectModule
    {
        public override void Load()
        {
            ISettings settings = new MockSettings();
            settings.Load("Software\\Nextdata\\Inspirus");
            DataSet gatewayRange = settings.GetGatewayRangeDataSet();

            Bind<IPhonesValidator>().To<PhonesValidator>();
            Bind<ISmsProvider>().To<MockSybaseProvider>()
                                .WithConstructorArgument("username", "inspirus_roaming")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "/inspirus_roaming/inspirus_roaming.sms")
                                .WithConstructorArgument("host", "messaging.sybase365.com")
                                .WithConstructorArgument("replyUrl", "http://203.63.5.170:9098/NextWS/HandsetAck.aspx")
                                .WithConstructorArgument("gatewayId", Constant.MOBILEWAY_ROAMING)
                                .WithConstructorArgument("order", 2)
                                .WithConstructorArgument("grade", "SS");
            Bind<ISmsProvider>().To<MockSybaseProvider>()
                                .WithConstructorArgument("username", "inspirus_direct")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "/inspirus_direct/inspirus_direct.sms")
                                .WithConstructorArgument("host", "messaging.sybase365.com")
                                .WithConstructorArgument("replyUrl", "http://203.63.5.170:9098/NextWS/HandsetAck.aspx")
                                .WithConstructorArgument("gatewayId", Constant.MOBILEWAY_DIRECT)
                                .WithConstructorArgument("order", 2)
                                .WithConstructorArgument("grade", "BG");
            Bind<ISmsProvider>().To<MockNextDigitalProvider>()
                                .WithConstructorArgument("username", "inspirus")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "http://srs.nextdigital.com/Enterprise/sendsmsv2")
                                .WithConstructorArgument("route", "default")
                                .WithConstructorArgument("gatewayId", Constant.NEXTDIGITAL_SS)
                                .WithConstructorArgument("order", 1)
                                .WithConstructorArgument("grade", "SS");
            Bind<ISmsProvider>().To<MockNextDigitalProvider>()
                                .WithConstructorArgument("username", "inspirusdirect")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "http://srs.nextdigital.com/Enterprise/sendsmsv2")
                                .WithConstructorArgument("route", "default")
                                .WithConstructorArgument("gatewayId", Constant.NEXTDIGITAL_DIRECT)
                                .WithConstructorArgument("order", 1)
                                .WithConstructorArgument("grade", "BG");
            Bind<ISmsProvider>().To<MockMBloxProvider>()
                                .WithConstructorArgument("username", "Inspirus")
                                .WithConstructorArgument("password", "")
                                .WithConstructorArgument("url", "http://xml3.mblox.com:8180/send")
                                .WithConstructorArgument("gatewayId", Constant.MBLOX)
                                .WithConstructorArgument("order", 3)
                                .WithConstructorArgument("grade", "SS");
            Bind<IMessageProcessor>().To<ManualMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Manual")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);
            Bind<IMessageProcessor>().To<GroupMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Group")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);
            Bind<IMessageProcessor>().To<GroupMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Staff")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);
            Bind<IMessageProcessor>().To<MergeMessageProcessor>()
                                     .WithConstructorArgument("smsChannel", "Merge")
                                     .WithConstructorArgument("dsGatewayRange", gatewayRange);

            Bind<IService>().To<InspirusSenderService>()
                            .WithConstructorArgument("settings", settings);
        }
    }
}
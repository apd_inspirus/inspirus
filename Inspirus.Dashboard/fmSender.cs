using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using Inspirus.Logic;
using Inspirus.Logic.Extension;
using Inspirus.Logic.Model;
using NLog;
using NLog.Config;

namespace Inspirus.Dashboard
{
    /// <summary>
    ///     Summary description for Form1.
    /// </summary>
    public class FmSender : Form
    {
        private Panel _panel1;
        private Label _label3;
        private ImageList _imageList1;
        private Label _label7;
        private Panel _panel2;
        private Label _label8;
        private Label _label9;
        private Label _lbInfo;
        private Button _button2;
        private Button _button1;
        private ListBox _lbLog;
        private Label _label2;
        private NumericUpDown _nmInterval;
        private NumericUpDown _nmLimit;
        private TextBox _txRunningSince;
        private IContainer components;
        private BackgroundWorker _worker = new BackgroundWorker();
        
        private const int LIMIT = 20;
        private const int TIMEOUT_SEC = 1;
        private const int INTERVAL = 3000;
        private readonly IList<QueueManager> _managers;

        public FmSender()
        {
            ConfigurationItemFactory.Default.Targets.RegisterDefinition("ExpiringMsmq", typeof(NlogMsmqTarget));
            InitializeComponent();
            _lbInfo.Text = string.Format("Build {0:yyMMdd}", DateTime.Today);
            _button2.Visible = false;

            _nmLimit.Value = 1;
            _nmInterval.Value = 3;
            _txRunningSince.Text = DateTime.Now.ToString("dd/MM/yy hh:mm tt");

            _managers = new List<QueueManager>(6);
            for (int i = 0; i < 6; i++)
            {
                _managers.Add(new QueueManager(string.Format(@".\Private$\{0}_Inspirus.Sender.Log", LogLevel.FromOrdinal(i))));
            }

            _worker.DoWork += CheckQueues;
            _worker.WorkerSupportsCancellation = true;
            _worker.RunWorkerAsync();
        }

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._panel1 = new System.Windows.Forms.Panel();
            this._button2 = new System.Windows.Forms.Button();
            this._button1 = new System.Windows.Forms.Button();
            this._txRunningSince = new System.Windows.Forms.TextBox();
            this._nmLimit = new System.Windows.Forms.NumericUpDown();
            this._label2 = new System.Windows.Forms.Label();
            this._nmInterval = new System.Windows.Forms.NumericUpDown();
            this._lbInfo = new System.Windows.Forms.Label();
            this._panel2 = new System.Windows.Forms.Panel();
            this._label9 = new System.Windows.Forms.Label();
            this._label8 = new System.Windows.Forms.Label();
            this._label7 = new System.Windows.Forms.Label();
            this._label3 = new System.Windows.Forms.Label();
            this._imageList1 = new System.Windows.Forms.ImageList(this.components);
            this._lbLog = new System.Windows.Forms.ListBox();
            this._panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._nmLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nmInterval)).BeginInit();
            this._panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _panel1
            // 
            this._panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this._panel1.Controls.Add(this._button2);
            this._panel1.Controls.Add(this._button1);
            this._panel1.Controls.Add(this._txRunningSince);
            this._panel1.Controls.Add(this._nmLimit);
            this._panel1.Controls.Add(this._label2);
            this._panel1.Controls.Add(this._nmInterval);
            this._panel1.Controls.Add(this._lbInfo);
            this._panel1.Controls.Add(this._panel2);
            this._panel1.Controls.Add(this._label7);
            this._panel1.Controls.Add(this._label3);
            this._panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this._panel1.Location = new System.Drawing.Point(0, 0);
            this._panel1.Name = "_panel1";
            this._panel1.Size = new System.Drawing.Size(581, 97);
            this._panel1.TabIndex = 5;
            // 
            // _button2
            // 
            this._button2.Location = new System.Drawing.Point(463, 35);
            this._button2.Name = "_button2";
            this._button2.Size = new System.Drawing.Size(96, 25);
            this._button2.TabIndex = 31;
            this._button2.Text = "Schedule";
            // 
            // _button1
            // 
            this._button1.Location = new System.Drawing.Point(377, 35);
            this._button1.Name = "_button1";
            this._button1.Size = new System.Drawing.Size(64, 25);
            this._button1.TabIndex = 30;
            this._button1.Text = "MQ";
            this._button1.Click += new System.EventHandler(this.TestMqClick);
            // 
            // _txRunningSince
            // 
            this._txRunningSince.Location = new System.Drawing.Point(345, 7);
            this._txRunningSince.Name = "_txRunningSince";
            this._txRunningSince.Size = new System.Drawing.Size(160, 21);
            this._txRunningSince.TabIndex = 29;
            // 
            // _nmLimit
            // 
            this._nmLimit.Location = new System.Drawing.Point(511, 10);
            this._nmLimit.Name = "_nmLimit";
            this._nmLimit.Size = new System.Drawing.Size(48, 21);
            this._nmLimit.TabIndex = 28;
            this._nmLimit.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // _label2
            // 
            this._label2.Location = new System.Drawing.Point(292, 35);
            this._label2.Name = "_label2";
            this._label2.Size = new System.Drawing.Size(79, 16);
            this._label2.TabIndex = 27;
            this._label2.Text = "Seconds";
            // 
            // _nmInterval
            // 
            this._nmInterval.Location = new System.Drawing.Point(224, 33);
            this._nmInterval.Name = "_nmInterval";
            this._nmInterval.Size = new System.Drawing.Size(62, 21);
            this._nmInterval.TabIndex = 26;
            this._nmInterval.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // _lbInfo
            // 
            this._lbInfo.Location = new System.Drawing.Point(174, 11);
            this._lbInfo.Name = "_lbInfo";
            this._lbInfo.Size = new System.Drawing.Size(112, 16);
            this._lbInfo.TabIndex = 21;
            this._lbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _panel2
            // 
            this._panel2.Controls.Add(this._label9);
            this._panel2.Controls.Add(this._label8);
            this._panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._panel2.Location = new System.Drawing.Point(0, 65);
            this._panel2.Name = "_panel2";
            this._panel2.Size = new System.Drawing.Size(577, 28);
            this._panel2.TabIndex = 15;
            // 
            // _label9
            // 
            this._label9.Location = new System.Drawing.Point(172, 0);
            this._label9.Name = "_label9";
            this._label9.Size = new System.Drawing.Size(160, 16);
            this._label9.TabIndex = 1;
            this._label9.Text = "www.nextdata.com.au";
            this._label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _label8
            // 
            this._label8.Location = new System.Drawing.Point(6, 0);
            this._label8.Name = "_label8";
            this._label8.Size = new System.Drawing.Size(160, 16);
            this._label8.TabIndex = 0;
            this._label8.Text = "Developed by Nextdata P/L";
            this._label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _label7
            // 
            this._label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._label7.Location = new System.Drawing.Point(0, 8);
            this._label7.Name = "_label7";
            this._label7.Size = new System.Drawing.Size(218, 32);
            this._label7.TabIndex = 14;
            this._label7.Text = "Inspirus Sender";
            // 
            // _label3
            // 
            this._label3.Location = new System.Drawing.Point(307, 11);
            this._label3.Name = "_label3";
            this._label3.Size = new System.Drawing.Size(112, 16);
            this._label3.TabIndex = 5;
            this._label3.Text = "Clock";
            // 
            // _imageList1
            // 
            this._imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this._imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this._imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // _mainTimer
            // 
            this._lbLog.CausesValidation = false;
            this._lbLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lbLog.FormattingEnabled = true;
            this._lbLog.ItemHeight = 15;
            this._lbLog.Location = new System.Drawing.Point(0, 97);
            this._lbLog.Name = "_lbLog";
            this._lbLog.Size = new System.Drawing.Size(581, 187);
            this._lbLog.TabIndex = 3;
            // 
            // FmSender
            // 
            this.ClientSize = new System.Drawing.Size(581, 284);
            this.Controls.Add(this._lbLog);
            this.Controls.Add(this._panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.Name = "FmSender";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMS Sender";
            this._panel1.ResumeLayout(false);
            this._panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._nmLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nmInterval)).EndInit();
            this._panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        
        /// <summary>
        ///     the main timer loop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        public void CheckQueues(object state, DoWorkEventArgs doWorkEventArgs)
        {
            while (true)
            {
                foreach (var manager in _managers)
                {
                    ThreadPool.QueueUserWorkItem(CheckQueue, manager);
                }
                Thread.Sleep(INTERVAL);
            }
        }

        public void CheckQueue(object state)
        {
            QueueManager queue = (QueueManager) state;
            for (int i = 0; i < LIMIT; i++)
            {
                string s = queue.ReceiveString(TIMEOUT_SEC);
                if (s == null)
                {
                    break;
                }
                _lbLog.BeginInvoke((MethodInvoker)(() =>
                    {
                        if (_lbLog.Items.Count >= 50)
                        {
                            _lbLog.Items.Clear();
                        }
                        _lbLog.Items.Add(s);
                    }));
            }
        }

        private void TestMqClick(object sender, EventArgs e)
        {
            int gateway = Constant.MBLOX;
            IEnumerable<int> gw = new List<int> { gateway };
            string msg = String.Format("  Hi dear!  Test gateway {0} flash", gateway);
            Sms m = new Sms(DateTime.Now,
                                          "uid_TEST1",
                                          "61401363045",
                                          null,
                                          msg,
                                          gw,
                                          "0401363045",
                                          "uid_B2U",
                                          false);
//todo pp organize test sends
//            m.send();
        }
    }
}